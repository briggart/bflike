module hpx_hack_mod

  !Hack to some HEALPix I/O routines to deal with huge BFLike datafiles
  !when Nside >= 64
    
  !----------------------------------------------------------------------------
  !
  !  Copyright (C) 1997-2013 Krzysztof M. Gorski, Eric Hivon,
  !                          Benjamin D. Wandelt, Anthony J. Banday, 
  !                          Matthias Bartelmann, Hans K. Eriksen, 
  !                          Frode K. Hansen, Martin Reinecke
  !
  !
  !
  !  HEALPix is free software; you can redistribute it and/or modify
  !  it under the terms of the GNU General Public License as published by
  !  the Free Software Foundation; either version 2 of the License, or
  !  (at your option) any later version.
  !
  !  HEALPix is distributed in the hope that it will be useful,
  !  but WITHOUT ANY WARRANTY; without even the implied warranty of
  !  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  !  GNU General Public License for more details.
  !
  !  You should have received a copy of the GNU General Public License
  !  along with HEALPix; if not, write to the Free Software
  !  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  !
  !  For more information about HEALPix see http://healpix.sourceforge.net
  !
  !----------------------------------------------------------------------------
  
  use healpix_types 
  use healpix_modules ,only : printerror ,fatal_error ,assert ,string ,putrec

  implicit none
  
  real(kind=SP),     private, parameter :: s_bad_value = HPX_SBADVAL
  real(kind=DP),     private, parameter :: d_bad_value = HPX_DBADVAL
  integer(kind=I4B), private, parameter :: i_bad_value = -1637500000
  integer(kind=I8B), private, parameter :: l_bad_value = -1637500000_I8B
  integer(I4B) ,     private, parameter :: nchunk_max  = 12000
  integer(I4B),      private, parameter :: MAXDIM_TOP  = 199 ! < 999

  private
  public write_bintabh_i8b

  interface f90ftpcl_
     module procedure f90ftpcle, f90ftpcld, f90ftpcldll, f90ftpclj, f90ftpclk
  end interface

contains
  
  subroutine write_bintabh_i8b(tod, npix, ntod, header, nlheader, filename, extno, firstpix, repeat)
    !========================================================================
    ! ======================================================================
    !     Create a FITS file containing a binary table extension in the first extension
    !
    !     Designed to deal with Huge file, (n_elements > 2^31)
    !
    !     OPTIONAL NEW PARAMETERS:
    !     firstpix : position in the file of the first element to be written (starts at 0) 
    !                default value =0
    !                8-byte integer
    !                if NE 0 then suppose that the file already exists
    !
    !     repeat   : length of vector per unit rows and columns of the first binary extension
    !                default value = 12000 (\equiv 1 mn of PLANCK/HFI data)
    !                4-byte integer
    ! 
    !     OTHER PARAMETERS
    !     unchanged with respect to the standard write_bintab of the HEALPIX package except 
    !     npix which is an 8-byte integer
    !
    !     Adapted from write_bintab
    !                                           E.H. & O.D. @ IAP 07/02
    !
    !     Requires a compilation in 64 bits of the CFITSIO 
    !     Note that the flag -D_FILE_OFFSETS_BITS=64 has to be added 
    !         (cf page CFITIO 2.2 User's guide  Chap 4, section 4-13)
    ! 
    ! 2002-07-08 : bugs correction by E.H. 
    !    (uniform use of firstpix_tmp, introduction of firstpix_chunk)
    ! 2015-04-28: better handling of repeat (suggested by R. Keskitalo)
    !
    !lpl: hack to deal with huge vectors
    !==========================================================================

    USE healpix_types
    IMPLICIT NONE

    INTEGER(I8B)     , INTENT(IN)           :: npix
    INTEGER(I8B)     , INTENT(IN), OPTIONAL :: firstpix
    INTEGER(I4B)     , INTENT(IN), OPTIONAL :: repeat
    INTEGER(I4B)     , INTENT(IN)           :: ntod,nlheader
    REAL(DP)         , INTENT(IN), DIMENSION(0:npix-1,1:ntod) :: tod
    CHARACTER(LEN=80), INTENT(IN), DIMENSION(1:nlheader)      :: header
    CHARACTER(LEN=*),  INTENT(IN)           :: filename
    INTEGER(I4B)    , INTENT(IN)     , OPTIONAL :: extno

    INTEGER(I4B) :: status,unit,blocksize,bitpix,naxis,naxes(1),repeat_fits
    INTEGER(I4B) :: i,npix_32
    LOGICAL(LGT) :: simple,extend
    CHARACTER(LEN=80) :: comment, ch
    integer(I8B) :: repeat_tmp


    INTEGER(I4B), PARAMETER :: MAXDIM = MAXDIM_TOP !number of columns in the extension
    INTEGER(I8B)      :: frow ,felem ,nrows !lpl
    INTEGER(I4B)      :: tfields,varidat
    INTEGER(I4B)      :: colnum,readwrite,width,datacode,hdutype
!    INTEGER(I4B)      :: frow,felem,colnum,readwrite,width,datacode,hdutype
    CHARACTER(LEN=20) :: ttype(MAXDIM), tform(MAXDIM), tunit(MAXDIM), extname
    CHARACTER(LEN=10) :: card
    CHARACTER(LEN=2)  :: stn
    INTEGER(I4B)      :: itn  

    real(dp), dimension(:), allocatable :: padding
    integer(I8B) :: lastpix
    integer(I4B) :: lengap

    INTEGER(I4B)      :: extno_i
    character(len=filenamelen) :: sfilename
    INTEGER(I8B) :: q ,iq ,npix_tmp ,firstpix_tmp ,firstpix_chunk ,i0 ,i1
    character(len=1) :: pform
    character(len=*), parameter :: code='write_bintabh'
    !-----------------------------------------------------------------------

    pform = 'D'

    IF (.NOT. PRESENT(repeat) ) THEN 
       repeat_tmp = 1
       if (mod(npix,1024_i8b) == 0) then
          repeat_tmp = 1024
       elseif (npix >= 12000) then
          repeat_tmp = 12000 
       endif
    ELSE 
       repeat_tmp = repeat
    ENDIF
    IF (.NOT. PRESENT(firstpix) ) THEN 
       firstpix_tmp = 0 
    ELSE 
       firstpix_tmp = firstpix
    ENDIF

    extno_i = 0
    if (present(extno)) extno_i = extno

    status=0
    unit = 137
    blocksize=1

    ! remove the leading '!' (if any) when reopening the same file
    sfilename = adjustl(filename)
    if (sfilename(1:1) == '!') sfilename = sfilename(2:filenamelen)

    ! create the new empty FITS file

    IF (firstpix_tmp .EQ. 0) THEN

       if (extno_i == 0) then 
          CALL ftinit(unit,filename,blocksize,status)
          if (status > 0) call fatal_error("Error while creating file " &
               & //trim(filename) &
               & //". Check path and/or access rights.")

          ! -----------------------------------------------------
          ! Initialize parameters about the FITS image
          simple=.TRUE.
          bitpix=32     ! integer*4
          naxis=0       ! no image
          naxes(1)=0
          extend=.TRUE. ! there is an extension

          !     ----------------------
          !     primary header
          !     ----------------------
          !     write the required header keywords
          CALL ftphpr(unit,simple,bitpix,naxis,naxes,0_i4b,1_i4b,extend,status)

          !     writes supplementary keywords : none

          !     write the current date
          CALL ftpdat(unit,status) ! format ccyy-mm-dd

       !     ----------------------
       !     image : none
       !     ----------------------

       !     ----------------------
       !     extension
       !     ----------------------
       else

          !*********************************************
          !     reopen an existing file and go to the end
          !*********************************************
          call ftopen(unit,sfilename,1_i4b,blocksize,status)
          call ftmahd(unit,1_i4b+extno_i,hdutype,status)

       endif

       !     creates an extension
       CALL ftcrhd(unit, status)

       !     writes required keywords
       ! if (npix < repeat_tmp) repeat_tmp = npix ! 2015-04-28

       ! nrows    = npix / repeat_tmp + 1 ! naxis1
       nrows    = (npix - 1_i8b) / repeat_tmp + 1_i4b ! 2015-04-28

       tfields  = ntod
       WRITE(ch,'(i8)') repeat_tmp
!       tform(1:ntod)  = TRIM(ADJUSTL(ch))//pform ! does not work with Ifort, EH, 2006-04-04
       ch = TRIM(ADJUSTL(ch))//pform
       tform(1:ntod)  = ch

!        IF (npix .LT. repeat_tmp) THEN  ! 2015-04-28
!           nrows = npix
!           ch = '1'//pform
!           tform(1:ntod) = ch
!        ENDIF
       ttype(1:ntod) = 'simulation'   ! will be updated
       tunit(1:ntod) = ''      ! optional, will not appear

       extname  = ''      ! optional, will not appear
       varidat  = 0


       CALL ftphbn(unit, nrows, tfields, ttype, tform, tunit, &
            &     extname, varidat, status)

       call FTMKYK(unit,'NAXIS2',nrows,'&', status) !lpl
       !     write the header literally, putting TFORM1 at the desired place
       comment = 'data format of field: 8-byte REAL'
       DO i=1,nlheader
          card = header(i)
          IF (card(1:5) == 'TTYPE') THEN ! if TTYPE1 is explicitely given
             stn = card(6:8)
             READ(stn,'(i3)') itn
             ! discard at their original location:
             call ftdkey(unit,'TTYPE'//stn,status)  ! old TTYPEi and
             status = 0
             call ftdkey(unit,'TFORM'//stn,status)  !     TFORMi
             status = 0
             if (itn <= tfields) then ! only put relevant information 2008-08-27
                call putrec(unit,header(i), status)    ! write new TTYPE1
                status = 0
                call ftpkys(unit,'TFORM'//stn,tform(itn),comment,status) ! and write new TFORM1 right after
                !CALL ftmcrd(unit,'TTYPE'//stn,'COMMENT',status)  ! old TTYPEi and 
                !CALL ftmcrd(unit,'TFORM'//stn,'COMMENT',status)  !     TFORMi
                !CALL ftprec(unit,header(i), status)           ! write new TTYPE1
                !CALL ftpkys(unit,'TFORM'//stn,tform(1),comment,status) ! and write new TFORM1 right after
             endif
          ELSEIF (header(i).NE.' ') THEN
             call putrec(unit,header(i), status)
!              CALL ftprec(unit,header(i), status)
          ENDIF
          status = 0
       ENDDO

    ELSE
       ! The file already exists
       readwrite=1
       CALL ftopen(unit,sfilename,readwrite,blocksize,status)
       CALL ftmahd(unit,2_i4b+extno_i,hdutype,status) 

       CALL ftgkys(unit,'TFORM1',tform(1),comment,status)
       CALL ftbnfm(tform(1),datacode,repeat_fits,width,status)

       IF (repeat_tmp .NE. repeat_fits) THEN
          if (present(repeat)) then 
             write(*,'(a)') code//'> WARNING: In file '//trim(sfilename)
             write(*,'(a)') &
                  & code//'>  user provided REPEAT value (' // &
                  & trim(adjustl(string(repeat_tmp)))       // &
                  & ') differs from value read from file (' // &
                  & trim(adjustl(string(repeat_fits)))      // &  
                  & ').'
             write(*,'(a)') code//'> The latter will be used.'
!           else
!              write(*,'(a,i0.0,a)') &
!                   & code//'> WARNING: REPEAT value read from file (', &
!                   & repeat_fits, &
!                   & ') will be used.'
          endif
          repeat_tmp = repeat_fits
       ENDIF

    ENDIF

    IF (npix .LT. nchunk_max) THEN ! data is small enough to be written in one chunk

       
       frow = (firstpix_tmp)/repeat_tmp + 1
       felem = firstpix_tmp-(frow-1)*repeat_tmp + 1
       npix_32 = npix 

       DO colnum = 1, ntod
          call f90ftpcl_(unit, colnum, Int(frow,i4b), Int(felem,i4b), npix_32, tod(0:npix_32-1,colnum), status)
       END DO

    ELSE ! data has to be written in several chunks

       q = (npix-1)/nchunk_max
       DO iq = 0,q
          IF (iq .LT. q) THEN
             npix_tmp = nchunk_max
          ELSE
             npix_tmp = npix - iq*nchunk_max
          ENDIF
          i0 = iq * nchunk_max
          i1 = i0 + npix_tmp - 1_i8b
          firstpix_chunk = firstpix_tmp + i0
          frow  = (firstpix_chunk)/repeat_tmp + 1
          felem =  firstpix_chunk-(frow-1)*repeat_tmp + 1
          npix_32 = npix_tmp
          if (i0 .le. 647947386 .and. i1 .ge. 647947386) then
             write(0,*) tod(647947382:647947386,1)
             write(0,*) i0 ,i1
             write(0,*) firstpix_tmp ,firstpix_chunk
             write(0,*) frow ,felem
             write(0,*) npix ,npix_tmp ,npix_32
!             stop
          end if
          if (npix .lt. 2**31) then
             DO colnum = 1, ntod
                call f90ftpcl_(unit, colnum, Int(frow,i4b), Int(felem,i4b), npix_32, &
                     &          tod(i0:i1, colnum), status)
             END DO
          else
             DO colnum = 1, ntod
                call f90ftpcl_(unit, colnum, Int(frow,i8b), Int(felem,i8b), Int(npix_32,i8b), &
                     &          tod(i0:i1, colnum), status)
             END DO
          end if
       ENDDO

    ENDIF

    ! pad entry if necessary ! 2015-04-28
    lastpix = firstpix_tmp + npix  ! number of pixels written above
    lengap = modulo(lastpix, repeat_tmp) ! remaining gap
    if (lengap > 0) then
       firstpix_tmp = lastpix
       npix_32 = repeat_tmp - lengap
       frow    = (firstpix_tmp)/repeat_tmp + 1
       felem   =  firstpix_tmp-(frow-1)*repeat_tmp + 1
       allocate(padding(0:npix_32-1))
       padding(:) = HPX_DBADVAL
       do colnum = 1, ntod
          call f90ftpcl_(unit, colnum, Int(frow,i8b), Int(felem,i8b), Int(npix_32,i8b), padding, status)
       enddo
       deallocate(padding)
    endif

    ! ----------------------
    ! close and exit
    ! ----------------------

    ! close the file and free the unit number
    CALL ftclos(unit, status)
    
    ! check for any error, and if so print out error messages
    IF (status .GT. 0) CALL printerror(status)

  END SUBROUTINE write_bintabh_i8b

  !----------------------------------------------------------------------------
  ! generic interface F90FTPCL_ for FITSIO's FTPCL[E,D,J,K]
  !           writes data in ASCTAB or BINTAB
  subroutine f90ftpcle(unit, colnum, frow, felem, np, data, status)
    integer(I4B), intent(in)  :: unit, colnum, frow, felem, np
    integer(I4B), intent(out) :: status
    real(SP),     intent(in), dimension(0:)  :: data
    call ftpcle(unit, colnum, frow, felem, np, data, status)
    return
  end subroutine f90ftpcle

  subroutine f90ftpcld(unit, colnum, frow, felem, np, data, status)
    integer(I4B), intent(in)  :: unit, colnum, frow, felem, np
    integer(I4B), intent(out) :: status
    real(DP),     intent(in), dimension(0:)  :: data
    call ftpcld(unit, colnum, frow, felem, np, data, status)
    return
  end subroutine f90ftpcld

  subroutine f90ftpcldll(unit, colnum, frow, felem, np, data, status)
    integer(I4B), intent(in)  :: unit, colnum
    integer(i8b), intent(in)  :: frow, felem, np
    integer(I4B), intent(out) :: status
    real(DP),     intent(in), dimension(0:)  :: data
    call ftpcldll(unit, colnum, frow, felem, np, data, status)
    return
  end subroutine f90ftpcldll


  
  subroutine f90ftpclj(unit, colnum, frow, felem, np, data, status)
    integer(I4B), intent(in)  :: unit, colnum, frow, felem, np
    integer(I4B), intent(out) :: status
    integer(I4B), intent(in), dimension(0:)  :: data
    call ftpclj(unit, colnum, frow, felem, np, data, status)
    return
  end subroutine f90ftpclj

  subroutine f90ftpclk(unit, colnum, frow, felem, np, data, status)
    integer(I4B), intent(in)  :: unit, colnum, frow, felem, np
    integer(I4B), intent(out) :: status
    integer(I8B), intent(in), dimension(0:)  :: data
    call ftpclk(unit, colnum, frow, felem, np, data, status)
    return
  end subroutine f90ftpclk


end module hpx_hack_mod



