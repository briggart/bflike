module bflike_types_mod

  use healpix_types
  use healpix_modules ,only : paramfile_handle ,parse_init ,parse_string &
       ,parse_lgt ,parse_int ,parse_double ,parse_finish ,parse_check_unused &
       ,parse_summarize
  use lpl_utils_mod ,only : myTT ,myTE ,myTB ,myEE ,myEB ,myBB ,maxlen &
       ,HPX_RING ,HPX_NEST
  implicit none

  private 

  public allocatable_matrix ,fit_params_nl ,fit_data_nl &
       ,bf_params ,tempfit_params ,bf_data ,tempfit_data ,binning

  type  allocatable_matrix

     integer  :: nrow
     integer  :: ncol
     logical  :: allocated
     real(dp) ,allocatable :: m(:,:)

   contains

     procedure :: alloc => alloc_am
     procedure :: clean => clean_am

  end type allocatable_matrix

  type :: bf_params

     character(maxlen) :: datafile ,basisfile ,maskfile ,beamfile ,mapfile &
          ,ncvmfile ,clfiducial ,binfile ,pwfdir
     integer(i4b)      :: nside ,nstokes ,numdata ,lmax ,lswitch ,nsims &
          ,iseed1 ,iseed2 ,iseed3 ,iseed4
     logical           :: use_datafile ,remove_mondip ,do_qml ,decouple_tp &
          ,orthogonal_eb ,fit_noise ,apply_pwf ,project_mondip ,do_linalm &
          ,force_lswitch ,bin_cls ,ncvm_is_masked ,ncvm_is_diagonal &
          ,pixel_pwf ,do_student ,debias
     real(dp)          :: ncvm_cf ,map_cf
     
   contains

     procedure :: set => set_bf_params_from_file
     
  end type bf_params

  type ,extends(bf_params) :: tempfit_params

     integer(i4b)      ,allocatable :: nstep(:) 
     integer(i4b)                   :: numtemp
     real(dp)          ,allocatable :: has_cmb(:) ,has_noise(:) &
          ,vmin(:) ,step(:) ,temp_cf(:) ,tncvm_cf(:)
     character(maxlen) ,allocatable :: NCVM_temp(:) ,templates(:)
     logical                        :: loglike = .false. ,multi_temp = .false.

   contains

     procedure :: set   => read_fit_params
     final     :: clean_fit_params
     
  end type tempfit_params
    
  type :: fit_params

     integer(i4b)       :: ntemplates = -1 ,lmax ,ndata = -1
     real(dp)           :: ncvm_cf ,data_cf
     real(dp) ,allocatable :: template_cf(:)
     character(maxlen)  :: maskfile ,ncvmfile ,clfile ,beamfile 
     character(maxlen) ,allocatable :: templates(:) ,datafile(:) 
     logical            :: project_mondip = .false. , multi_templ = .false.

     real(dp) ,allocatable          :: has_cmb(:) ,ncvm_templ_cf(:) ,has_noise(:) ,cmin(:) ,cstep(:)
     character(maxlen) ,allocatable :: NCVM_templ(:)
     integer(i4b)                   :: nstep
     logical                        :: loglike = .false.

   contains

     procedure::  set_fit_params
     generic  ::  set => set_fit_params

  end type fit_params

  type ,extends(fit_params) :: fit_params_nl
  !empty type, included for legacy reasons   

  end type fit_params_nl

  type bf_data
     
     real(dp) ,allocatable ,dimension(:,:) :: ncvm ,S ,dt
     
  end type bf_data
     
  type ,extends(bf_data) :: tempfit_data
     type(allocatable_matrix) ,allocatable :: ncvmt(:)
     real(dp)                 ,allocatable :: temps(:,:,:) ,tcov(:,:) 
  end type tempfit_data

  type ,extends(tempfit_data) :: fit_data_nl
  !empty type, included for legacy reasons   
     
  end type fit_data_nl

  integer ,parameter :: flat = 0, d_ell = 1
  type :: binning

     integer :: btype = -1 ,nb = -1

     !for now assume same bandpowers for all spectra
     integer ,allocatable :: lmin(:) ,lmax(:)
     real(dp),allocatable :: w(:) 

   contains

     procedure :: from_bins ,from_file
     generic   :: set => from_bins ,from_file

     procedure :: binCl_1D ,binCl_2D
     generic   :: binCl => binCl_1D ,binCl_2D

     procedure :: Cl2band_1D ,Cl2band_2D 
     generic   :: Cl2band => Cl2band_1D ,Cl2band_2D 

     procedure :: band2Cl_1D ,band2Cl_2D
     generic   :: band2Cl => band2Cl_1D ,band2Cl_2D

     procedure :: truncate

     procedure :: clean => deallocate_bin
     
  end type binning
  
contains

  subroutine deallocate_bin(self)
    class(binning) ,intent(inout) :: self

    self%nb    = -1
    self%btype = -1

    if (allocated(self%lmin)) deallocate(self%lmin) 
    if (allocated(self%lmax)) deallocate(self%lmax) 
    if (allocated(self%w))    deallocate(self%w) 
    
  end subroutine deallocate_bin

  
  subroutine truncate(self,lcut)
    class(binning) ,intent(inout) :: self
    integer        ,intent(in)    :: lcut

    integer ,allocatable :: lmin(:) ,lmax(:)
    integer              :: new_nb ,i ,bt
    
    if(self%nb .lt. 1 ) return 
    if(lcut .ge. self%lmax(self%nb) .or. &
         lcut .lt. self%lmin(1)) return !we want self%nb to be set 
    
    new_nb = 0
    do i = 1,self%nb
       if (lcut .ge. self%lmin(i)) then
          new_nb = new_nb +1
       else
          exit
       end if
    end do

    self%lmax(new_nb) = lcut

    bt = self%btype
    allocate(lmin, source = self%lmin(1:new_nb))
    allocate(lmax, source = self%lmax(1:new_nb))

    call self%set(bt,lmin,lmax)

  end subroutine truncate

  subroutine from_bins(self,btype,lmin,lmax)
    class(binning) ,intent(inout) :: self
    integer        ,intent(in)    :: btype ,lmin(:) ,lmax(:)

    integer  :: nb ,i ,l
    real(dp) :: norm

    call self%clean()
    select case(btype)
    case(flat,d_ell)
       !carry on
    case default
       write(*,*) "bandpowers type must be one of:"
       write(*,*) "0 : flat C_ell"
       write(*,*) "1 : l(l+1) C_ell"       
       stop 
    end select
    nb = size(lmin)

    if (nb .lt. 1) return
    self%nb    = nb
    self%btype = btype

    allocate(self%lmin , source = lmin)
    allocate(self%lmax , source = lmax)

    allocate(self%w(2:self%lmax(nb)) ,source = 0._dp)
    select case(btype)
    case(flat)
       do i = 1,nb
          self%w(lmin(i):lmax(i)) = 1._dp/(1 +lmax(i) -lmin(i))
       end do
    case(d_ell)
       do i = 1,nb
          do l = lmin(i),lmax(i) 
             self%w(l) = l*(l+1)
          end do
          norm = sum_of_squares(lmax(i)) -sum_of_squares(lmin(i)-1) &
               +(lmax(i)*(lmax(i)+1) -(lmin(i)-1)*lmin(i))/2  
          self%w(lmin(i):lmax(i)) = self%w(lmin(i):lmax(i))/norm 
       end do
    end select

  end subroutine from_bins
  
  subroutine from_file(self,filename)
    class(binning)   ,intent(inout) :: self
    character(len=*) ,intent(in)    :: filename

    integer              :: unit ,bt ,nb ,i 
    integer ,allocatable :: lmin(:) ,lmax(:)

    call self%clean()
    
    open(newunit=unit,file=filename,status='old',action='read')
    read(unit,'(i4)') bt

    select case(bt)
    case(flat,d_ell)
       !carry on
    case default
       write(*,*) "bandpowers type must be one of:"
       write(*,*) "0 : flat C_ell"
       write(*,*) "1 : l(l+1) C_ell"       
       stop 
    end select

    read(unit,'(i4)') nb

    allocate(lmin(nb) ,lmax(nb))
    do i = 1,nb
       read(unit,*) lmin(i) ,lmax(i)
    end do
    close(unit)

    call self%from_bins(bt,lmin,lmax)

  end subroutine from_file

  function binCl_2D(self,Cl,total_power) result(res)
    !returns array of C_ell filled with binned values
    class(binning),intent(in) :: self
    real(dp)      ,intent(in) :: cl(2:,:)
    logical       ,intent(in) ,optional :: total_power
    real(dp)     ,allocatable :: res(:,:)

    logical   :: tp
    integer   :: ltot ,ns ,i ,j ,li ,ls ,qb
    
    if( present(total_power)) then
       tp = total_power
    else
       tp = .false.
    end if
    
    ltot = size(cl(:,1)) -1
    ns   = size(cl(2,:))
    
    allocate(res(2:ltot,ns) ,source =0._dp)

    do j = 1,ns
       do i = 1,self%nb
          li = self%lmin(i)
          ls = min(self%lmax(i),ltot)
          if(tp) then
             qb = ls -li +1
          else
             qb = 1
          end if
          res(li:ls,j) = sum(self%w(li:ls)*cl(li:ls,j))*qb
       end do
    end do
    
  end function binCl_2D

  function binCl_1D(self,Cl,total_power) result(res)
    !returns array of C_ell filled with binned values
    class(binning),intent(in) :: self
    real(dp)      ,intent(in) :: cl(2:)
    logical       ,intent(in) ,optional :: total_power
    real(dp)     ,allocatable :: res(:)

    logical   :: tp
    integer   :: ltot ,i ,j ,li ,ls ,qb
    
    if( present(total_power)) then
       tp = total_power
    else
       tp = .false.
    end if
    
    ltot = size(cl(:)) -1
    
    allocate(res(2:ltot) ,source =0._dp)

    do i = 1,self%nb
       li = self%lmin(i)
       ls = min(self%lmax(i),ltot)
       if(tp) then
          qb = ls -li +1
       else
          qb = 1
       end if
       res(li:ls) = sum(self%w(li:ls)*cl(li:ls))*qb
    end do
    
  end function binCl_1D
  
  function Cl2band_2D(self,Cl,total_power) result(band)
    !bin C_ell into bandpowers                          
    class(binning) ,intent(in) :: self
    real(dp)       ,intent(in) :: cl(2:,:)
    logical ,optional ,intent(in) :: total_power
    real(dp)      ,allocatable :: band(:,:)

    logical   :: tp
    integer   :: ltot ,ns ,i ,j ,li ,ls ,qb
    
    ltot = size(cl(:,1)) -1
    ns   = size(cl(2,:))

    if (present(total_power)) then
       tp = total_power
    else
       tp = .false.
    end if
    
    allocate(band(self%nb,ns) ,source =0._dp)
    
    do j = 1,ns
       do i = 1,self%nb
          li = self%lmin(i)
          ls = min(self%lmax(i),ltot)
          if (tp) then
             qb = 1 +ls -li
          else
             qb = 1
          end if
          band(i,j) = qb*sum(self%w(li:ls)*cl(li:ls,j))
       end do
    end do
    
  end function Cl2band_2D
  
  function Cl2band_1D(self,Cl,total_power) result(band)
    !bin C_ell into bandpowers                          
    class(binning) ,intent(in) :: self
    real(dp)       ,intent(in) :: cl(2:)
    logical ,optional ,intent(in) :: total_power
    real(dp)      ,allocatable :: band(:)

    logical   :: tp
    integer   :: ltot ,ns ,i ,j ,li ,ls ,qb

    ltot = size(cl(:)) +1

    allocate(band(self%nb) ,source =0._dp)

    if (present(total_power)) then
       tp = total_power
    else
       tp = .false.
    end if

    do i = 1,self%nb
       li = self%lmin(i)
       ls = min(self%lmax(i),ltot)
       if (tp) then
          qb = 1 +ls -li
       else
          qb = 1
       end if
       band(i) = qb*sum(self%w(li:ls)*cl(li:ls))
    end do

  end function Cl2band_1D

  function band2Cl_2D(self,band) result(Cl)
    !expand bandpowers into C_ell array                          
    class(binning) ,intent(in) :: self
    real(dp)       ,intent(in) :: band(:,:)
    real(dp)      ,allocatable :: cl(:,:)

    integer :: lmax ,ns ,i ,j ,li ,ls
    
    lmax = self%lmax(self%nb)
    ns   = size(band(1,:))

    allocate(Cl(2:lmax,ns) ,source =0._dp)

    do j = 1,ns
       do i = 1,self%nb
          li = self%lmin(i)
          ls = self%lmax(i)
          cl(li:ls,j) = band(i,j)
       end do
    end do

  end function band2Cl_2D

  function band2Cl_1D(self,band) result(Cl)
    !expand bandpowers into C_ell array                          
    class(binning) ,intent(in) :: self
    real(dp)       ,intent(in) :: band(:)
    real(dp)      ,allocatable :: cl(:)

    integer :: lmax ,i ,j ,li ,ls
    
    lmax = self%lmax(self%nb)

    allocate(Cl(2:lmax) ,source =0._dp)

    do i = 1,self%nb
       li = self%lmin(i)
       ls = self%lmax(i)
       cl(li:ls) = band(i)
    end do

  end function band2Cl_1D

  function sum_of_squares(n) result(tot)
    !returns the sum of the squares of first n integers
    integer(i4b)  ,intent(in) :: n
    integer(i8b)              :: tot

    tot = n*(n+1_i8b)*(2*n+1_i8b)
    tot = tot/6
    
  end function sum_of_squares
    
  subroutine read_fit_params(self,parfile)

    class(tempfit_params) ,intent(out) :: self
    character(len=*) ,intent(in)       :: parfile

    type(paramfile_handle)             :: handle
    integer(i4b)                       :: i ,nt ,neff ,unit
    real(dp)                           :: vmax
    character(len=2)                   :: tag
    character(maxlen)                  :: lonchar
    
    !read the basic parameters 
    call set_bf_params_from_file(self,parfile)

    !now read the additional parameters we need for the template fit
    handle        = parse_init(parfile)
    self%numtemp  = parse_int(handle,'num_templates')

    self%multi_temp = parse_lgt(handle,'multi_template',.false.)
    self%loglike    = parse_lgt(handle,'loglike',.false.)
    lonchar  = parse_string(handle,'list_templates')

    nt = self%numtemp
    
    allocate(self%temp_cf(nt),self%has_cmb(nt),&
         self%has_noise(nt),self%vmin(nt),self%nstep(nt),self%step(nt),&
         self%ncvm_temp(nt),self%tncvm_cf(nt))
    do i = 1,nt
       write(tag,'(i2.2)') i
       self%temp_cf(i)   = parse_double(handle,'template_cf_'//tag)
       self%has_cmb(i)   = parse_double(handle,'has_cmb_'//tag)
       self%has_noise(i) = parse_double(handle,'has_noise_'//tag)
       self%vmin(i)      = parse_double(handle,'min_'//tag)
       vmax              = parse_double(handle,'max_'//tag)
       self%nstep(i)     = parse_int(handle,'num_steps_'//tag,1)
       self%step(i)      = (vmax -self%vmin(i))/self%nstep(i)
       self%ncvm_temp(i) = parse_string(handle,'template_ncvm_'//tag,'')
       self%tncvm_cf(i)  = parse_double(handle,'template_ncvm_cf_'//tag,0._dp)
    end do
    call parse_summarize(handle)
    call parse_finish(handle)

    if (self%multi_temp) then
       neff = self%numtemp*self%numdata
    else
       neff = self%numtemp
    end if
    allocate(self%templates(neff))
    open(newunit=unit,file=trim(lonchar),status='old',action='read')
    do i = 1,neff
       read(unit,'(a)') self%templates(i)
    end do
       
  end subroutine read_fit_params

  subroutine clean_fit_params(self)

    type(tempfit_params) ,intent(inout) :: self

     if(allocated(self%nstep))     deallocate(self%nstep)
     if(allocated(self%has_cmb))   deallocate(self%has_cmb)
     if(allocated(self%has_noise)) deallocate(self%has_noise)
     if(allocated(self%vmin))      deallocate(self%vmin)
     if(allocated(self%step))      deallocate(self%step)
     if(allocated(self%temp_cf))   deallocate(self%temp_cf)
     if(allocated(self%tncvm_cf))  deallocate(self%tncvm_cf)
     if(allocated(self%ncvm_temp)) deallocate(self%ncvm_temp)

   end subroutine clean_fit_params

    
  subroutine set_fit_params(P,ntemplates,ndata,maskfile,datafile,ncvmfile,&
       clfile,templates,lmax,ncvm_cf,data_cf,template_cf,project_mondip,&
       beamfile,multi_template,ncvm_templ ,has_cmb ,has_noise ,ncvm_templ_cf &
       ,cmin ,cstep ,nstep ,loglike)
    class(fit_params) ,intent(inout)       :: P
    integer(i4b) ,intent(in) ,optional     :: ntemplates ,lmax ,ndata
    character(len=*) ,intent(in) ,optional :: maskfile ,datafile(:) ,ncvmfile &
         ,clfile ,templates(:) ,beamfile
    real(dp) ,intent(in) ,optional         :: ncvm_cf ,data_cf ,template_cf(:)
    logical ,intent(in) ,optional          :: project_mondip ,multi_template
    integer(i4b) :: nt

    character(len=*) ,intent(in) ,optional :: ncvm_templ(:) 
    real(dp)         ,intent(in) ,optional :: has_cmb(:) ,has_noise(:) ,ncvm_templ_cf(:) ,cmin(:) ,cstep(:) 
    integer(i4b)     ,intent(in) ,optional :: nstep     
    logical          ,intent(in) ,optional :: loglike


    if(present(ntemplates)) P%ntemplates = ntemplates
    if(present(ndata))    P%ndata = ndata
    if(present(maskfile)) P%maskfile = maskfile
    if(present(ncvmfile)) P%ncvmfile = ncvmfile
    if(present(beamfile)) P%beamfile = beamfile
    if(present(clfile))   P%clfile   = clfile
    if(present(project_mondip)) P%project_mondip = project_mondip

    if(present(templates)) then
       nt = size(templates(:))
       if (nt .ne. P%ntemplates .and. .not. P%multi_templ) then
          write(*,*) 'number of template maps does not match ntemplates '
          write(*,*) 'setting ntemplates = ',nt
          P%ntemplates = nt
       end if
       if(allocated(P%templates)) deallocate(P%Templates)
       allocate(P%templates,source=templates)
    end if

    if(present(datafile)) then
       nt = size(datafile(:))
       if (nt .ne. P%ndata) then
          write(*,*) 'number of data maps  does not match ndata ',P%ndata
          write(*,*) 'setting ndata = ',nt
          p%ndata = nt
       end if
       if(allocated(P%datafile)) deallocate(P%datafile)
       allocate(P%datafile,source =datafile)
    end if

    if(present(multi_template)) P%multi_templ = multi_template
    if(present(lmax)) P%lmax = lmax
    if(present(ncvm_cf)) P%ncvm_cf = ncvm_cf
    if(present(data_cf)) P%data_cf = data_cf

    if(present(template_cf)) then
       nt = size(template_cf(:))
       if (nt .ne. P%ntemplates) stop  'number of template_cf  does not match ntemplatess'
       if(allocated(P%template_cf)) deallocate(P%Template_cf)
       allocate(P%template_cf,source = template_cf)
    end if

    if(present(ncvm_templ_cf)) then
       nt = size(ncvm_templ_cf(:))
       if (nt .ne. P%ntemplates) stop  'number of ncvm_templ_cf  does not match ntemplatess'
       if(allocated(P%ncvm_templ_cf)) deallocate(P%ncvm_templ_cf)
       allocate(P%ncvm_templ_cf,source=ncvm_templ_cf)
    end if

    if(present(has_cmb)) then
       nt = size(has_cmb(:))
       if (nt .ne. P%ntemplates) stop  'number of has_cmb  does not match ntemplatess'
       if(allocated(P%has_cmb)) deallocate(P%has_cmb)
       allocate(P%has_cmb,source=has_cmb)
    end if

    if(present(has_noise)) then
       nt = size(has_noise(:))
       if (nt .ne. P%ntemplates) stop  'number of has_noise  does not match ntemplates'
       if(allocated(P%has_noise)) deallocate(P%has_noise)
       allocate(P%has_noise,source=has_noise)
    end if

    if(present(cmin)) then
       nt = size(cmin(:))
       if (nt .ne. P%ntemplates) stop  'number of cmin  does not match ntemplates'
       if(allocated(P%cmin)) deallocate(P%cmin)
       allocate(P%cmin,source=cmin)
    end if

    if(present(cstep)) then
       nt = size(cstep(:))
       if (nt .ne. P%ntemplates) stop  'number of cstep  does not match ntemplates'
       if(allocated(P%cstep)) deallocate(P%cstep)
       allocate(P%cstep ,source=cstep)
    end if

    if(present(ncvm_templ)) then
       nt = size(ncvm_templ(:))
       if (nt .ne. P%ntemplates) stop  'number of ncvm_templ  does not match ntemplates'
       if(allocated(P%ncvm_templ)) deallocate(P%ncvm_templ)
       allocate(P%ncvm_templ,source=ncvm_templ)
    end if

    if(present(nstep))   P%nstep   = nstep
    if(present(loglike)) P%loglike = loglike

  end subroutine set_fit_params

  subroutine alloc_am(self,nrow,ncol)
    class(allocatable_matrix) ,intent(inout) :: self
    integer ,intent(in)                     :: nrow,ncol

    if(self%allocated) call self%clean()

    self%nrow = nrow
    self%ncol = ncol
    allocate(self%m(nrow,ncol) ,source = 0._dp)

    self%allocated = .true.

  end subroutine alloc_am

  subroutine clean_am(self)
    class(allocatable_matrix) ,intent(inout) :: self
    if(.not. self%allocated) return

    deallocate(self%m)

    self%nrow = -1
    self%ncol = -1
    self%allocated = .false.

  end subroutine clean_am

  subroutine set_bf_params_from_file(self,parfile)

    class(bf_params) ,intent(out) :: self
    character(len=*) ,intent(in)  :: parfile
    
    type(paramfile_handle)        :: handle

    handle            = parse_init(parfile)

    self%datafile     = parse_string(handle,'datafile','')
    self%basisfile    = parse_string(handle,'basisfile','')
    self%use_datafile = parse_lgt(handle,'use_datafile',.false.)

    if (.not. self%use_datafile) then
       self%nside     = parse_int(handle,'nside')
       self%nstokes   = parse_int(handle,'nstokes')
       self%numdata   = parse_int(handle,'numdata')
       self%maskfile  = parse_string(handle,'maskfile')
       self%beamfile  = parse_string(handle,'beamfile')
       self%mapfile   = parse_string(handle,'mapfile')
       self%ncvmfile  = parse_string(handle,'ncvmfile')
       self%ncvm_cf   = parse_double(handle,'ncvm_cf')
       self%map_cf    = parse_double(handle,'map_cf')
       self%apply_pwf = parse_lgt(handle,'apply_pixwin',.true.)
       self%pixel_pwf = parse_lgt(handle,'anisotropic_pixwin',.false.)
       self%pixel_pwf = self%pixel_pwf .and. self%apply_pwf
       self%remove_mondip = parse_lgt(handle,'remove_mondip',.false.)
       self%ncvm_is_masked   = parse_lgt(handle,'ncvm_is_masked',.false.)
       self%ncvm_is_diagonal = parse_lgt(handle,'ncvm_is_diagonal',.false.)    
    end if

    self%debias   = parse_lgt(handle,'remove_noise_bias',.true.)
    
    if(self%pixel_pwf) then
       self%pwfdir = parse_string(handle,'pixwin_dir')
    else
       self%pwfdir = ''
    end if
    
    self%force_lswitch  = parse_lgt(handle,'force_lswitch',.false.)
    self%clfiducial     = parse_string(handle,'clfiducial')
    self%project_mondip = parse_lgt(handle,'project_mondip',.false.)
    self%lmax           = parse_int(handle,'lmax')
    self%lswitch        = parse_int(handle,'lswitch')

    self%do_qml        = parse_lgt(handle,'do_qml',.false.)
    self%fit_noise     = parse_lgt(handle,'fit_noise',.false.)
    if (self%fit_noise) stop 'fit noise not supported yet'

    if (self%do_qml) then
       self%decouple_tp   = parse_lgt(handle,'decouple_tp',.false.)
       self%orthogonal_eb = parse_lgt(handle,'orthogonal_eb',.false.)
       self%bin_cls       = parse_lgt(handle,'bin_cl',.false.)
       if(self%bin_cls) self%binfile = parse_string(handle,'binfile')
    end if

    self%do_linalm  = parse_lgt(handle,'do_linalm',.false.)

    self%do_student = parse_lgt(handle,'do_student',.false.)
    if (self%do_student) then
       self%nsims    = parse_int(handle,'nsims',-1)
       self%iseed1   = parse_int(handle,'iseed1',101)
       self%iseed2   = parse_int(handle,'iseed2',6131)
       self%iseed3   = parse_int(handle,'iseed3',11621)
       self%iseed4   = parse_int(handle,'iseed4',102161)
    end if
    
    call parse_summarize(handle)
    call parse_finish(handle)

  end subroutine set_bf_params_from_file
  
end module bflike_types_mod
