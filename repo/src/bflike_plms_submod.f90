submodule (bflike_smw_mod) bflike_plms_smod

contains

  module subroutine compute_modes(vals,vecs,blocks,file,lstart)
    implicit none
    !compute legendre (associated) functions and mask them
    type(allocatable_matrix) ,intent(inout) :: vals(2:) ,vecs(2:) ,blocks(2:,2:)
    character(len=*) ,intent(in) ,optional :: file
    integer(i4b)     ,intent(in) ,optional :: lstart

    real(dp) ,allocatable :: plms(:,:) ,maps(:,:,:,:) ,theta(:) ,phi(:) &
         ,y(:,:) ,v(:) ,tm1(:,:) ,tm2(:,:) ,evec(:,:) ,smphi(:) ,cmphi(:)
    real(dp)              :: sumT ,sumD ,sumS ,xnorm ,vnorm ,phase &
         ,enorm ,bnorm
    
    integer(i4b) :: nsmax ,nlmax ,nmmax ,n_plm ,npol ,nrings ,nchunk ,nl0 &
         ,i ,id ,ipol ,j ,l ,istart ,istop ,nl ,ntheta ,m ,switch ,ntot &
         ,n_tmpl ,idEE ,idBB 
    integer(i4b) ,allocatable :: list(:,:) ,nri(:) 
    
    nsmax = mask%s%nside
    nlmax = lswitch
    nmmax = lswitch
    n_plm = nsmax*(nmmax +1)*(2*nlmax -nmmax+2)
    nchunk = nlmax +1 +(nlmax*(nlmax+1))/2
    ntot  = mask%ntot
    
    if(has_p) then
       npol = 3
    else
       npol = 1
    end if

    allocate(plms(0:n_plm-1,1:npol))

!    if(present(pixel_pwf)) then
!       ppwf = pixel_pwf
!    else
!       ppwf = .false.
!    end if
    
!    if (ppwf) then
       !we use RING internally, convert to final ordering when applying mask
!       call pixel2templates(nsmax,HPX_RING,pix_temp)
!       call read_pixel_pwf(nsmax,pwfdir,pwf,nlmax)
!    else
!       n_tmpl = nside2ntemp(nsmax)
!       allocate(pwf(0:nlmax,3,0:n_tmpl-1),source = 1._dp)
!    end if

    !should modify to compute plms only for unmasked pixels
    !rather than full sky 
    call plm_gen( nsmax, nlmax, nmmax, plms)
    
    !now map them to the sphere...
    
    allocate(theta(0:mask%s%npix-1) ,phi(0:mask%s%npix-1))
    
    do i = 0,mask%s%npix -1 
       call pix2ang_ring(nsmax, i, theta(i), phi(i))
    end do

    nrings = 4*nsmax - 1
    allocate(list(0:nrings,nrings))
    allocate(nri(nrings))
    do i = 1,nrings
       call in_ring(nsmax,i,0._dp,pi,list(:,i),nri(i))
    end do

    allocate(maps(0:mask%s%npix-1,0:nlmax,0:nmmax,1:npol))
    allocate(v(nchunk))

    ntheta = 2*nsmax 
    do ipol = 1,npol
       istart = 0
       if(ipol .eq. 3) then
          switch = -1
       else
          switch = 1
       end if
       do i = 1,ntheta
          istop = istart +(nchunk-1)
          v(1:nchunk) = plms(istart:istop,ipol)
          id    = 0
          do m = 0,nmmax
             do l = m,nlmax
                id = id +1
                maps(list(0:nri(i)-1,i)                  ,l,m,ipol) = &
                     v(id)*pwf(l,ipol,pix_temp(list(0:nri(i)-1,i))) 
                maps(list(0:nri(nrings+1-i)-1,nrings+1-i),l,m,ipol) = &
                     v(id)*switch*(-1)**(l+m)*pwf(l,ipol,pix_temp(list(0:nri(nrings+1-i)-1,nrings+1-i))) 
             end do
          end do
          istart = istop+1
       end do
    end do
    deallocate(plms,v,nri,list)

    allocate(y(0:mask%s%npix-1,npol) ,source = 0._dp)

    if(has_t) then
!$OMP PARALLELDO DEFAULT(private) SHARED(nlmax,clnorm,maps,vals,vecs,phi,mask,off,blocks) SCHEDULE(dynamic,1)
       do l = 2,nlmax
          !for each l we store modes in order of m = 0,-1,+1,-2,+2,...,-l,+l
          id = 1
          xnorm = clnorm(l,myTT)*fourpi/(2*l+1)
 
          !m = 0
          y(:,1)   = maps(:,l,0,1)
          y(:,2:3) = 0._dp
          call reorder_map(y,from=1,to=mask%s%ordering)
          v      = map2vec(y)
          call mask_vec(mask%lmask(0:npol*mask%s%npix-1),v)
          vals(l)%m(id,1) = xnorm

          vecs(l)%m(:,id) = v
          
          do m = 1,l
             !negative m
             id       = id+1
             y(:,1)   = sqrt(2._dp)*maps(:,l,m,1)*sin(m*phi)*(-1)**m
             !y(:,2:3) = 0._dp
             call reorder_map(y,from=1,to=mask%s%ordering)
             v      = map2vec(y)
             call mask_vec(mask%lmask(0:npol*mask%s%npix-1),v)
             vals(l)%m(id,1) = xnorm
             vecs(l)%m(:,id) = v

             !positive m
             id       = id+1
             y(:,1)   = sqrt(2._dp)*maps(:,l,m,1)*cos(m*phi)*(-1)**m
             !y(:,2:3) = 0._dp
             call reorder_map(y,from=1,to=mask%s%ordering)
             v      = map2vec(y)
             call mask_vec(mask%lmask(0:npol*mask%s%npix-1),v)
             vals(l)%m(id,1) = xnorm
             vecs(l)%m(:,id) = v
          end do
       end do
!       deallocate(v)
    end if

    
    if (has_p) then

       !EE
!$OMP PARALLELDO DEFAULT(private) SHARED(nlmax,clnorm,maps,vals,vecs,phi,mask,off,blocks) SCHEDULE(dynamic,1)
       do l = 2,nlmax
          nl0 = 2*l+1 
!          id  =  off   *nl0 +1
!          xnorm = clnorm(l,myEE)*fourpi/(2*l+1)
          enorm = clnorm(l,myEE)*fourpi/(2*l+1)
          bnorm = clnorm(l,myBB)*fourpi/(2*l+1)

          !m = 0
          y(:,1) = 0._dp
          y(:,2) =  -maps(:,l,0,2)
          y(:,3) =   maps(:,l,0,3)
          call reorder_map(y,from=1,to=mask%s%ordering)

          idEE  =  off   *nl0 +1
          v      = map2vec(y)
          call mask_vec(mask%lmask,v)
          vecs(l)%m(:,idEE) = v
          vals(l)%m(idEE,1) = enorm
          blocks(l,myEE)%m(idEE,idEE) = vals(l)%m(idEE,1)

          
          idBB  = (off+1)*nl0 +1
          !if we forced equal Q and U mask, we could KCS after masking
          v      = map2vec(y,EtoB=.true.)
          call mask_vec(mask%lmask,v)
          vecs(l)%m(:,idBB) = v
          vals(l)%m(idBB,1) = bnorm
          blocks(l,myBB)%m(idBB,idBB) = vals(l)%m(idBB,1)

          do m = 1,l
             !negative m
             phase  = sqrt(2._dp)*(-1)**m
             !y(:,1) = 0._dp
             smphi  = sin(m*phi)
             cmphi  = cos(m*phi)
             y(:,2) =  -phase*maps(:,l,m,2)*smphi
             y(:,3) =   phase*maps(:,l,m,3)*cmphi
             call reorder_map(y,from=1,to=mask%s%ordering)

             idEE   = idEE+1
             v      = map2vec(y)
             call mask_vec(mask%lmask,v)
             vecs(l)%m(:,idEE) = v
             vals(l)%m(idEE,1) = enorm
             blocks(l,myEE)%m(idEE,idEE) = vals(l)%m(idEE,1)

             idBB   = idBB+1
             v      = map2vec(y,EtoB = .true.)
             call mask_vec(mask%lmask,v)
             vecs(l)%m(:,idBB) = v
             vals(l)%m(idBB,1) = bnorm
             blocks(l,myBB)%m(idBB,idBB) = vals(l)%m(idBB,1)
             
             !positive m
             !y(:,1) = 0._dp
             y(:,2) =  -phase*maps(:,l,m,2)*cmphi
             y(:,3) =  -phase*maps(:,l,m,3)*smphi
             call reorder_map(y,from=1,to=mask%s%ordering)

             idEE   = idEE+1
             v      = map2vec(y)
             call mask_vec(mask%lmask,v)
             vecs(l)%m(:,idEE) = v
             vals(l)%m(idEE,1) = enorm
             blocks(l,myEE)%m(idEE,idEE) = vals(l)%m(idEE,1)

             idBB   = idBB+1
             v      = map2vec(y,EtoB = .true.)
             call mask_vec(mask%lmask,v)
             vecs(l)%m(:,idBB) = v
             vals(l)%m(idBB,1) = bnorm
             blocks(l,myBB)%m(idBB,idBB) = vals(l)%m(idBB,1)

          end do

          xnorm = clnorm(l,myEB)*fourpi/(2*l+1)
          do m = 1,nl0
             blocks(l,myEB)%m(off*nl0+m,(off+1)*nl0+m) = xnorm
             blocks(l,myEB)%m((off+1)*nl0+m,m+off*nl0) = xnorm
          end do

       end do
       
     !BB
!!$OMP PARALLELDO DEFAULT(private) SHARED(nlmax,clnorm,maps,vals,vecs,phi,mask,off,blocks) SCHEDULE(dynamic,1)
!       do l = 2,nlmax
!          nl0 = 2*l+1
!          id  = (off+1)*nl0 +1
!          xnorm = clnorm(l,myBB)*fourpi/(2*l+1)
!          !m = 0
!          y(:,1) = 0._dp
!          y(:,2) = -maps(:,l,0,3)
!          y(:,3) = -maps(:,l,0,2)
!          call reorder_map(y,from=1,to=mask%s%ordering)
!          v      = map2vec(y)
!          call mask_vec(mask%lmask,v)
!          vecs(l)%m(:,id) = v
!          vals(l)%m(id,1) = xnorm
!          blocks(l,myBB)%m(id,id) = vals(l)%m(id,1)
!
!          do m = 1,l
!             !negative m
!             phase  = sqrt(2._dp)*(-1)**m
!             id     = id+1
!             !y(:,1) = 0._dp
!             smphi  = sin(m*phi)
!             cmphi  = cos(m*phi)
!             y(:,2) = -phase*maps(:,l,m,3)*cmphi
!             y(:,3) = -phase*maps(:,l,m,2)*smphi
!             call reorder_map(y,from=1,to=mask%s%ordering)
!             v      = map2vec(y)
!             call mask_vec(mask%lmask,v)
!             vecs(l)%m(:,id) = v
!             vals(l)%m(id,1) = xnorm
!             blocks(l,myBB)%m(id,id) = vals(l)%m(id,1)

!             !positive m
!             id     = id+1
!             !y(:,1) = 0._dp
!             y(:,2) =  phase*maps(:,l,m,3)*smphi
!             y(:,3) = -phase*maps(:,l,m,2)*cmphi
!             call reorder_map(y,from=1,to=mask%s%ordering)
!             v      = map2vec(y)
!             call mask_vec(mask%lmask,v)
!             vecs(l)%m(:,id) = v
!             vals(l)%m(id,1) = xnorm
!             blocks(l,myBB)%m(id,id) = vals(l)%m(id,1)
!          end do
!       end do

       !EB                                      
!!$OMP PARALLELDO DEFAULT(private) SHARED(nlmax,clnorm,maps,vals,vecs,phi,mask,off,blocks) SCHEDULE(dynamic,1)
!       do l = 2,nlmax
!          nl0 = 2*l+1
!          nl  = (off+2)*nl0
!          xnorm = clnorm(l,myEB)*fourpi/(2*l+1)
!          do m = 1,nl0
!             blocks(l,myEB)%m(off*nl0+m,(off+1)*nl0+m) = xnorm
!             blocks(l,myEB)%m((off+1)*nl0+m,m+off*nl0) = xnorm
!          end do
!       end do

    end if

    deallocate(maps)

    if (has_x) then
!$OMP PARALLELDO DEFAULT(private) SHARED(nlmax,clnorm,maps,vals,vecs,phi,mask,off,blocks) SCHEDULE(dynamic,1)
       do l= 2,nlmax
          nl0 = 2*l+1         
          
          !TE
          blocks(l,myTE)%m = 0._dp
          xnorm = clnorm(l,myTE)*fourpi/(2*l+1)
          do m = 1,nl0
             blocks(l,myTE)%m(nl0+m,m) = xnorm
             blocks(l,myTE)%m(m,m+nl0) = xnorm
          end do
             
          !TB                                                
          blocks(l,myTB)%m(:,:) = 0._dp
          xnorm = clnorm(l,myTB)*fourpi/(2*l+1)
          do m = 1,nl0
             blocks(l,myTB)%m(2*nl0+m,m) = xnorm
             blocks(l,myTB)%m(m,m+2*nl0) = xnorm
          end do
       end do

    end if

    if (present(file)) call write_plms_to_fits(file,clnorm,mask%Tvec,mask%Qvec,mask%Uvec,vals,vecs,blocks)
    
  end subroutine compute_modes

! ---------------------------------------------------------------------------

  module subroutine write_plms_to_fits(file,clnorm,Tvec,Qvec,Uvec,eval,evec&
       ,blocks)
    
    implicit none
    character(len=*)         ,intent(in) :: file
    real(dp)                 ,intent(in) :: clnorm(2:,:) ,Tvec(:,:) &
         ,Qvec(:,:) ,Uvec(:,:)
    type(allocatable_matrix) ,intent(in) :: eval(2:) ,evec(2:) ,blocks(2:,2:)

    integer(i4b)                         :: flsw ,l ,fntot ,nl ,j &
         ,extno ,lmax ,nfield ,nwrite
    character(len=80)                    :: header(nlheader) = ''
    character(filenamelen)               :: xfile
    character(:)            ,allocatable :: tag 
    character(len=6)                     :: dump 
    real(dp)                ,allocatable :: tmat(:,:)

    
    lmax = size(eval) +1
    
    call add_card(header,'NSIDE',mask%s%nside)
    call add_card(header,'NTEMP',ntemp)
    call add_card(header,'NQ'   ,nq)
    call add_card(header,'NU'   ,nu)
    call add_card(header,'LMAX' ,lmax)

    !always overwrite
    if (file(1:1) .eq. '!') then
       xfile = trim(file)
    else
       xfile = '!'//trim(file)
    end if

    !start writing

    !write normalization in first extension
    call add_card(header,'CONTAINS','clnorm')
    flsw  = lmax
    extno = 0
    nfield = 6
    do j = 1,nfield
       !need different names for each field to prevent healpy from getting all workedup
       write(dump,'(I0)') j
       tag = trim(adjustl(dump))
       call add_card(header,'TTYPE'//tag,'data_'//tag)
    end do
    call write_bintabh(clnorm(2:lmax,1:6), int(lmax-1,i8b), nfield, header, nlheader, xfile, &
         extno=extno)
    
    !write coordinates in second extension
    call add_card(header,'CONTAINS','coords',update=.true.)
    allocate(tmat(3,0:ntot-1))
    extno = 1
    tmat(:,0:ntemp-1)        = Tvec(:,1:ntemp)
    tmat(:,ntemp:ntemp+nq-1) = Qvec(:,1:nq)
    tmat(:,ntemp+nq:ntot-1)  = Uvec(:,1:nu)
    nfield = 3
    do j = 1,nfield
       write(dump,'(I0)') j
       tag = trim(adjustl(dump))
       call add_card(header,'TTYPE'//tag,'data_'//tag,update=.true.)
    end do
!    tmat = transpose(tmat)
    call write_bintabh(transpose(tmat), int(ntot,i8b), nfield, header, nlheader, &
         xfile, extno=extno ,repeat=ntot)
    deallocate(tmat)
    
    !now write each l in a different extenstion
    !blocks are mostly 0, so this is inefficient, but most space is taken
    !up by eigenvectors so this should do for now
    do l = 2,lmax
       extno = extno+1 
       call add_card(header,'ELL',l,update=.true.)
       nl = 2*l+1
       if(has_x) then
          flsw  = 3*nl
          fntot = ntot +6*flsw
          allocate(tmat(0:fntot-1,flsw),source = 0._dp)
          do j = 1,nl
             tmat(j-1,j) = eval(l)%m(j,1)
          end do
          do j = 2,6
             tmat((j-1)*flsw:j*flsw-1,1:flsw) = blocks(l,j)%m(1:flsw,1:flsw)
          end do
          tmat(6*flsw:fntot-1,:)   = evec(l)%m
          nwrite = int(fntot,i8b)*int(flsw,i8b)
          tmat = reshape(tmat(0:fntot-1,1:flsw),[nwrite,1])
 
          call add_card(header,'FNTOT',fntot,update=.true.)
          call add_card(header,'FLSW',flsw,update=.true.)
          call write_bintabh(tmat, int(nwrite,i8b), 1, header, &
               nlheader, xfile, extno=extno ,repeat=nwrite)
       elseif (has_p) then      
          flsw = 2*nl
          !we need only EE ,BB and EB blocks
          fntot = ntot +3*flsw
          allocate(tmat(0:fntot-1,flsw),source = 0._dp)

          tmat(0:flsw-1,:)        = blocks(l,myEE)%m
          tmat(flsw:2*flsw-1,:)   = blocks(l,myBB)%m
          tmat(2*flsw:3*flsw-1,:) = blocks(l,myEB)%m
          tmat(3*flsw:fntot-1,:)  = evec(l)%m
          nwrite = fntot*flsw
          tmat = reshape(tmat(0:fntot-1,1:flsw),[nwrite,1])
          call add_card(header,'FNTOT',fntot,update=.true.)
          call add_card(header,'FLSW',flsw,update=.true.)
          call write_bintabh(tmat, int(nwrite,i8b), 1, header, &
               nlheader, xfile, extno=extno ,repeat=nwrite)
       else
          flsw = nl
          !we need only TT
          fntot = ntot +flsw
          allocate(tmat(0:fntot-1,flsw),source = 0._dp)
          do j = 1,nl
             tmat(j-1,j) = eval(l)%m(j,1)
          end do
          tmat(flsw:fntot-1,:)   = evec(l)%m
          nwrite = fntot*flsw
          tmat = reshape(tmat(0:fntot-1,1:flsw),[nwrite,1])
          call add_card(header,'FNTOT',fntot,update=.true.)
          call add_card(header,'FLSW',flsw,update=.true.)
          call write_bintabh(tmat, int(nwrite,i8b), 1, header, &
               nlheader, xfile, extno=extno ,repeat=nwrite)
       end if
       deallocate(tmat)
    end do

  end subroutine write_plms_to_fits

! ---------------------------------------------------------------------------

  module logical function read_plms_from_fits(file, eval, evec, blocks ,lfound)&
       result(OK)

    implicit none
    character(len=*)        ,intent(in)   :: file
    type(allocatable_matrix),intent(inout):: eval(2:) ,evec(2:) ,blocks(2:,2:)
    integer(i4b)            ,intent(out)  :: lfound

    real(dp)  ,allocatable :: tmat(:,:) ,scale(:,:) ,xtra(:,:) 
    real(dp)               :: nullval ,test
    integer(i4b)           :: flsw ,l ,fntot ,nl ,j ,extno ,lmax ,ltodo &
         ,myunit ,blocksize ,err ,readwrite ,hdutype ,ndum
    integer(i8b)           :: nread
    character(len=80)      :: comment
    logical                :: skip ,anynull
    
    ok = .false.
    
    do myunit = 100,1000
       inquire(unit=myunit, opened=skip)
       if(.not. skip) exit
    end do

    readwrite  = 0 ;err = 0
    call ftopen(myunit,file,readwrite,blocksize,err)
    if (err .ne. 0 ) return

    call ftmrhd(myunit, +1, hdutype, err)
    if (err .ne. 0 ) return

    call ftgkyj(myunit,'NTEMP',ndum ,comment ,err)
    if (err .ne. 0 .or. ndum .ne. ntemp) return

    call ftgkyj(myunit,'NQ',ndum ,comment ,err)
    if (err .ne. 0 .or. ndum .ne. nq) return

    call ftgkyj(myunit,'NU',ndum ,comment ,err)
    if (err .ne. 0 .or. ndum .ne. nu) return

    call ftgkyj(myunit,'LMAX',lfound ,comment ,err)
    call stop_on_fits_error(err,'LMAX')

    lmax  = size(eval) +1
    ltodo = min(lfound,lmax)

    allocate(scale(2:lfound,6))

    !read normalization
    extno = 0

    call read_bintab(file, scale(2:lfound,1:6), lfound-1, 6, nullval ,anynull ,extno=extno)
    scale(2:ltodo,1:6) = clnorm(2:ltodo,1:6)/scale(2:ltodo,1:6)
    where (.not.(abs(scale) .lt. huge(1.d0))) scale = 0._dp
    !read coordinates
    extno = 1
    allocate(tmat(ntot,3))
    call read_bintab(file, tmat, ntot, 3, nullval ,anynull ,extno=extno)
    if(has_t) then 
       test = sum(abs(transpose(tmat(1:ntemp,1:3)) -Tvec(1:3,1:ntemp)))
       if (test .gt. 1.d-12) then
          write(*,*) 'stored basis T mask does not match'
          return
       end if
    end if
    if(has_p) then
       test = sum(abs(transpose(tmat(ntemp+1:ntemp+nq,1:3)) -Qvec(1:3,1:nq)))
       if (test .gt. 1.d-12) then
          write(*,*) 'stored basis Q mask does not match'
          return
       end if
       test = sum(abs(transpose(tmat(ntemp+nq+1:ntot,1:3)) -Uvec(1:3,1:nu)))
       if (test .gt. 1.d-12) then
          write(*,*) 'stored basis U mask does not match'
          return
       end if
    end if
    deallocate(tmat)
 
    do l = 2,ltodo
       !now read each l from a different extension
       extno = extno+1 
       nl    = 2*l+1
 
       if(has_x) then
          flsw  = 3*nl
          fntot = ntot +6*flsw
          nread = int(flsw,i8b)*int(fntot,i8b)
          allocate(xtra(0:nread-1,1))
          call read_bintab(file, xtra, nread, 1, nullval ,anynull &
               ,extno=extno)
          allocate(tmat(0:fntot-1,flsw),source=reshape(xtra,[fntot,flsw]))
          do j = 1,nl
             eval(l)%m(j,1) = tmat(j-1,j)*scale(l,1) 
          end do
          do j = 2,6
             blocks(l,j)%m  = tmat((j-1)*flsw:j*flsw-1,:)*scale(l,j)
          end do
          evec(l)%m = tmat(6*flsw:fntot-1,:)  

          do j = 1,nl
             eval(l)%m(j+nl,1) = blocks(l,myEE)%m(j+nl,j+nl)*scale(l,myEE) 
          end do
          do j = 1,nl
             eval(l)%m(j+2*nl,1) = blocks(l,myBB)%m(j+2*nl,j+2*nl)* &
                  scale(l,myBB) 
          end do
       elseif (has_p) then      
          flsw = 2*nl
          !we need only EE ,BB and EB blocks
          fntot = ntot +3*flsw
          nread = int(flsw,i8b)*int(fntot,i8b)
          allocate(xtra(0:nread-1,1))
          call read_bintab(file, xtra, nread, 1, nullval ,anynull &
               ,extno=extno)
          allocate(tmat(0:fntot-1,flsw),source=reshape(xtra,[fntot,flsw]))
!          allocate(tmat(0:fntot-1,flsw))
!          call read_bintab(file, tmat, int(fntot,i8b), flsw, nullval ,anynull &
!               ,extno=extno)
          blocks(l,myEE)%m = tmat(0:flsw-1,:)*scale(l,myEE)        
          blocks(l,myBB)%m = tmat(flsw:2*flsw-1,:)*scale(l,myBB)         
          blocks(l,myEB)%m = tmat(2*flsw:3*flsw-1,:)*scale(l,myEB) 
          evec(l)%m        = tmat(3*flsw:fntot-1,:) 
          do j = 1,nl
             eval(l)%m(j,1) = blocks(l,myEE)%m(j,j)*scale(l,myEE) 
          end do
          do j = 1,nl
             eval(l)%m(j+nl,1) = blocks(l,myBB)%m(j+nl,j+nl)* &
                  scale(l,myBB) 
          end do
       else
          flsw = nl
          !we need only TT
          fntot = ntot +flsw
          nread = int(flsw,i8b)*int(fntot,i8b)
          allocate(xtra(0:nread-1,1))
          call read_bintab(file, xtra, nread, 1, nullval ,anynull &
               ,extno=extno)
          allocate(tmat(0:fntot-1,flsw),source=reshape(xtra,[fntot,flsw]))
          do j = 1,nl
             eval(l)%m(j,1) = tmat(j-1,j)*scale(l,myTT) 
          end do
          evec(l)%m = tmat(flsw:fntot-1,:)
       end if
       deallocate(xtra,tmat)
    end do

    call ftclos(myunit, err)
    call stop_on_fits_error(err,'Closing fileplm file')
    
    ok = .true.
    
  end function read_plms_from_fits

! ---------------------------------------------------------------------------

  module logical function read_plms_external(file, lmax, eval, evec, blocks ,norm &
       ,lmask) result(OK)
    
    implicit none
    character(len=*)                   ,intent(in)   :: file
    integer(i4b)                       ,intent(out)  :: lmax
    type(allocatable_matrix),allocatable,intent(out) :: eval(:) ,evec(:) &
         ,blocks(:,:)
    real(dp)              ,allocatable ,intent(out)  :: norm(:,:)
    logical               ,allocatable ,intent(out)  :: lmask(:)
    
    real(dp)  ,allocatable :: tmat(:,:) ,xtra(:,:) 
    real(dp)               :: nullval ,test
    integer(i4b)           :: flsw ,l ,fntot ,nl ,j ,extno ,ltodo &
         ,myunit ,blocksize ,err ,readwrite ,hdutype ,ndum ,nside ,ntemp &
         ,nq ,nu ,ipix ,ntot ,nstokes
    integer(i8b)           :: nread ,npix
    character(len=80)      :: comment
    logical                :: skip ,anynull ,has_t ,has_p ,has_x
    
    ok = .false.
    
    do myunit = 100,1000
       inquire(unit=myunit, opened=skip)
       if(.not. skip) exit
    end do

    readwrite  = 0 ;err = 0
    call ftopen(myunit,file,readwrite,blocksize,err)
    if (err .ne. 0 ) return

    call ftmrhd(myunit, +1, hdutype, err)
    if (err .ne. 0 ) return

    call ftgkyj(myunit,'NSIDE',nside ,comment ,err)
    if (err .ne. 0 ) return

    call ftgkyj(myunit,'NTEMP',ntemp ,comment ,err)
    if (err .ne. 0 ) return

    call ftgkyj(myunit,'NQ',nq ,comment ,err)
    if (err .ne. 0 ) return

    call ftgkyj(myunit,'NU',nu ,comment ,err)
    if (err .ne. 0 ) return

    call ftgkyj(myunit,'LMAX',lmax ,comment ,err)
    call stop_on_fits_error(err,'LMAX')

    has_t = ntemp .gt. 0
    has_p = nq + nu .gt. 0
    has_x = has_t .and. has_p

    ntot  = ntemp +nq +nu

    nstokes = 0
    if(ntemp .gt. 0) nstokes = nstokes +1
    if(nq+nu .gt. 0) nstokes = nstokes +2
    
    call init_modes(lmax,ntot,nstokes,eval,evec,blocks)

    allocate(norm(2:lmax,6))

    npix = 12*nside**2
    allocate(xtra(0:npix-1,3) ,source = 0._dp)

    !read normalization
    extno = 0

    call read_bintab(file, norm(2:lmax,1:6), lmax-1, 6, nullval ,anynull ,extno=extno)

    !read coordinates
    extno = 1
    allocate(tmat(ntot,3))
    call read_bintab(file, tmat, ntot, 3, nullval ,anynull ,extno=extno)
    if(has_t) then
       do j = 1,ntemp
          call vec2pix_nest(nside,tmat(j,1:3),ipix)
          xtra(ipix,1) = 1._dp
       end do
    end if
    if(has_p) then
       do j = ntemp+1,ntemp+nq
          call vec2pix_nest(nside,tmat(j,1:3),ipix)
          xtra(ipix,2) = 1._dp
       end do
       do j = ntemp+nq+1,ntot
          call vec2pix_nest(nside,tmat(j,1:3),ipix)
          xtra(ipix,3) = 1._dp
       end do
    end if
    
    lmask = map2vec(xtra .gt. 0.5_dp)
    deallocate(xtra,tmat)
     
    do l = 2,lmax
       !now read each l from a different extension
       extno = extno+1 
       nl    = 2*l+1
 
       if(has_x) then
          flsw  = 3*nl
          fntot = ntot +6*flsw
          nread = int(flsw,i8b)*int(fntot,i8b)
          allocate(xtra(0:nread-1,1))
          call read_bintab(file, xtra, nread, 1, nullval ,anynull &
               ,extno=extno)
          allocate(tmat(0:fntot-1,flsw),source=reshape(xtra,[fntot,flsw]))
          do j = 1,nl
             eval(l)%m(j,1) = tmat(j-1,j)
          end do
          do j = 2,6
             blocks(l,j)%m  = tmat((j-1)*flsw:j*flsw-1,:)
          end do
          evec(l)%m = tmat(6*flsw:fntot-1,:)  

          do j = 1,nl
             eval(l)%m(j+nl,1) = blocks(l,myEE)%m(j+nl,j+nl)
          end do
          do j = 1,nl
             eval(l)%m(j+2*nl,1) = blocks(l,myBB)%m(j+2*nl,j+2*nl)
          end do
       elseif (has_p) then      
          flsw = 2*nl
          !we need only EE ,BB and EB blocks
          fntot = ntot +3*flsw
          nread = int(flsw,i8b)*int(fntot,i8b)
          allocate(xtra(0:nread-1,1))
          call read_bintab(file, xtra, nread, 1, nullval ,anynull &
               ,extno=extno)
          allocate(tmat(0:fntot-1,flsw),source=reshape(xtra,[fntot,flsw]))
!          allocate(tmat(0:fntot-1,flsw))
!          call read_bintab(file, tmat, int(fntot,i8b), flsw, nullval ,anynull &
!               ,extno=extno)
          blocks(l,myEE)%m = tmat(0:flsw-1,:)
          blocks(l,myBB)%m = tmat(flsw:2*flsw-1,:)
          blocks(l,myEB)%m = tmat(2*flsw:3*flsw-1,:)
          evec(l)%m        = tmat(3*flsw:fntot-1,:) 
          do j = 1,nl
             eval(l)%m(j,1) = blocks(l,myEE)%m(j,j)
          end do
          do j = 1,nl
             eval(l)%m(j+nl,1) = blocks(l,myBB)%m(j+nl,j+nl)
          end do
       else
          flsw = nl
          !we need only TT
          fntot = ntot +flsw
          nread = int(flsw,i8b)*int(fntot,i8b)
          allocate(xtra(0:nread-1,1))
          call read_bintab(file, xtra, nread, 1, nullval ,anynull &
               ,extno=extno)
          allocate(tmat(0:fntot-1,flsw),source=reshape(xtra,[fntot,flsw]))
          do j = 1,nl
             eval(l)%m(j,1) = tmat(j-1,j)
          end do
          evec(l)%m = tmat(flsw:fntot-1,:)
       end if
       deallocate(xtra,tmat)
    end do

    call ftclos(myunit, err)
    call stop_on_fits_error(err,'Closing plms file')
    
    ok = .true.
    
  end function read_plms_external

! ---------------------------------------------------------------------------
  
  module subroutine init_modes(lmax,ntot,nstokes,evec,eval,blocks)
    implicit none
    integer ,intent(in)                                :: lmax ,ntot ,nstokes
    type(allocatable_matrix) ,allocatable ,intent(out) :: evec(:) ,eval(:) &
         ,blocks(:,:)

    integer  :: l ,j

    allocate(evec(2:lmax),eval(2:lmax),blocks(2:lmax,2:6))
    do l=2,lmax
       call evec(l)%alloc(nrow=ntot,ncol=nstokes*(2*l+1))
       call eval(l)%alloc(nrow=nstokes*(2*l+1),ncol=1)
       do j=2,6
          call blocks(l,j)%alloc(nrow=nstokes*(2*l+1),ncol=nstokes*(2*l+1))
       end do
    end do

  end subroutine init_modes
  
end submodule bflike_plms_smod
