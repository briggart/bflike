module debugging_mod

  implicit none
  logical ,parameter :: debugging = .true.

end module debugging_mod

module bflike_smw_mod

  use healpix_types
  use healpix_modules ,only : getsize_fits ,convert_ring2nest ,input_map  &
       ,fits2cl ,pix2vec_ring ,pix2vec_nest ,pixel_window ,read_dbintab   &
       ,printerror ,remove_dipole ,add_card ,write_bintabh ,read_bintab &
       ,del_card ,in_ring ,plm_gen ,pix2ang_ring ,vec2pix_nest  ,input_tod
  use rngmod
  use debugging_mod
  use bflike_types_mod
  use lpl_utils_mod 
  use hpx_hack_mod
  use omp_lib
  implicit none
  
  public init_pix_like ,clean_pix_like ,get_pix_loglike ,get_qml_estimate &
       ,get_qmlm_estimate ,get_template_coeff ,map2vec ,mask_matrix &
       ,mask_vec ,init_template_fit ,get_template_loglike_mcmc ,read_map &
       ,read_plms_external ,get_linalm ,get_student_loglike_tp

#ifdef GFORTRAN
 public update_ncvm ,precompute_rotation_angle ,write_plms_to_fits ,get_pp_cov ,get_xx_cov ,get_tt_cov ,compute_clnorm 
#endif
 
  private

  interface

     module subroutine init_template_fit(parfile,T)

       implicit none
       character(len=*)      ,intent(in)  :: parfile
       class(tempfit_params) ,intent(out) :: T

     end subroutine

     module subroutine get_template_coeff(T,coeff,cov,chi2)

       implicit none
       class(tempfit_params) ,intent(in)  :: T
       real(dp) ,intent(out) ,allocatable :: coeff(:,:) ,cov(:,:) ,chi2(:,:)

     end subroutine

     module subroutine get_template_loglike_mcmc(P,D,coeff,lglike)

       implicit none
       type(fit_params_nl) ,intent(in)    :: P
       type(fit_data_nl)   ,intent(inout) :: D
       real(dp)            ,intent(in)    :: coeff(:)
       real(dp)            ,intent(out)   :: lglike

     end subroutine

     module subroutine compute_modes(vals,vecs,blocks,file,lstart)

       implicit none
       !compute legendre (associated) functions and mask them                  
       type(allocatable_matrix) ,intent(inout) :: vals(2:) ,vecs(2:) &
            ,blocks(2:,2:)
       character(len=*) ,intent(in) ,optional  :: file 
       integer(i4b)     ,intent(in) ,optional  :: lstart 

     end subroutine compute_modes

     module subroutine write_plms_to_fits(file,clnorm,Tvec,Qvec,Uvec,eval,evec&
       ,blocks)

       implicit none
       character(len=*)         ,intent(in) :: file
       real(dp)                 ,intent(in) :: clnorm(2:,:) ,Tvec(:,:) &
            ,Qvec(:,:) ,Uvec(:,:)
       type(allocatable_matrix) ,intent(in) :: eval(2:) ,evec(2:) ,blocks(2:,2:)
     end subroutine

     module logical function read_plms_from_fits(file, eval, evec, blocks &
          ,lfound) result(OK)

       implicit none
       character(len=*)        ,intent(in)   :: file
       type(allocatable_matrix),intent(inout):: eval(2:) ,evec(2:) ,blocks(2:,2:)
       integer(i4b)            ,intent(out)  :: lfound

     end function read_plms_from_fits
     
     module logical function read_plms_external(file, lmax, eval, evec, blocks ,norm &
          ,lmask) result(OK)

       implicit none
       character(len=*)                   ,intent(in)   :: file
       integer(i4b)                       ,intent(out)  :: lmax
       type(allocatable_matrix),allocatable,intent(out) :: eval(:) ,evec(:) &
            ,blocks(:,:)
       real(dp)              ,allocatable ,intent(out)  :: norm(:,:)
       logical               ,allocatable ,intent(out)  :: lmask(:)
       
     end function read_plms_external

     module subroutine init_modes(lmax,ntot,nstokes,evec,eval,blocks)

       implicit none
       integer ,intent(in)                             :: lmax ,ntot ,nstokes
       type(allocatable_matrix),allocatable,intent(out):: evec(:) ,eval(:) &
            ,blocks(:,:)

     end subroutine init_modes

     module subroutine get_linalm(linalm,cl,map,ncm)

       implicit none
       real(dp)           ,allocatable ,intent(out) :: linalm(:,:,:)
       real(dp) ,optional ,allocatable ,intent(out) :: map(:,:,:)
       real(dp) ,optional ,allocatable ,intent(out) :: cl(:,:,:)
       real(dp) ,optional ,allocatable ,intent(out) :: ncm(:,:)
       
     end subroutine get_linalm
     
     module subroutine modes2matrix(eval, evec, mat)

       implicit none
       type(allocatable_matrix) ,intent(in)  :: evec(2:) ,eval(2:)
       real(dp)                 ,intent(out) :: mat(:,:)

     end subroutine modes2matrix
     
  end interface
   
  integer(i4b) ,parameter   :: nlheader = 256
  integer(i4b) ,save        :: ntemp ,ntot ,lmax ,lswitch ,ndata ,nq ,nqu ,nu &
       ,nsp = 0 ,off = 0 ,nsims
  integer(i4b) ,allocatable :: pix_temp(:)
  
  real(dp) ,allocatable ,save :: pwf(:,:,:)

  real(dp) ,allocatable ,dimension(:,:) ,save :: clnorm ,dt ,auxdt ,cls ,S &
       ,N0 ,NCVM ,cos1 ,cos2 ,sin1 ,sin2 ,Tvec ,Qvec ,Uvec ,Ylm ,Ylm_full
  real(dp) ,allocatable ,dimension(:)   ,save :: reflike ,pl ,plm ,f1 ,f2
  type(allocatable_matrix) ,allocatable ,save :: fevec(:) ,feval(:) &
       ,cblocks(:,:) 

  logical        ,save :: decouple_tp = .false. ,orthogonal_eb = .false. &
       ,has_t = .false. ,has_p = .false. ,has_x = .false. ,debias = .true. 
  type(sky_mask) ,save :: mask
  type(binning)  ,save :: bins

  type(planck_rng) ,save :: bfl_handle

  
contains

  subroutine create_dataset(P)

    implicit none
    type(bf_params) ,intent(inout) :: P

    integer(i4b)           :: i ,unit ,rcl ,repeat ,j
    integer(i8b)           :: nwrite ,il ,iu
    real(dp) ,allocatable  :: dt(:,:) ,cov(:,:) ,map(:,:) ,v(:) ,bl(:,:) &
         ,hw(:,:)
    real(dp)               :: mondip(4)
    character(len=80)      :: header(nlheader) = '' ,bheader(nlheader) = ''
    character(len=8)       :: date
    character(len=10)      :: time
    character(filenamelen) :: filename ,lonstring

    !if no datafile is given, set a name based on date    
    if (trim(P%datafile) .eq. '') then
       call date_and_time(date=date,time=time)
       P%datafile = "BFLIKE_SCRATCH_"//date//"_"//time//".fits"
    end if
    
    ! read mask    
    call mask%set(P%maskfile)
    if(P%nside .ne. mask%s%nside)     stop "Mask has wrong nside"
    if(P%nstokes .ne. mask%s%nstokes) stop "Mask has wrong nstokes"

    P%lmax = min(P%lmax, 4*P%nside)
    
    ! add cards to header    
    call add_card(header,'NSIDE',P%nside)
    call add_card(header,'NSTOKES',P%nstokes)
    call add_card(header,'MASK',trim(P%maskfile))
    call add_card(header,'NTEMP',mask%ntemp)
    call add_card(header,'NQ',mask%nq)
    call add_card(header,'NU',mask%nu)
    call add_card(header,'NUMDATA',P%numdata)
    call add_card(header,'MAP',trim(P%mapfile))
    call add_card(header,'MAP_CF',P%map_cf)
    call add_card(header,'REMOVE_MD',P%remove_mondip)
    call add_card(header,'NCVM',trim(P%ncvmfile))
    call add_card(header,'NCVM_CF',P%ncvm_cf)
    call add_card(header,'BEAM',trim(P%beamfile))
    call add_card(header,'APPLY_PIXWIN',P%apply_pwf)
    call add_card(header,'PIXEL_PIXWIN',P%pixel_pwf)
    if(P%do_student) call add_card(header,'NSIMS',P%nsims)

    !process data
    allocate(dt(mask%ntot,P%numdata))
    if(P%numdata .eq. 1) then
       open(newunit=unit,status='scratch')
       write(unit,'(a)') trim(P%mapfile)
       rewind(unit)
    else
       open(newunit=unit,file=trim(P%mapfile),status='old',action='read')
    end if

    do i = 1, P%numdata
       read(unit,'(a)') filename
       call read_map(filename,map,mask%s)

       if(P%remove_mondip) &
            call remove_dipole(P%nside,map(:,1),mask%s%ordering,2,mondip,[-1.d0,1.d0],mask=mask%m(:,1))

       v = map2vec(map)

       call mask_vec(mask%lmask,v)
       dt(:,i) = v
    end do
    close(unit) ;deallocate(map,v)
    dt = dt*P%map_cf
    
    !process the NCVM
    !assume ncvm is fortran direct for now

    if(P%ncvm_is_masked) then
       allocate(cov(mask%ntot,mask%ntot))
       inquire(iolength=rcl) cov(:,1)
       open(newunit=unit,file=trim(P%ncvmfile),status='old',action='read',form='unformatted',access='direct',recl=rcl)
       if(P%ncvm_is_diagonal) then
          v = cov(:,1)
          read(unit,rec=1) v
          do i = 1,mask%ntot
             cov(i,i) = v(i)
          end do
          deallocate(v)
       else
          do i = 1,mask%ntot
             read(unit,rec=i) cov(:,i)
          end do
       end if
       close(unit)
    else
       allocate(v(3*mask%s%npix))
       inquire(iolength=rcl) v
       open(newunit=unit,file=trim(P%ncvmfile),status='old',action='read',form='unformatted',access='direct',recl=rcl)

       if(P%ncvm_is_diagonal) then
          allocate(cov(mask%ntot,mask%ntot),source = 0._dp)
          read(unit,rec=1) v
          call mask_vec(mask%lmask,v)
          do i = 1,mask%ntot
             cov(i,i) = v(i)
          end do
       else
          allocate(cov(3*mask%s%npix,3*mask%s%npix))
          do i = 1,3*mask%s%npix
             read(unit,rec=i) cov(:,i)
          end do
          call mask_matrix(mask%lmask,cov)
       end if
       deallocate(v)
       close(unit)
    end if
    cov = cov*P%ncvm_cf

!    do i = 1,mask%ntot
!       do j = i,mask%ntot
!          cov(j,i) = (mask%ntot +1_i8b)*i -i*(i+1_i8b)/2 +j
!end do
!    end do
!    write(0,*) 'NOOOOOOOOOOOOOOOOOOOOO!!!!'
    
    !process the beam
    allocate(bl(0:4*P%nside,6),source=0._dp)
    call fits2cl(P%beamfile,bl(0:P%lmax,1:6),P%lmax,6,bheader)
    if(P%apply_pwf) then
       allocate(hw(0:4*P%nside,3))
       call pixel_window(hw,P%nside)
       bl(:,1) = bl(:,1)*hw(:,1)
       bl(:,2) = bl(:,2)*hw(:,2)
       bl(:,3) = bl(:,3)*hw(:,3)
       bl(:,4) = bl(:,4)*sqrt(hw(:,1)*hw(:,2))
       bl(:,5) = bl(:,5)*sqrt(hw(:,1)*hw(:,3))
       bl(:,6) = bl(:,6)*sqrt(hw(:,2)*hw(:,3))
       deallocate(hw)
    end if

    !find out how much stuff we need to write
    !pix vectors +symmetric NCVM +numdata maps +6 beam components (l>=2)
    associate(nti8b => int(mask%ntot,i8b))
      nwrite = (3+P%numdata)*nti8b +(nti8b*(nti8b+1))/2 &
           +6*(4*P%nside-1)
    end associate
    call add_card(header,'NWRITE',nwrite)

    !pack everything into single vector
    allocate(map(nwrite,1))

    iu = 0
    !coordinates
    do i = 1, mask%ntemp
       il = iu +1
       iu = il +2
       map(il:iu,1) = mask%Tvec(:,i)
    end do
    do i = 1, mask%nq
       il = iu +1
       iu = il +2
       map(il:iu,1) = mask%Qvec(:,i)
    end do
    do i = 1, mask%nu
       il = iu +1
       iu = il +2
       map(il:iu,1) = mask%Uvec(:,i)
    end do
       
    !beam
    do i = 1, 6
       il = iu +1
       iu = il +4*P%nside-2
       map(il:iu,1) = bl(2:4*P%nside,i)  
    end do

    !NCVM
    do i = 1, mask%ntot
       il = iu +1
       iu = il +(mask%ntot -i)
       map(il:iu,1) = cov(i:mask%ntot,i)
    end do
    !maps
    do i = 1, P%numdata
       il = iu +1
       iu = il +(mask%ntot -1)
       map(il:iu,1) = dt(:,i)
    end do
    
    call findrepeat(nwrite,repeat)

    !write stuff to file
    if (P%datafile(1:1) .eq. '!') then
       call write_bintabh_i8b(map, int(nwrite,i8b), 1, header, nlheader, &
          trim(P%datafile),repeat=repeat,firstpix=0_i8b)
    else
       call write_bintabh_i8b(map, int(nwrite,i8b), 1, header, nlheader, &
            '!'//trim(P%datafile),repeat=repeat,firstpix=0_i8b)
    end if

    write(*,*) "Dataset has been saved in: "//trim(P%datafile)
    
    !should be automatic
    deallocate(dt,cov)

  contains

    subroutine findrepeat(nwrite,repeat)

      integer(i8b) ,intent(in)  :: nwrite
      integer(i4b) ,intent(out) :: repeat
      integer(i4b) ,parameter   :: nprime = 24
      integer(i8b) ,parameter   :: primes(nprime) = [2, 3, 5, 7, 11, 13,&
           17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 73, 79, 83,&
           89, 97]
      integer(i8b) :: swrite
      
      swrite = nwrite
      
      repeat = 1

      ploop: do i = 1,nprime
         do while (mod(swrite,primes(i)) .eq. 0)
            repeat = repeat * primes(i)
            swrite = swrite / primes(i)
            if (repeat .ge. 12000) exit ploop
         end do
      end do ploop
      
    end subroutine findrepeat
    
  end subroutine create_dataset

! ---------------------------------------------------------------------------
  
  subroutine read_dataset(P)

    implicit none
    type(bf_params) ,intent(inout) :: P

    integer(i4b)                 :: myunit ,i ,blocksize &
         ,readwrite ,hdutype ,err ,nside
    integer(i8b)                 :: nwrite ,il ,iu
    real(dp) ,allocatable        :: tmp(:,:) ,bl(:,:)
    real(dp)                     :: nullval
    logical                      :: skip ,anynull
    character(len=80)            :: comment

    do myunit = 100,1000
       inquire(unit=myunit, opened=skip)
       if(.not. skip) exit
    end do

    readwrite  = 0 ;err = 0
    call ftopen(myunit,P%datafile,readwrite,blocksize,err)
    call stop_on_fits_error(err,'opening dataset file')

    call ftmrhd(myunit, +1, hdutype, err)
    call stop_on_fits_error(err,'HDR')

    call ftgkyj(myunit,'NSIDE',nside ,comment ,err)
    call stop_on_fits_error(err,'NSIDE')

    call ftgkyj(myunit,'NTEMP',ntemp ,comment ,err)
    call stop_on_fits_error(err,'NTEMP')

    call ftgkyj(myunit,'NQ',nq ,comment ,err)
    call stop_on_fits_error(err,'NQ')

    call ftgkyj(myunit,'NU',nu ,comment ,err)
    call stop_on_fits_error(err,'NU')

    if(P%do_student) then
       call ftgkyj(myunit,'NSIMS',nsims ,comment ,err)
       call stop_on_fits_error(err,'NU')
    else
       nsims = -1
    end if

    if(P%use_datafile) then
       !it's possible to reconstruct lmask from pixel coordinates,
       !but this is easier
       call ftgkys(myunit,'MASK',P%maskfile ,comment ,err)
       call stop_on_fits_error(err,'MASK')
       call mask%set(P%maskfile)
    end if

    nqu  = nq +nu
    ntot = ntemp +nqu

    has_t = ntemp .gt. 0
    has_p = nqu   .gt. 0
    has_x = has_t .and. has_p

    !nsp = Number of Stokes Params
    nsp = 0      ;off = 0
    if (has_t) then
       off = 1
       nsp = nsp +1
    end if
    if (has_p) nsp = nsp +2
    
    call ftgkyj(myunit,'NUMDATA',ndata ,comment ,err)
    call stop_on_fits_error(err,'NUMDATA')

    call ftgkyk(myunit,'NWRITE',nwrite ,comment ,err)
    call stop_on_fits_error(err,'NWRITE')
    
    allocate(tmp(0:nwrite-1,1))
    call input_tod(P%datafile, tmp, nwrite, 1, fmissval=hpx_dbadval)

    if(anynull) then
       write(*,*) 'Missing pixels have been given the value ',nullval
    end if

    !this is redundant with sky_mask, but will do for now
    !allocate(Tvec(3,ntemp) ,Qvec(3,nq) ,Uvec(3,nu))

    !iu = -1 !tmp starts at 0

    !coordinates
    !do i = 1 ,ntemp
    !   il = iu +1
    !   iu = il +2
    !   Tvec(:,i) = tmp(il:iu,1)
    !end do
    !do i = 1 ,nq
    !   il = iu +1
    !   iu = il +2
    !   Qvec(:,i) = tmp(il:iu,1)
    !end do
    !do i = 1 ,nu
    !   il = iu +1
    !   iu = il +2
    !   Uvec(:,i) = tmp(il:iu,1)
    !end do

    iu = 3*mask%ntot -1 !tmp starts at 0
        
    !beam
    allocate(bl(0:4*nside,6) ,source=0._dp)
    do i = 1,6
       il = iu +1
       iu = il +4*nside-2
       bl(2:4*nside,i) = tmp(il:iu,1)
    end do
    allocate(clnorm(2:lmax,6) ,source=0._dp)
    call compute_clnorm(bl,clnorm)
    deallocate(bl)
    
    allocate(ncvm(ntot,ntot), source = -1._dp)
    do i = 1 ,ntot
       il = iu +1
       iu = il +(ntot -i)
       ncvm(i:ntot,i) = tmp(il:iu,1) 
!       write(900,*) i,il,ncvm(i,i),ncvm(i+1,i)
    end do
!    stop
    
    allocate(dt(ntot,ndata))
    do i = 1 ,ndata
       il = iu +1
       iu = il +(ntot -1)
       dt(:,i) = tmp(il:iu,1) 
    end do
    deallocate(tmp,stat=err)
    if(err .ne. 0) then
       write(*,*) 'failed buffer deallocation in read_dataset'
    end if
    
    call ftclos(myunit, err)
    if (err > 0) call printerror(err)

    if (P%datafile(1:15) .eq. "BFLIKE_SCRATCH_") then
       !clean up after yourself
       open(newunit = myunit ,file=trim(P%datafile) ,status='old') 
       close(myunit ,status='delete')
    end if
    
    P%nside   = nside
    P%numdata = ndata
    
  end subroutine read_dataset

! ---------------------------------------------------------------------------
  
  subroutine init_pix_like(parfile,passlmax,passlswitch,numdata)

    implicit none
    character(len=*) ,intent(in)  :: parfile
    integer(i4b)     ,intent(out) :: passlmax ,passlswitch ,numdata
 
    integer(i4b)          :: i ,j ,l ,l1 ,l2 ,nside ,info ,neigen
    integer(i8b)          :: il ,iu
    real(dp) ,allocatable :: evec(:,:) ,clstmp(:,:) 
    logical               :: do_qml ,do_linalm
    type(bf_params)       :: BFP
    type(sky_mask) ,allocatable :: tmpmask
    integer(i8b)          :: rcl
    
    call BFP%set(parfile)
    lmax    = BFP%lmax
    lswitch = BFP%lswitch

    ! for Nside <= 16 we could just compute this on the fly 
    if (.not. BFP%use_datafile) then
       call create_dataset(BFP)
    end if
    call read_dataset(BFP)

    nside   = BFP%nside
    ndata   = BFP%numdata
    do_qml  = BFP%do_qml
    do_linalm = BFP%do_linalm

    !debuggin parameter for QML
    debias = BFP%debias
    
    if(lswitch .gt. 2.5*nside .and. .not.BFP%force_lswitch) then
       lswitch = 2.5*nside
       write(*,*) 'lswitch must be <= 2.5nside'
       write(*,*) 'setting lswitch  =',lswitch
    end if
    passlmax    = lmax
    passlswitch = lswitch
    numdata     = ndata
    
    if (BFP%do_student) then
       nsims = BFP%nsims
       if (nsims .le. nsp*(lswitch+1)**2) then
          stop "number or simulations must be > number of harmonic modes "
       end if
       call rand_init(bfl_handle,BFP%iseed1,BFP%iseed2,BFP%iseed3,BFP%iseed4)
    end if

    if(nsp*(lswitch+1)**2 .gt. ntot .and. .not.BFP%bin_cls) &
         stop 'number of modes must be lower than number of pixels: try a lower lswitch'

!   global quantities, used throught the code
    allocate(cls(2:lmax,6) ,source = 0._dp) 

!   temp storage used in code initialization
    allocate(pl(1:lmax) ,source = 0._dp) ;allocate(plm ,source = pl)
    allocate(f1(2:lmax) ,source = 0._dp) ;allocate(f2  ,source = f1)

!   precompute the rotation angles
!    call precompute_rotation_angle(sin1,cos1,sin2,cos2)

!   pixel-dependent window function
    call pixel2templates(nside,mask%s%ordering,pix_temp)
    if (BFP%pixel_pwf) then
       call read_pixel_pwf(nside,trim(BFP%pwfdir),pwf,lmax)
    else
       allocate(pwf(0:lmax,3,0:nside2ntemp(nside)-1),source = 1._dp)
    end if    
    
!   update the NCVM
    allocate(clstmp(2:lmax,6), source = 0._dp) 
    call read_cls_from_file(trim(BFP%clfiducial),clstmp) 
    
    if(do_qml) then
       if(BFP%bin_cls) then
          call bins%set(BFP%binfile)
          call bins%truncate(lswitch)
          do l = 2,lswitch
             clstmp(l,:) = clstmp(l,:)*twopi/(l*(l+1))
          end do
          clstmp(2:lswitch,:) = bins%binCl(clstmp(2:lswitch,:))
          do l = 2,lswitch
             clstmp(l,:) = clstmp(l,:)*(l*(l+1))/twopi
          end do
       end if
       !allocate(N0,source=NCVM)
       !To keep memory usege down at Nside=64
       allocate(N0((Int(ntot,i8b)*Int(ntot+1,i8b))/2,1),source = 0._dp)
       iu = 0
       do i = 1,ntot
          il = iu+1
          iu = il +(ntot-i)
          N0(il:iu,1) = NCVM(i:ntot,i)
       end do          
       l1 = 2            ;l2 = lmax   !need fiducial S+N for QML
    elseif(lswitch .lt. lmax) then
       l1 = lswitch +1   ;l2 = lmax   !fix high-ell for likelihood
    else
       l1 = 3            ;l2 = 2      !only need to project mono/dipole
    end if
    
    call update_ncvm(clstmp,l1,l2,NCVM,project_mondip=BFP%project_mondip)
    deallocate(clstmp)
    
    !shouldn't need these anymore
!    deallocate(sin1,cos1,sin2,cos2)

    call init_modes(lswitch,ntot,nsp,fevec,feval,cblocks)

    !modes computation uses RING internally
    call reorder_map(pix_temp,from=mask%s%ordering, to=HPX_RING)
    
    !look for plms eigenmodes
    if(.not.read_plms_from_fits(BFP%basisfile,feval,fevec,cblocks,l1)) then
       write(*,*) 'basis file not found/wrong number of modes'
       write(*,*) 'computing plms from scratch'
       if(len_trim(BFP%basisfile) .gt. 0) then
          write(*,*) 'computed plms will be saved in: '//trim(BFP%basisfile)
          write(*,*)size(feval),size(fevec),size(cblocks)
          call compute_modes(feval,fevec,cblocks,trim(BFP%basisfile))
       else
          call compute_modes(feval,fevec,cblocks)
       end if
    elseif (l1 .lt. lswitch) then
       call compute_modes(feval,fevec,cblocks,trim(BFP%basisfile),l1+1)       
    end if

    !clean up stuff we don't need anymore
    !deallocate(Tvec,Uvec,Qvec)
    !deallocate(Tvec,Uvec,Qvec,sin1,cos1,sin2,cos2)
    deallocate(pl,plm,f1,f2)

    if(do_qml) then
       !NCVM = S+N -> S
       call move_alloc(to=S,from=NCVM)

    elseif (do_linalm) then
       
       neigen = nsp*(lswitch+3)*(lswitch-1) !nsp*(2l+1) from 2 to lswitch 
       allocate(Ylm(ntot,neigen))
       call modes2matrix(feval,fevec,Ylm)
       
       do l=2,lswitch
          call fevec(l)%clean()
          call feval(l)%clean()
          do j = 2,6
             call cblocks(l,j)%clean()
          end do
       end do       
       deallocate(fevec ,feval ,cblocks)

       !crude but will work for now
       allocate(tmpmask, source = mask)
       call init_modes(lswitch,int(mask%s%npix*count([has_t,has_p,has_p])),nsp,fevec,feval,cblocks)

       if (has_t) mask%lmask(0:mask%s%npix-1) = .true. 
       if (has_p) mask%lmask(mask%s%npix:3*mask%s%npix-1) = .true.
       
       allocate(Ylm_full(count(mask%lmask),neigen))
       call compute_modes(feval,fevec,cblocks)
       call modes2matrix(feval,fevec,Ylm_full)

       do l=2,lswitch
          call fevec(l)%clean()
          call feval(l)%clean()
          do j = 2,6
             call cblocks(l,j)%clean()
          end do
       end do       
       mask = tmpmask
       deallocate(tmpmask)
    else
       allocate(reflike(ndata) ,source = 0._dp)

       !contract NCVM and data on the plms 
       neigen = nsp*(lswitch+3)*(lswitch-1) !nsp*(2l+1) from 2 to lswitch 

       allocate(evec(ntot,neigen))
       iu = 0
       do l=2,lswitch
          il = iu +1
          iu = il +nsp*(2*l +1)-1
          evec(:,il:iu) = fevec(l)%m
       end do
       S     = evec
       auxdt = dt

       call dposv('L',ntot,ndata,ncvm,ntot,auxdt,ntot,info)
       if(info .ne. 0) then 
          write(*,*) '(updated) NCVM is not invertible'
          write(*,*) 'info = ',info
          stop
       end if
       
       do i = 1,ndata
          reflike(i) = sum(dt(:,i)*auxdt(:,i))
       end do
       if(debugging) write(*,*) 'reflike =',reflike(1)
       
       deallocate(dt)  ;allocate(dt(neigen,ndata))
       call dgemm('T','N', neigen, ndata, ntot, 1.d0, evec, &
            ntot, auxdt, ntot, 0.d0, dt, neigen)
       deallocate(auxdt) ;allocate(auxdt,source = dt)
       
       call dpotrs('L',ntot,neigen,NCVM,ntot,evec,ntot,info)
       write(*,*) 'info = ',info
       
       if(debugging) then
          write(*,*) '--------------------------'
          write(*,*) ' done V A^-1 d  '
          write(*,*) '--------------------------'
       end if
       
       deallocate(NCVM) ;allocate(NCVM(neigen,neigen))
       call dgemm('T','N', neigen, neigen, ntot, 1.d0, S, &
            ntot, evec, ntot, 0.d0, NCVM , neigen)
       
       if(debugging) then
          write(*,*) '--------------------------'
          write(*,*) ' done V A^-1 V^t  '
          write(*,*) '--------------------------'
       end if

       if(allocated(S)) deallocate(S) ;allocate(S, mold = NCVM)

       !eigenvectors are not needed for likelihood
       do l=2,lswitch
          call fevec(l)%clean()
       end do
       deallocate(fevec)
    end if

    deallocate(clnorm)

    deallocate(pix_temp ,pwf)
    
    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) ' done init_pix_like   '
       write(*,*) '--------------------------'
    end if
    
  end subroutine init_pix_like

! ---------------------------------------------------------------------------
  
  subroutine compute_clnorm(bl,clnorm)

    implicit none
    real(dp) ,intent(in)  :: bl(0:,:)
    real(dp) ,intent(out) :: clnorm(2:,:)

    integer(i4b)          :: l
    real(dp)              :: ell ,fct ,fct2 ,chngconv

    do l=2,lmax
       ell = real(l,kind=dp)

       !input cls are l(l+1)/2pi, we need (2l+1)/4pi
       chngconv = (2.d0*ell +1.d0)/2.d0/(ell*(ell+1.d0))
       !chngconv = real(2*l +1,kind=dp)/(2*ell*(ell+1))

       !TP function is proportional to P_l2
       !F_X = sqrt((l-2)!/(l+2)!)*P_l2
!       fct2 = 1.d0/((ell+2.d0)*(ell+1.d0)*ell*(ell-1.d0))
       fct2 = 1.d0
       fct  = sqrt(fct2)

       clnorm(l,myTT) = chngconv*bl(l,myTT)**2
       clnorm(l,myEE) = chngconv*bl(l,myEE)**2*fct2
       clnorm(l,myBB) = chngconv*bl(l,myBB)**2*fct2
       clnorm(l,myTE) = chngconv*bl(l,myTE)**2*fct
       clnorm(l,myTB) = chngconv*bl(l,myTB)**2*fct
       clnorm(l,myEB) = chngconv*bl(l,myEB)**2*fct2

    end do

  end subroutine compute_clnorm

! ---------------------------------------------------------------------------

  subroutine clean_pix_like
    implicit none

    if(allocated(S))        deallocate(S)
    if(allocated(clnorm))   deallocate(clnorm)
    if(allocated(cls))      deallocate(cls)
    if(allocated(pl))       deallocate(pl)
    if(allocated(plm))      deallocate(plm)
    if(allocated(f1))       deallocate(f1)
    if(allocated(f2))       deallocate(f2)
    if(allocated(dt))       deallocate(dt)
    if(allocated(auxdt))    deallocate(auxdt)

  end subroutine clean_pix_like

! ---------------------------------------------------------------------------

  subroutine get_tt_cov(clsin,linf,lsup,cov,project_mondip,symmetrize,update)
 
    implicit none
    real(dp) ,intent(in)          :: clsin(2:,:)
    integer ,intent(in)           :: linf ,lsup
    real(dp) ,intent(inout)       :: cov(:,:)
    logical ,optional ,intent(in) :: project_mondip ,symmetrize ,update

    logical  :: up
    integer  :: i ,j ,l
    real(dp) :: tt ,cz ,ct0 ,ct1

    ct0 = 0._dp
    ct1 = 0._dp

    if(present(project_mondip)) then
       if(project_mondip) then
          ct0 = 1.e6_dp
          ct1 = 1.e3_dp
       end if
    end if

    !default is overwrite 
    if(present(update)) then 
       up = update
    else
       up = .false.
    end if
    if(.not. up) cov = 0.d0
    
    cls(2:lsup,myTT) = clsin(2:lsup,myTT)*clnorm(2:lsup,myTT)
!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(cov,mask,cls,ntemp,lsup,linf,ct1,ct0) SCHEDULE(dynamic,1)
    do i=1,ntemp
! TT 
       cov(i,i) = cov(i,i) +sum(cls(linf:lsup,myTT)) +ct1 +ct0        
       do j=i+1,ntemp
          cz = sum(mask%Tvec(:,j)*mask%Tvec(:,i))
          pl(1) = cz
          pl(2) = 1.5d0*cz*cz -.5d0
          do l = 3,lsup
             pl(l) =(cz*(2*l -1)*pl(l-1) -(l-1)*pl(l-2))/l
          enddo
          tt = sum(cls(linf:lsup,myTT)*pl(linf:lsup)) &
               +Pl(1)*ct1 +ct0 !project out monopole and dipole
          cov(j,i) = cov(j,i) +tt
       enddo
    end do

    if (present(symmetrize)) then
       if(symmetrize) call symmetrize_matrix(cov)
    end if

  end subroutine get_tt_cov

! ---------------------------------------------------------------------------

  subroutine get_pp_cov(clsin,linf,lsup,cov,symmetrize,update)

    implicit none
    real(dp)          ,intent(in)    :: clsin(2:,:)
    integer           ,intent(in)    :: linf ,lsup
    real(dp)          ,intent(inout) :: cov(:,:)
    logical ,optional ,intent(in)    :: symmetrize ,update

    real(dp) ,allocatable :: fl(:) 
    real(dp)              :: qq ,uu ,qu ,cz 
    integer(i8b)          :: i ,j ,iu ,iq ,ju ,jq
    logical               :: up 

    real(dp)              :: a1 ,a2 ,s1 ,s2 ,c1 ,c2
        
    !added to compensate change in definition of clnorm
    allocate(fl(2:lsup))
    do i = 2,lsup
       fl(i) = (i+2)*(i+1)*i*(i-1)
    end do
    cls(2:lsup,myEE) = clsin(2:lsup,myEE)*clnorm(2:lsup,myEE)/fl(2:lsup)
    cls(2:lsup,myBB) = clsin(2:lsup,myBB)*clnorm(2:lsup,myBB)/fl(2:lsup)
    cls(2:lsup,myEB) = clsin(2:lsup,myEB)*clnorm(2:lsup,myEB)/fl(2:lsup)
    deallocate(fl)
    
    !default is overwrite 
    if(present(update)) then 
       up = update
    else
       up = .false.
    end if
    if(.not. up) cov = 0.d0

    write(0,*) 'start loop'
    
!!$OMP PARALLEL DEFAULT (PRIVATE) SHARED(cov,mask,cls,nu,nq,ntemp,lsup,linf) 
!!$OMP DO SCHEDULE(dynamic,1)
!$OMP PARALLEL DO DEFAULT (PRIVATE) SHARED(cov,mask,cls,nu,nq,ntemp,lsup,linf) schedule(dynamic,1)
    do i = 1,nq
!QQ 
!       iq = i +ntemp
       do j = i,nq
          cz = sum(mask%Qvec(:,j)*mask%Qvec(:,i))
          call compute_polfunc2(cz,qq,uu,qu)
!          call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)
          call get_rotation_angle(mask%QVec(:,j),mask%QVec(:,i),a1,a2)
          s1 = sin(a1)
          s2 = sin(a2)
          c1 = cos(a1)
          c2 = cos(a2)
          
!          jq = j +ntemp
          cov(j,i) = cov(j,i) +qq*c1*c2 +uu*s1*s2 +qu*(c1*s2+s1*c2)
       end do

 !UQ 
       do j=1,nu
          cz = sum(mask%Uvec(:,j)*mask%Qvec(:,i))
!          call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)
          call compute_polfunc2(cz,qq,uu,qu)
          call get_rotation_angle(mask%UVec(:,j),mask%QVec(:,i),a1,a2)
          s1 = sin(a1)
          s2 = sin(a2)
          c1 = cos(a1)
          c2 = cos(a2)

!          ju = j+nq+ntemp
          cov(j+nq,i) = cov(j+nq,i) -qq*s1*c2 +uu*c1*s2 +qu*(c1*c2 -s1*s2)

       end do
    end do
    write(0,*) 'QQ+UQ done',omp_get_thread_num()

!!$OMP DO SCHEDULE(dynamic,1)
!$OMP PARALLEL DO DEFAULT (PRIVATE) SHARED(cov,mask,cls,nu,nq,ntemp,lsup,linf) schedule(dynamic,1)
    do i =1,nu
!UU 
       iu = i+nq+ntemp
       do j=i,nu
          cz = sum(mask%Uvec(:,j)*mask%Uvec(:,i))
!          call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)
          call compute_polfunc2(cz,qq,uu,qu)

!          ju = j+nq+ntemp

          call get_rotation_angle(mask%UVec(:,j),mask%QVec(:,i),a1,a2)
          s1 = sin(a1)
          s2 = sin(a2)
          c1 = cos(a1)
          c2 = cos(a2)

          cov(j+nq,i+nq) = cov(j+nq,i+nq) +qq*s1*s2 +uu*c1*c2 -qu*(c1*s2 +s1*c2)
       end do
    end do
!!$OMP END PARALLEL
    write(0,*) 'UU done',omp_get_thread_num()

    if (present(symmetrize)) then
       if(symmetrize) call symmetrize_matrix(cov)
    end if

    if(debugging  .and. .not.debugging)  then

!$OMP PARALLEL DEFAULT (PRIVATE) SHARED(cov,mask,cls,nu,nq,ntemp,lsup,linf) 
!$OMP DO
       do i = 1,nq
!QQ 
          iq = i +ntemp
          do j = 1,i-1
             cz = sum(mask%Qvec(:,j)*mask%Qvec(:,i))
             call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)
             
!             jq = j +ntemp
             call get_rotation_angle(mask%QVec(:,j),mask%QVec(:,i),a1,a2)
             s1 = sin(a1)
             s2 = sin(a2)
             c1 = cos(a1)
             c2 = cos(a2)
                         
             cov(j,i) = cov(j,i) +qq*c1*c2 +uu*s1*s2 +qu*(c1*s2+s1*c2)
             
          end do
       end do
       
!$OMP DO      
       do i =1,nu
 !UQ 
          iu = i+nq+ntemp
          
          do j=1,nq
             cz = sum(mask%Qvec(:,j)*mask%Uvec(:,i))
             call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)
             
             jq = i +ntemp
             
             call get_rotation_angle(mask%QVec(:,j),mask%UVec(:,i),a1,a2)
             s1 = sin(a1)
             s2 = sin(a2)
             c1 = cos(a1)
             c2 = cos(a2)
             
             cov(j,i+nq) = cov(j,i+nq) -qq*c1*s2 +uu*s1*c2 +qu*(c1*c2 -s1*s2)
          end do
       
!UU 
          do j=1,i-i
             cz = sum(Uvec(:,j)*Uvec(:,i))
             call compute_polfunc(cz,plm,f1,f2,qq,uu,qu)
          
             call get_rotation_angle(mask%UVec(:,j),mask%UVec(:,i),a1,a2)
             s1 = sin(a1)
             s2 = sin(a2)
             c1 = cos(a1)
             c2 = cos(a2)

             ju = j+nq+ntemp
             
             cov(j+nq,i+nq) = cov(j+nq,i+nq) +qq*s1*s2 +uu*c1*c2 -qu*(c1*s2 +s1*c2)
             
          end do
       end do
!$OMP END PARALLEL
    end if

  contains

    subroutine compute_polfunc(cz,plm,f1,f2,qq,uu,qu)

      implicit none
      real(dp) ,intent(in)  :: cz
      real(dp) ,intent(out) :: plm(:),f1(2:),f2(2:)
      !real(qp) ,intent(out) :: plm(:),f1(2:),f2(2:)
      real(dp) ,intent(out) :: qq,uu,qu

      real(dp) ,parameter   :: pix_size = 6._dp !Nside=512 pixel size in amin
      real(dp) ,parameter   :: cz1 = cos(pix_size*pi/10800) 
      real(dp) ,parameter   :: cz2 = cos((1 - pix_size/10800)*pi) 
      
      integer(i8b)          :: l

      if (cz .gt. cz1) then     !same pixel
         do l = 2,lsup
            f1(l) =  0.5_qp*((l-1)*l*(l+1)*(l+2))
         end do
         f2 = -f1
      elseif (cz .lt. cz2) then !opposite pixels
         do l = 2,lsup
            f1(l) = (-1)**l*0.5_dp*((l-1)*l*(l+1)*(l+2))
         end do
         f2 = f1
      else
         !do recursion on plm/(1-z^2) to avoid division by 0
         plm(1) = 0
         plm(2) = 3
         f1(2)  = 6 +6*cz*cz
         f2(2)  = -12*cz
         do l = 3,lsup
            plm(l) =(cz*(2*l -1)*plm(l-1) -(l+1)*plm(l-2))/(l -2)
            f1(l) =-(2*l-8 +l*(l-1)*(1 -cz*cz))*plm(l)+ &
                 (2*l+4)*cz*plm(l-1)
            f2(l) = 4*(-(l-1)*cz*plm(l) +(l+2)*plm(l-1))
         enddo
      end if
         
      qq = sum(cls(linf:lsup,myEE)*f1(linf:lsup) &
           -cls(linf:lsup,myBB)*f2(linf:lsup))
      uu = sum(cls(linf:lsup,myBB)*f1(linf:lsup) &
           -cls(linf:lsup,myEE)*f2(linf:lsup))
      qu = sum((f1(linf:lsup) +f2(linf:lsup))*cls(linf:lsup,myEB))
      
    end subroutine compute_polfunc


    subroutine compute_polfunc2(cz,qq,uu,qu)

      implicit none
      real(dp) ,intent(in)  :: cz
      real(dp) ,intent(out) :: qq,uu,qu

      real(dp)              :: plm0,plm1,plm2,f1(2:lsup),f2(2:lsup)
      integer               :: l

      !do recursion on plm/(1-z^2) to avoid division by 0
      plm2 = 0.d0
      plm1 = 3.d0
      f1(2)  = 6.d0*(1d0+cz*cz)
      f2(2)  = -12.d0*cz
      do l = 3,lsup
         plm0 =(cz*(2*l -1)*plm1 -(l+1)*plm2)/(l -2)
         f1(l) =-(2*l-8 +l*(l-1)*(1.d0 -cz*cz))*plm0+ &
              (2*l+4)*cz*plm1
         f2(l) = 4.d0*(-(l-1)*cz*plm0 +(l+2)*plm1)
         plm2 = plm1
         plm1 = plm0
      enddo

      qq = sum(cls(linf:lsup,myEE)*f1(linf:lsup) &
           -cls(linf:lsup,myBB)*f2(linf:lsup))
      uu = sum(cls(linf:lsup,myBB)*f1(linf:lsup) &
           -cls(linf:lsup,myEE)*f2(linf:lsup))
      qu = sum((f1(linf:lsup) +f2(linf:lsup))*cls(linf:lsup,myEB))
      
    end subroutine compute_polfunc2


    
  end subroutine get_pp_cov

! ---------------------------------------------------------------------------

  subroutine get_xx_cov(clsin,linf,lsup,cov,symmetrize,update)

    implicit none
    real(dp)          ,intent(in)    :: clsin(2:,:)
    integer           ,intent(in)    :: linf ,lsup
    real(dp)          ,intent(inout) :: cov(:,:)
    logical ,optional ,intent(in)    :: symmetrize ,update

    real(dp) ,allocatable :: fl(:)
    real(dp)              :: tq ,tu ,cz ,a1
    logical               :: up
    integer(i8b)          :: i ,j ,ju ,jq ,iq ,iu

    !added to compensate change in definition of clnorm
    allocate(fl(2:lsup))
    do i =2,lsup
       fl(i) = sqrt(real((i+2)*(i+1)*i*(i-1),dp))
    end do
    cls(2:lsup,myTE) = clsin(2:lsup,myTE)*clnorm(2:lsup,myTE)/fl(2:lsup)
    cls(2:lsup,myTB) = clsin(2:lsup,myTB)*clnorm(2:lsup,myTB)/fl(2:lsup)
    deallocate(fl)
    
    !default is overwrite 
    if(present(update)) then 
       up = update
    else
       up = .false.
    end if
    if(.not. up) then
       cov(1:ntemp,ntemp+1:ntot) = 0.d0
       cov(ntemp+1:ntot,1:ntot)  = 0.d0
    end if

!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(cov,mask,cls,nu,nq,ntemp,lsup,linf,cos1,cos2,sin1,sin2) 
    do i=1,ntemp
! QT
       do j=1,nq
          cz = sum(mask%Qvec(:,j)*mask%Tvec(:,i))
          call compute_tpfunc(cz,cls,plm,tq,tu)
          call get_rotation_angle(mask%QVec(:,j),mask%TVec(:,i),a1)
          jq = j +ntemp
          cov(jq,i) = cov(jq,i) +tq*cos(a1) +tu*sin(a1) 

       enddo

!UT
       do j=1,nu
          cz = sum(mask%Uvec(:,j)*mask%Tvec(:,i))
          call compute_tpfunc(cz,cls,plm,tq,tu)
          call get_rotation_angle(mask%UVec(:,j),mask%TVec(:,i),a1)
          ju = j +ntemp +nq
          cov(ju,i) = cov(ju,i) -tq*sin(a1) +tu*cos(a1) 

       enddo

    end do

    if (present(symmetrize)) then
       if (symmetrize) call symmetrize_matrix(cov)
    end if

    if(debugging .and.  .not.debugging) then
!$OMP PARALLEL DEFAULT (PRIVATE) SHARED(cov,mask,cls,nu,nq,ntemp,lsup,linf,cos1,cos2,sin1,sin2)
!$OMP DO
       do i=1,nq
!TQ
          iq = i +ntemp
          do j=1,ntemp
             cz = sum(mask%Tvec(:,j)*mask%Qvec(:,i))
             call compute_tpfunc(cz,cls,plm,tq,tu)
             cov(j,iq) = cov(j,iq) +tq*cos2(j,iq)  +tu*sin2(j,iq)
             
          enddo
       end do
!TU
!$OMP DO
       do i=1,nu
          iu = i+ntemp+nq
          do j=1,ntemp
             cz = sum(mask%Tvec(:,j)*mask%Uvec(:,i))
             call compute_tpfunc(cz,cls,plm,tq,tu)
             cov(j,iu) = cov(j,iu) -tq*sin2(j,iu) +tu*cos2(j,iu)
          enddo
       end do
!$OMP END PARALLEL
    end if

  contains

    subroutine compute_tpfunc(cz,cls,plm,tq,tu)

      implicit none
      real(dp) ,intent(in)  :: cz ,cls(2:,:)
      real(dp) ,intent(out) :: plm(:) 
      !real(qp) ,intent(out) :: plm(:) 
      real(dp) ,intent(out) :: tq ,tu

      integer               :: l

      plm(1) = 0.d0
      plm(2) = 3.d0*(1.d0 -cz*cz)
      do l = 3,lsup
         plm(l) =(cz*(2*l -1)*plm(l-1) -(l+1)*plm(l-2))/(l-2)
      enddo
      tq = -sum(cls(linf:lsup,myTE)*plm(linf:lsup))
      tu = -sum(cls(linf:lsup,myTB)*plm(linf:lsup))

    end subroutine compute_tpfunc
      
  end subroutine get_xx_cov

! ---------------------------------------------------------------------------

  subroutine get_pix_loglike_tt(clsin,alike,argexp,logdet)
!input clsin are l(l+1)C_l/2pi

    implicit none    
    real(dp),intent(in)  :: clsin(2:)
    real(dp),intent(out) :: alike(:),argexp(:),logdet(:)

    integer              :: i ,j ,l ,info ,neigen ,nl ,id
    real(dp)             :: tmp ,tmplog

    logdet = 0._dp
    neigen = size(dt(:,1))

    do concurrent (i = 1:neigen)
       S(i:neigen,i) = ncvm(i:neigen,i)
    end do

    id     = 0
    tmplog = 0._dp
    
    !TT is diagonal
    do l = 2,lswitch
       nl  = 2*l+1
       do i=1,nl
          id  = id +1
          tmp = clsin(l)*feval(l)%m(i,1)
          tmplog   = tmplog +log(abs(tmp))
          S(id,id) = S(id,id) +1._dp/tmp
       end do
    end do

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'S +NCVM ,dt'
       do i = 1,15
          write(*,*) i,i,S(i,i),dt(i,1)
       end do
       write(*,*) '--------------------------'
    end if

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'starting dposv '
       write(*,*) '--------------------------'
    end if

    auxdt = dt
    
    call dposv('L',neigen,ndata,S,neigen,auxdt,neigen,info)

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'finished dposv '
       write(*,*) 'info = ',info
       write(*,*) '--------------------------'
    end if

!    cholesky: X*(C^-1*X)
    logdet(1) = tmplog/2
    if (info.eq.0) then

       do j=1,neigen
          logdet(1) = logdet(1) +log(S(j,j))
       enddo
       logdet(1) = 2.d0*logdet(1)

       logdet(2:ndata) = logdet(1)

!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(argexp,dt,auxdt,alike,logdet,reflike,ndata)
       do i=1,ndata
          argexp(i) = reflike(i) -sum(dt(:,i)*auxdt(:,i))
          alike(i)  = -.5d0*(argexp(i)+logdet(i))
       end do

    else

       argexp = 1.d30
       logdet = 1.d30

       alike = -.5d0*(argexp+logdet)
    endif

    if(debugging) then
       write(*,*) '--------------------------'
       do j=1,1
          write(*,*) 'argexp =',argexp(j)
          write(*,*) 'logdet =',logdet(j)
       end do
       write(*,*) '--------------------------'
    end if

  end subroutine get_pix_loglike_tt

! ---------------------------------------------------------------------------

  subroutine get_pix_loglike(clsin,alike,argexp,logdet)
    !input clsin are l(l+1)C_l/2pi ,conversions are handled internally
    implicit none

    real(dp),intent(in)  :: clsin(2:,:)
    real(dp),intent(out) :: alike(:),argexp(:),logdet(:)

    if (debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'size = ' ,size(clsin(:,1)) ,size(clsin(2,:))
       write(*,*) 'ndata = ',ndata
       write(*,*) 'size = ',size(alike(:)),size(argexp(:)),size(logdet(:))
       write(*,*) '--------------------------'
    end if

    if (debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'in get_pix_loglike'
       write(*,'(6e13.6)') clsin(2,:)
       write(*,'(6e13.6)') clsin(lmax-1,:)
       write(*,*) '--------------------------'
    end if

    if (has_x) then
       call  get_pix_loglike_tp(clsin,alike,argexp,logdet)
    elseif (has_t) then
       call  get_pix_loglike_tt(clsin(:,myTT),alike,argexp,logdet)
    else
       call  get_pix_loglike_pp(clsin(:,:),alike,argexp,logdet)
    end if
       
  end subroutine get_pix_loglike
  
! ---------------------------------------------------------------------------
  
  subroutine get_pix_loglike_tp(clsin,alike,argexp,logdet)

    implicit none
    
    real(dp),intent(in)  :: clsin(2:,:)
    real(dp),intent(out) :: alike(:),argexp(:),logdet(:)

    integer              :: i ,j ,l ,info ,neigen ,nl ,nl2 ,nl3 ,iu
    integer  ,allocatable:: pinfo(:) ,il(:)
    real(dp) ,allocatable:: tm1(:,:) ,tmplog(:)
    real(dp)             :: tmp

    logdet = 0._dp
    neigen = size(dt(:,1))

    nl2 = 2*(2*lswitch+1)
    allocate(tm1(nl2,nl2),tmplog(2:lswitch),pinfo(2:lswitch),il(2:lswitch))
    iu = 0
    do l=2,lswitch
       il(l) = iu +1
       iu = il(l) +6*l+2 !3*(2l+1) for each l
    end do

!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(S,ncvm,neigen)
    do i=1,neigen
       S(i:neigen,i) = ncvm(i:neigen,i)
    end do

!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(S,neigen,lswitch,clsin,tmplog,cblocks,feval,pinfo,il) SCHEDULE(dynamic,1)
    do l=2,lswitch

       tmplog(l) = 0._dp
    
       nl  = 2*l+1
       nl2 = 2*nl
       nl3 = 3*nl
 
       ! TT,TE,EE 
       do i=1,nl
          tm1(i,i)        = clsin(l,myTT)*feval(l)%m(i,1)
          tm1(i+1:nl,i)   = 0.d0
          tm1(nl+1:nl2,i) = clsin(l,myTE)*cblocks(l,myTE)%m(nl+1:nl2,i)
       end do
       do i = nl+1,nl2
          tm1(i,i)        = clsin(l,myEE)*feval(l)%m(i,1)
          tm1(i+1:nl2,i)  = 0.d0
       end do

       call dpotrf('L',nl2,tm1(1:nl2,1:nl2),nl2,pinfo(l))

       do j=1,nl2
          tmplog(l) = tmplog(l) +log(tm1(j,j))
       end do
       call dpotri('L',nl2,tm1(1:nl2,1:nl2),nl2,info)

       do i=1,nl2
          do j=i,nl2
             S(il(l)-1+j,il(l)-1+i) = S(il(l)-1+j,il(l)-1+i) +tm1(j,i)
          end do
       end do

       !BB is diagonal 
       do i = nl2+1,nl3
          tmp = 1._dp/(max(clsin(l,myBB),1.d-30)*feval(l)%m(i,1))
          S(il(l)+i-1,il(l)+i-1) = S(il(l)+i-1,il(l)+i-1) +tmp
          tmplog(l) = tmplog(l) -.5_dp*log(tmp) 
       end do
 
    end do

    logdet(1) = sum(tmplog(2:lswitch))
    
    do l=2,lswitch
       if(pinfo(l).ne.0) then
          write(*,*) 'block inversion failed for l =',l,' info =',pinfo(l)
          argexp = -1.d30
          logdet = -1.d30
          alike  = -1.d30
          deallocate(tmplog,il,tm1,pinfo)
          if(debugging) stop
          return
       end if
    end do

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'S +NCVM ,dt'
       do i = 1,15
          write(*,*) i,i,S(i,i),dt(i,1)
       end do
       write(*,*) '--------------------------'
    end if

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'starting dposv '
       write(*,*) '--------------------------'
    end if

    auxdt = dt
    
    call dposv('L',neigen,ndata,S,neigen,auxdt,neigen,info)

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'finished dposv '
       write(*,*) 'info = ',info
       write(*,*) '--------------------------'
    end if

!    cholesky: X*(C^-1*X)
    if (info.eq.0) then

       do j=1,neigen
          logdet(1) = logdet(1) +log(S(j,j))
       enddo
       logdet(1) = 2.d0*logdet(1)

       logdet(2:ndata) = logdet(1)

!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(argexp,dt,auxdt,alike,logdet,reflike,ndata)
       do i=1,ndata
          argexp(i) = reflike(i) -sum(dt(:,i)*auxdt(:,i))
          alike(i)  = -.5d0*(argexp(i)+logdet(i))
       end do

    else

       argexp = 1.d30
       logdet = 1.d30

       alike = -.5d0*(argexp+logdet)
    endif

    if(debugging) then
       write(*,*) '--------------------------'
       do j=1,1
          write(*,*) 'argexp =',argexp(j)
          write(*,*) 'logdet =',logdet(j)
       end do
       write(*,*) '--------------------------'
    end if

    write(*,*) argexp(1),logdet(1)

    deallocate(tmplog,il,tm1,pinfo)
  end subroutine get_pix_loglike_tp

! ---------------------------------------------------------------------------
  
  subroutine get_student_loglike_tp(clsin,alike,argexp,logdet)

    implicit none
    
    real(dp),intent(in)  :: clsin(2:,:)
    real(dp),intent(out) :: alike(:),argexp(:),logdet(:)

    integer              :: i ,j ,l ,info ,neigen ,nl ,nl2 ,nl3 ,iu
    integer  ,allocatable:: pinfo(:) ,il(:)
    real(dp) ,allocatable:: tm1(:,:) ,tmplog(:) ,clhat(:,:)
    real(dp)             :: tmp ,logp

    logdet = 0._dp
    neigen = size(dt(:,1))
    
    nl2 = 2*(2*lswitch+1)
    allocate(tm1(nl2,nl2),tmplog(2:lswitch),pinfo(2:lswitch),il(2:lswitch))
    iu = 0
    do l=2,lswitch
       il(l) = iu +1
       iu = il(l) +6*l+2 !3*(2l+1) for each l
    end do

    call cl2hatcl_tp(clsin(2:lswitch,:),nsims,clhat,logp)

!    clhat = clsin
    logp = 0._dp
    
    if (debugging) then
       write(*,*) '--------------------------'
       do l = 2,lswitch
          write(*,*) 'TT ',l,clhat(l,myTE)/clsin(l,myTE)
       end do
       do l = 2,lswitch
          write(*,*) 'TE ',l,clhat(l,myTE)/clsin(l,myTE)
       end do
       do l = 2,lswitch
          write(*,*) 'EE ',l,clhat(l,myEE)/clsin(l,myEE)
       end do
       do l = 2,lswitch
          write(*,*) 'BB ',l,clhat(l,myBB)/max(clsin(l,myBB),1.d-30)
       end do

       write(*,*) 'logp     = ',logp
       write(*,*) '--------------------------'
    end if
    
!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(S,ncvm,neigen)
    do i=1,neigen
       S(i:neigen,i) = ncvm(i:neigen,i)
    end do
    
!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(S,neigen,lswitch,clhat,tmplog,cblocks,feval,pinfo,il)
    do l=2,lswitch

       tmplog(l) = 0._dp
    
       nl  = 2*l+1
       nl2 = 2*nl
       nl3 = 3*nl
 
       ! TT,TE,EE 
       do i=1,nl
          tm1(i,i)        = clhat(l,myTT)*feval(l)%m(i,1)
          tm1(i+1:nl,i)   = 0.d0
          tm1(nl+1:nl2,i) = clhat(l,myTE)*cblocks(l,myTE)%m(nl+1:nl2,i)
       end do
       do i = nl+1,nl2
          tm1(i,i)        = clhat(l,myEE)*feval(l)%m(i,1)
          tm1(i+1:nl2,i)  = 0.d0
       end do

       call dpotrf('L',nl2,tm1(1:nl2,1:nl2),nl2,pinfo(l))

       do j=1,nl2
          tmplog(l) = tmplog(l) +log(tm1(j,j))
       end do
       call dpotri('L',nl2,tm1(1:nl2,1:nl2),nl2,info)

       do i=1,nl2
          do j=i,nl2
             S(il(l)-1+j,il(l)-1+i) = S(il(l)-1+j,il(l)-1+i) +tm1(j,i)
          end do
       end do

       !BB is diagonal 
       do i = nl2+1,nl3
          tmp = 1._dp/(max(clhat(l,myBB),1.d-30)*feval(l)%m(i,1))
          S(il(l)+i-1,il(l)+i-1) = S(il(l)+i-1,il(l)+i-1) +tmp
          tmplog(l) = tmplog(l) -.5_dp*log(tmp) 
       end do
 
    end do

    logdet(1) = sum(tmplog(2:lswitch))
    
    do l=2,lswitch
       if(pinfo(l).ne.0) then
          write(*,*) 'block inversion failed for l =',l,' info =',pinfo(l)
          argexp = -1.d30
          logdet = -1.d30
          alike  = -1.d30
          deallocate(tmplog,il,tm1,pinfo)
          if(debugging) stop
          return
       end if
    end do

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'S +NCVM ,dt'
       do i = 1,15
          write(*,*) i,i,S(i,i),dt(i,1)
       end do
       write(*,*) '--------------------------'
    end if

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'starting dposv '
       write(*,*) '--------------------------'
    end if

    auxdt = dt
    
    call dposv('L',neigen,ndata,S,neigen,auxdt,neigen,info)

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'finished dposv '
       write(*,*) 'info = ',info
       write(*,*) '--------------------------'
    end if

!    cholesky: X*(C^-1*X)
    if (info.eq.0) then

       do j=1,neigen
          logdet(1) = logdet(1) +log(S(j,j))
       enddo
       logdet(1) = 2.d0*logdet(1)

       logdet(2:ndata) = logdet(1)

!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(argexp,dt,auxdt,alike,logdet,reflike,ndata,logp,nsims)
       do i=1,ndata
          argexp(i) = reflike(i) -sum(dt(:,i)*auxdt(:,i))
          !alike(i)  = -.5d0*(argexp(i)+logdet(i))
          alike(i)  = logp -.5_dp*(logdet(i) +nsims*log(1._dp +argexp(i)/(nsims-1)))
       end do

    else

       argexp = 1.d30
       logdet = 1.d30

       !alike = -.5d0*(argexp+logdet)
       alike  = 1.d30
    endif


    write(*,*) argexp(1),logdet(1),logp 

    if(debugging) then
       write(*,*) '--------------------------'
       do j=1,1
          write(*,*) 'argexp =',argexp(j)
          write(*,*) 'logdet =',logdet(j)
       end do
       write(*,*) '--------------------------'
       stop
    end if


    deallocate(tmplog,il,tm1,pinfo)

  contains

    subroutine cl2hatcl_tp(cl,nsims,clhat,logp) 
 
      implicit none
      real(dp)     ,intent(in) :: cl(2:,:)
      integer(i4b) ,intent(in) :: nsims

      real(dp) ,intent(out) ,allocatable :: clhat(:,:)
      real(dp) ,intent(out)              :: logp

      real(dp)     :: deltam ,detclhat ,trace ,rho ,sumTT ,sumEE ,sumBB &
           ,sumTE ,sqomr2 ,CTT ,CEE ,CBB ,CTE ,alm_tt ,alm_ee ,alm_bb &
           ,detcl ,eta_tt ,eta_ee ,eta_bb
      integer(i4b) :: l ,i ,m ,lmax ,ncl ,k
      type(planck_rng) ,save :: tmp_handle
      real(dp) ,allocatable :: plogp(:) ,tmpcl(:,:)
      
      lmax = size(cl(:,1)) +1
      ncl  = size(cl(2,:))
      !actually assume ncl = 4, 'cause can't be bothered right now

      allocate(tmpcl(ncl,2:lmax) ,source = 0._dp)
      allocate(clhat(2:lmax,ncl) ,source = 0._dp)

      allocate(plogp(2:lmax))
      plogp = 0._dp

      if(debugging) then
         write(*,*) '--------------------------'
         write(*,*) 'In cl2hatcl lmax = ',lmax
         write(*,*) 'In cl2hatcl ncl  = ',ncl
         write(*,*) '--------------------------'
      end if
      
      !$OMP PARALLELDO SCHEDULE(dynamic) DEFAULT(private) SHARED(bfl_handle,lmax,cl,clhat,tmpcl,plogp,nsims) 
      do l = 2,lmax         
         tmp_handle = bfl_handle
         do i = 1,131*l
            eta_tt = rand_uni(tmp_handle)
         end do            
         CTT = cl(l,myTT)
         CEE = cl(l,myEE)
         CBB = max(cl(l,myBB),1.d-30)
         CTE = cl(l,myTE)

         rho = CTE/sqrt(CTT*CEE)
         sqomr2 = sqrt(1._dp - rho**2)
         sumTT = 0._dp
         sumEE = 0._dp
         sumBB = 0._dp
         sumTE = 0._dp
         
         do i = 1,nsims
            eta_tt = rand_gauss(tmp_handle)
            eta_ee = rand_gauss(tmp_handle)
            eta_bb = rand_gauss(tmp_handle)
            
            alm_tt = eta_tt
            alm_ee = eta_tt*rho +eta_ee*sqomr2
            alm_bb = eta_bb

            sumTT = sumTT +alm_tt**2
            sumEE = sumEE +alm_ee**2
            sumBB = sumBB +alm_bb**2
            sumTE = sumTE +alm_tt*alm_ee!/rho

            do m = 1,l
               do k = 1,2 !positive and negative m
                  eta_tt = rand_gauss(tmp_handle)
                  eta_ee = rand_gauss(tmp_handle)
                  eta_bb = rand_gauss(tmp_handle)
                  
                  alm_tt = eta_tt
                  alm_ee = eta_tt*rho +eta_ee*sqomr2
                  alm_bb = eta_bb
                  
                  sumTT = sumTT +alm_tt**2
                  sumEE = sumEE +alm_ee**2
                  sumBB = sumBB +alm_bb**2
                  sumTE = sumTE +alm_tt*alm_ee!/rho

               end do
            end do
         end do

         clhat(l,myTT) = CTT*sumTT/(nsims*(2*l+1))
         clhat(l,myEE) = CEE*sumEE/(nsims*(2*l+1))
         clhat(l,myBB) = CBB*sumBB/(nsims*(2*l+1))
         clhat(l,myTE) = CTE*sumTE/(nsims*(2*l+1))/rho

         detcl    = CTT*CEE -CTE**2
         detclhat = clhat(l,myTT)*clhat(l,myEE) -clhat(l,myTE)**2
         trace = (CEE*clhat(l,myTT) +CTT*clhat(l,myEE) &
              -2*CTE*clhat(l,myTE))/detcl +clhat(l,myBB)/CBB 
         !this is -2 log P
         plogp(l) = nsims*(2*l+1)*(trace + log(detcl*cBB)) -((2*l+1)*nsims -4)*log(detclhat*clhat(l,myBB))
         
!         detclhat = tmpcl(myTT,l)*tmpcl(myEE,l) -tmpcl(myTE,l)**2
!         trace = (CEE*tmpcl(myTT,l) +CTT*tmpcl(myEE,l) &
!              -2*CTE*tmpcl(myTE,l))/detcl +tmpcl(myBB,l)/max(CBB,1.d-30) 
!         !this is -2 log P
!         plogp(l) = (nsims*(2*l+1))*(trace + log(detcl*max(cBB,1.d-30))) -((2*l+1)*nsims -4)*log(detclhat*tmpcl(myBB,l))
         
      end do

      !clhat = transpose(tmpcl)

      logp = -.5_dp*sum(plogp)
      
      if(debugging) then
         write(*,*) '--------------------------'
         write(*,*) 'In cl2hatcl C_TT = ',cl(2,myTT),clhat(2,myTT)
         write(*,*) 'In cl2hatcl C_EE = ',cl(2,myEE),clhat(2,myEE)
         write(*,*) 'In cl2hatcl C_BB = ',cl(2,myBB),clhat(2,myBB)
         write(*,*) 'In cl2hatcl C_TE = ',cl(2,myTE),clhat(2,myTE)
         write(*,*) '--------------------------'
      end if
      
    end subroutine cl2hatcl_tp

    
  end subroutine get_student_loglike_tp

! ---------------------------------------------------------------------------
  
  subroutine get_student_loglike_matrix(clsin,alike,argexp,logdet)

    implicit none
    
    real(dp),intent(in)  :: clsin(2:,:)
    real(dp),intent(out) :: alike(:),argexp(:),logdet(:)

    integer              :: i ,j ,l ,info ,neigen ,nl ,nl2 ,nl3 ,iu
    integer  ,allocatable:: pinfo(:) ,il(:)
    real(dp) ,allocatable:: tm1(:,:) ,tmplog(:) ,clhat(:,:)
    real(dp)             :: tmp ,logp

    logdet = 0._dp
    neigen = size(dt(:,1))
    
    nl2 = 2*(2*lswitch+1)
    allocate(tm1(nl2,nl2),tmplog(2:lswitch),pinfo(2:lswitch),il(2:lswitch))
    iu = 0
    do l=2,lswitch
       il(l) = iu +1
       iu = il(l) +6*l+2 !3*(2l+1) for each l
    end do

    call cl2hatcl_matrix(clsin(2:lswitch,:),nsims,clhat,logp)
    stop 'Unfinished WIP'
    
!    clhat = clsin
    logp = 0._dp
    
    if (debugging) then
       write(*,*) '--------------------------'
       do l = 2,lswitch
          write(*,*) 'TT ',l,clhat(l,myTE)/clsin(l,myTE)
       end do
       do l = 2,lswitch
          write(*,*) 'TE ',l,clhat(l,myTE)/clsin(l,myTE)
       end do
       do l = 2,lswitch
          write(*,*) 'EE ',l,clhat(l,myEE)/clsin(l,myEE)
       end do
       do l = 2,lswitch
          write(*,*) 'BB ',l,clhat(l,myBB)/max(clsin(l,myBB),1.d-30)
       end do

       write(*,*) 'logp     = ',logp
       write(*,*) '--------------------------'
    end if
    
!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(S,ncvm,neigen)
    do i=1,neigen
       S(i:neigen,i) = ncvm(i:neigen,i)
    end do
    
!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(S,neigen,lswitch,clhat,tmplog,cblocks,feval,pinfo,il)
    do l=2,lswitch

       tmplog(l) = 0._dp
    
       nl  = 2*l+1
       nl2 = 2*nl
       nl3 = 3*nl
 
       ! TT,TE,EE 
       do i=1,nl
          tm1(i,i)        = clhat(l,myTT)*feval(l)%m(i,1)
          tm1(i+1:nl,i)   = 0.d0
          tm1(nl+1:nl2,i) = clhat(l,myTE)*cblocks(l,myTE)%m(nl+1:nl2,i)
       end do
       do i = nl+1,nl2
          tm1(i,i)        = clhat(l,myEE)*feval(l)%m(i,1)
          tm1(i+1:nl2,i)  = 0.d0
       end do

       call dpotrf('L',nl2,tm1(1:nl2,1:nl2),nl2,pinfo(l))

       do j=1,nl2
          tmplog(l) = tmplog(l) +log(tm1(j,j))
       end do
       call dpotri('L',nl2,tm1(1:nl2,1:nl2),nl2,info)

       do i=1,nl2
          do j=i,nl2
             S(il(l)-1+j,il(l)-1+i) = S(il(l)-1+j,il(l)-1+i) +tm1(j,i)
          end do
       end do

       !BB is diagonal 
       do i = nl2+1,nl3
          tmp = 1._dp/(max(clhat(l,myBB),1.d-30)*feval(l)%m(i,1))
          S(il(l)+i-1,il(l)+i-1) = S(il(l)+i-1,il(l)+i-1) +tmp
          tmplog(l) = tmplog(l) -.5_dp*log(tmp) 
       end do
 
    end do

    logdet(1) = sum(tmplog(2:lswitch))
    
    do l=2,lswitch
       if(pinfo(l).ne.0) then
          write(*,*) 'block inversion failed for l =',l,' info =',pinfo(l)
          argexp = -1.d30
          logdet = -1.d30
          alike  = -1.d30
          deallocate(tmplog,il,tm1,pinfo)
          if(debugging) stop
          return
       end if
    end do

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'S +NCVM ,dt'
       do i = 1,15
          write(*,*) i,i,S(i,i),dt(i,1)
       end do
       write(*,*) '--------------------------'
    end if

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'starting dposv '
       write(*,*) '--------------------------'
    end if

    auxdt = dt
    
    call dposv('L',neigen,ndata,S,neigen,auxdt,neigen,info)

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'finished dposv '
       write(*,*) 'info = ',info
       write(*,*) '--------------------------'
    end if

!    cholesky: X*(C^-1*X)
    if (info.eq.0) then

       do j=1,neigen
          logdet(1) = logdet(1) +log(S(j,j))
       enddo
       logdet(1) = 2.d0*logdet(1)

       logdet(2:ndata) = logdet(1)

!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(argexp,dt,auxdt,alike,logdet,reflike,ndata,logp,nsims)
       do i=1,ndata
          argexp(i) = reflike(i) -sum(dt(:,i)*auxdt(:,i))
          !alike(i)  = -.5d0*(argexp(i)+logdet(i))
          alike(i)  = logp -.5_dp*(logdet(i) +nsims*log(1._dp +argexp(i)/(nsims-1)))
       end do

    else

       argexp = 1.d30
       logdet = 1.d30

       !alike = -.5d0*(argexp+logdet)
       alike  = 1.d30
    endif


    write(*,*) argexp(1),logdet(1),logp 

    if(debugging) then
       write(*,*) '--------------------------'
       do j=1,1
          write(*,*) 'argexp =',argexp(j)
          write(*,*) 'logdet =',logdet(j)
       end do
       write(*,*) '--------------------------'
       stop
    end if


    deallocate(tmplog,il,tm1,pinfo)

  contains

    subroutine rand_gauss_multi(handle,x)
      type(planck_rng) ,intent(inout) :: handle
      real(dp)         ,intent(inout) :: x(:)
      
      real(dp) :: fac,rsq,v1,v2,r(2)
      integer  :: i ,n ,j
      
      n = size(x)

      do i = 1,n-1,2

1        v1 = 2.0_dp * rand_uni(handle) - 1.0_dp
         v2 = 2.0_dp * rand_uni(handle) - 1.0_dp
         rsq = v1**2 + v2**2
         if(rsq>=1.0_dp .or. rsq==0.0_dp) goto 1
         fac = sqrt(-2.0_dp * log(rsq)/rsq)
         x(i)   = v1*fac
         x(i+1) = v2*fac
      end do
      
2     v1 = 2.0_dp * rand_uni(handle) - 1.0_dp
      v2 = 2.0_dp * rand_uni(handle) - 1.0_dp
      rsq = v1**2 + v2**2
      if(rsq>=1.0_dp .or. rsq==0.0_dp) goto 2
      fac = sqrt(-2.0_dp * log(rsq)/rsq)
      x(n-1)   = v1*fac
      x(n) = v2*fac

    end subroutine rand_gauss_multi

    
    subroutine cl2hatcl_matrix(cl,nsims,cov,logp) 
 
      implicit none
      real(dp)     ,intent(in) :: cl(2:,:)
      integer(i4b) ,intent(in) :: nsims
      
      real(dp) ,intent(out) ,allocatable :: cov(:,:)
      real(dp) ,intent(out)              :: logp
      
      real(dp)     :: eta_tt ,eta_ee ,eta_bb ,alm_tt ,alm_ee ,alm_bb &
           ,detcl ,detclhat ,trace ,rho ,sumTT, sumEE, sumBB, sumTE &
           ,sqomr2 ,CTT ,CEE ,CBB ,CTE ,Tnorm ,Enorm ,Bnorm ,delta
      integer(i4b) :: l ,i ,m ,lmax ,ncl ,k ,nmodes
      integer(i4b) :: Tstart ,Tstop ,Estart ,Estop ,Bstart ,Bstop
      type(planck_rng) ,save :: tmp_handle
      real(dp) ,allocatable :: plogp(:) ,alm(:,:) 
      
      lmax = size(cl(:,1)) +1
      ncl  = size(cl(2,:))
      
      nmodes = (lswitch+1)**2 -4
      
      allocate(alm(nsims,nmodes))
      
      !first generate 0-mean unit Gaussian
      do i = 1,nmodes
         call rand_gauss_multi(tmp_handle,alm(:,i))
      end do
      
      !normalize the modes
      do l = 2,lmax
         nl = 2*l+1
         Tstart = 3*(l**2 -4) +1 
         Tstop  = Tstart +nl -1
         Estart = Tstop +1
         Estop  = Estart +nl -1
         Bstart = Estop +1 
         Bstop  = Bstart +nl -1
         
         Tnorm = sqrt(cl(l,myTT))
         Enorm = sqrt(cl(l,myEE))
         Bnorm = sqrt(cl(l,myBB))
         rho   = Cl(l,myTE)/(Tnorm*Enorm)
         sqomr2 = sqrt(1._dp - rho**2)
         
         alm(:,Estart:Estop) = (alm(:,Tstart:Tstop)*rho +alm(:,Estart:Estop)*sqomr2)*Enorm
         alm(:,Tstart:Tstop) = alm(:,Tstart:Tstop)*Tnorm 
         alm(:,Bstart:Bstop) = alm(:,Bstart:Bstop)*Bnorm 
         
      end do

      allocate(cov(nmodes,nmodes))
      call dsyrk('L','T',nmodes,nsims,1._dp,alm,nsims,0._dp,cov,nmodes)
      
      !now we need to compute logp
      !first we compute the trace part
      do l =2,lmax
         nl = 2*l+1
         Tstart = 3*(l**2 -4) +1 
         Tstop  = Tstart +nl -1
         Estart = Tstop +1
         Estop  = Estart +nl -1
         Bstart = Estop +1 
         Bstop  = Bstart +nl -1
         
         CTT = cl(l,myTT)
         CEE = cl(l,myEE)
         CBB = max(cl(l,myBB),1.d-30)
         CTE = cl(l,myTE)
         
         delta = CTT*CEE -CTE**2
         sumTT = 0._dp
         sumEE = 0._dp
         sumTE = 0._dp
         sumBB = 0._dp
         
         do i = Tstart,Tstop
            sumTT = sumTT +cov(i,i)
            sumTE = sumTE +cov(i+nl,i)
         end do
         do i = Estart,Estop
            sumEE = sumEE +cov(i,i)
         end do
         do i = Bstart,Bstop
            sumBB = sumBB +cov(i,i)
         end do
         
         plogp(l) = CTT*sumEE +CEE*sumTT -2*CTE*sumTE +sumBB/CBB +nl*nmodes*log(delta*CBB)
      end do
      
     call dpotrf('L',nmodes,cov,nmodes,info)
     if (info .ne. 0) then
        stop 'sampled covariance is not invertible'
     end if
     
     detclhat = 0._dp
     do i = 1,nmodes
        detclhat = detclhat + log(cov(i,i))
     end do
     detclhat = 2._dp*detclhat
     
     logp = -2._dp*(sum(plogp) -(nsims-nmodes-1)*detclhat)
     
     call dpotri('L',nmodes,cov,nmodes,info)
     
   end subroutine cl2hatcl_matrix
   
    
 end subroutine get_student_loglike_matrix

! ---------------------------------------------------------------------------

  
  subroutine get_pix_loglike_pp(clsin,alike,argexp,logdet)

    implicit none
    
    real(dp),intent(in)  :: clsin(2:,:)
    real(dp),intent(out) :: alike(:) ,argexp(:) ,logdet(:)

    integer              :: i ,j ,l ,info ,neigen ,nl ,nl2 ,iu
    integer  ,allocatable:: pinfo(:) ,il(:)
    real(dp) ,allocatable:: tm1(:,:) ,tmplog(:)
!
    logdet = 0._dp
    neigen = size(dt(:,1))

    nl2 = 2*(2*lswitch+1)
    allocate(tm1(nl2,nl2) ,tmplog(2:lswitch) ,source = 0._dp)
    allocate(pinfo(2:lswitch) ,il(2:lswitch) ,source = 0)
    iu = 0
    do l=2,lswitch
       il(l) = iu +1
       iu = il(l) +4*l+1 !2*(2l+1) for each l
    end do

!$OMP PARALLELDO DEFAULT (PRIVATE) SHARED(S,ncvm,neigen)
    do i=1,neigen
       S(i:neigen,i) = ncvm(i:neigen,i)
    end do

!!!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(S,neigen,lswitch,clsin,tmplog,cblocks,feval,pinfo,il)
    do l=2,lswitch

       tmplog(l) = 0._dp
    
       nl  = 2*l+1
       nl2 = 2*nl

       ! EE, BB
       ! This is diagonal, but keep matrix structure to include EB later
       do i = 1,nl
          tm1(i,i)        = clsin(l,myEE)*feval(l)%m(i,1)
          tm1(i+1:nl2,i)  = 0.d0
       end do
       do i = nl+1,nl2
          tm1(i,i)        = clsin(l,myBB)*feval(l)%m(i,1)
          tm1(i+1:nl2,i)  = 0.d0
       end do

       call dpotrf('L',nl2,tm1(1:nl2,1:nl2),nl2,pinfo(l))

       do j=1,nl2
          tmplog(l) = tmplog(l) +log(tm1(j,j))
       end do
       call dpotri('L',nl2,tm1(1:nl2,1:nl2),nl2,info)

       do i=1,nl2
          do j=i,nl2
             S(il(l)-1+j,il(l)-1+i) = S(il(l)-1+j,il(l)-1+i) +tm1(j,i)
          end do
       end do

    end do

    logdet(1) = sum(tmplog(2:lswitch))
    
    do l=2,lswitch
       if(pinfo(l).ne.0) then
          write(*,*) 'block inversion failed for l =',l,' info =',pinfo(l)
          argexp = -1.d30
          logdet = -1.d30
          alike  = -1.d30
          deallocate(tmplog,il,tm1,pinfo)
          if(debugging) stop
          return
       end if
    end do

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'S +NCVM ,dt'
       do i = 1,min(15,size(S(:,1)))
          write(*,*) i,i,S(i,i),dt(i,1)
       end do
       write(*,*) '--------------------------'
    end if

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'starting dposv '
       write(*,*) '--------------------------'
    end if

    auxdt = dt
    
    call dposv('L',neigen,ndata,S,neigen,auxdt,neigen,info)

    if(debugging) then
       write(*,*) '--------------------------'
       write(*,*) 'finished dposv '
       write(*,*) 'info = ',info
       write(*,*) '--------------------------'
    end if

!    cholesky: X*(C^-1*X)
    if (info.eq.0) then

       do j=1,neigen
          logdet(1) = logdet(1) +log(S(j,j))
       enddo
       logdet(1) = 2.d0*logdet(1)

       logdet(2:ndata) = logdet(1)

!$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(argexp,dt,auxdt,alike,logdet,reflike,ndata)
       do i=1,ndata
          argexp(i) = reflike(i) -sum(dt(:,i)*auxdt(:,i))
          alike(i)  = -.5d0*(argexp(i)+logdet(i))
       end do

    else

       argexp = 1.d30
       logdet = 1.d30

       alike = -.5d0*(argexp+logdet)
    endif

    if(debugging) then
       write(*,*) '--------------------------'
       do j=1,1
          write(*,*) 'argexp =',argexp(j)
          write(*,*) 'logdet =',logdet(j)
       end do
       write(*,*) '--------------------------'
    end if

    deallocate(tmplog,il,tm1,pinfo)
  end subroutine get_pix_loglike_pp

! ---------------------------------------------------------------------------

  subroutine update_ncvm(clsin,linf,lsup,NCM,project_mondip)
!input clsin are l(l+1)C_l/2pi

    implicit none
    
    real(dp) ,intent(in)           :: clsin(2:,:)
    integer  ,intent(in)           :: linf ,lsup
    real(dp) ,intent(inout)        :: NCM(:,:)
    logical  ,intent(in) ,optional :: project_mondip

    logical  :: pmd

    pmd = .false.
    if(present(project_mondip)) pmd = project_mondip

    if(ntemp .gt. 0) then
       if(debugging) then
          write(*,*) 'updating ncvm between '
          write(*,*) 'linf  = ',linf 
          write(*,*) 'lsup = ',lsup
          write(*,*) ''
          write(*,*) 'TT'
          write(*,*) 'NCVM(1,1) in  :',NCM(1,1)
          write(*,*) 'C_2^TT        :',clsin(2,myTT)
       end if
       call get_tt_cov(clsin,linf,lsup,NCM(1:ntemp,1:ntemp),project_mondip=pmd,symmetrize=.true.,update=.true.)
       if(debugging) then
          write(*,*) 'NCVM(1,1) out :',NCM(1,1)
       end if
    end if

    if(nqu .gt. 0) then
       if(debugging) then
          write(*,*) 'PP'
          write(*,*) 'NCVM(',ntemp+1,',',ntemp+1,') in  :',NCM(ntemp+1,ntemp+1)
          write(*,*) 'C_2^EE        :',clsin(2,myEE)
          write(*,*) 'C_2^BB        :',clsin(2,myBB)
          write(*,*) 'C_2^EB        :',clsin(2,myEB)
       end if
       call get_pp_cov(clsin, linf, lsup, NCM(1+ntemp:ntot,1+ntemp:ntot),&
            symmetrize=.true.,update=.true.)
       if(debugging) then
          write(*,*) 'NCVM(',ntemp+1,',',ntemp+1,') out :',NCM(ntemp+1,ntemp+1)
       end if
    end if

    if(ntemp .gt. 0 .and. nqu .gt.0) then 

       if(debugging) then
          write(*,*) 'TP'
          write(*,*) 'NCVM(',1,',',ntemp+1,') in  :',NCM(1,ntemp+1)
          write(*,*) 'NCVM(',ntemp+1,',',1,') in  :',NCM(ntemp+1,1)
          write(*,*) 'C_2^TE                      :',clsin(2,myTE)
          write(*,*) 'C_2^TB                      :',clsin(2,myTB)
       end if
       call get_xx_cov(clsin, linf, lsup, NCM, symmetrize=.true., &
            update=.true.)
       if(debugging) then
          write(*,*) 'NCVM(',1,',',ntemp+1,') out :',NCM(1,ntemp+1)
          write(*,*) 'NCVM(',ntemp+1,',',1,') out :',NCM(ntemp+1,1) 
       end if
    end if
    
    return

  end subroutine update_ncvm

! ---------------------------------------------------------------------------

  pure subroutine get_rotation_angle(r1,r2,a12,a21)
    !computes TWICE the rotation angle

    implicit none
    real(dp) ,intent(in) ,dimension(3) :: r1,r2
    real(dp) ,intent(out)              :: a12
    real(dp) ,intent(out) ,optional    :: a21

    real(dp) ,parameter :: eps = 3.141592653589793d0/180.d0/3600.d0/100.d0
    real(dp) ,parameter ,dimension(3) :: zz =(/0,0,1/),epsilon =(/eps,0.d0,0.d0/)
    
    real(dp) ,dimension(3) :: r12,r1star,r2star
    real(dp)               :: mod

    call ext_prod(r1,r2,r12)
    mod = sqrt(sum(r12*r12))
    if(mod.lt.1.d-8) then !same or opposite pixels
       a12 = 0.d0
       if(present(a21)) a21 = 0.d0
       return
       
    end if
    r12 = r12/mod

    call ext_prod(zz,r1,r1star)
    r1star(3) = 0.d0
    mod = sqrt(sum(r1star*r1star))
    if(mod.lt.1.d-8) then   !r1 is at a pole            
       r1star = r1star+epsilon
       mod = sqrt(sum(r1star*r1star))
    end if
    r1star = r1star/mod

    call ext_prod(zz,r2,r2star)
    r2star(3) = 0.d0
    mod = sqrt(sum(r2star*r2star))
    if(mod.lt.1.d-8) then   !r2 is at a pole            
       r2star = r2star+epsilon
       mod = sqrt(sum(r2star*r2star))
    end if
    r2star = r2star/mod

    mod = sum(r12*r1star)
    mod = min(1.d0,mod)
    mod = max(-1.d0,mod)
    if(sum(r12*zz).gt.0.d0) then
       a12 = 2.d0*acos(mod)
    else
       a12 = -2.d0*acos(mod)
    end if

    a12 = -a12

    if(present(a21)) then
       r12 = -r12   !r21 = - r12
       mod = sum(r12*r2star)
       mod = min(1.d0,mod)
       mod = max(-1.d0,mod)
       if(sum(r12*zz).gt.0.d0) then
          a21 =  2.d0*acos(mod)
       else
          a21 = -2.d0*acos(mod)
       end if

       a21 = -a21
    end if

  end subroutine get_rotation_angle

! ---------------------------------------------------------------------------

  subroutine  get_QMLB_estimate(lsup,cbsout,cbserr)

    implicit none
    integer(i4b) ,intent(in) :: lsup
    real(dp)    ,intent(out) :: cbsout(2:,:,:) ,cbserr(2:,:)

    integer(i4b)             :: xndata
    real(dp)    ,allocatable :: xout(:,:,:) ,xerr(:,:)
    
    cbsout = 0._dp
    cbserr = 0._dp
    if(has_x) then
       call get_QMLB_estimate_tp(lsup,cbsout,cbserr)
    else
       stop "Bandpower estimation only supported for I,Q,U maps"
    end if

  end subroutine get_QMLB_estimate

! ---------------------------------------------------------------------------

  subroutine  get_QMLB_estimate_tp(lsup,cbsout,cbserr)

    implicit none
    integer(i4b) ,intent(in) :: lsup
    real(dp)    ,intent(out) :: cbsout(2:,:,:) ,cbserr(2:,:)

    integer(i4b)                         :: info ,i ,nb
    integer(i4b)                         :: j1 ,j2 ,l1 ,l2 ,m1 ,nl1 ,nl10 &
         ,nl2 ,nl20
    integer(i8b)                         :: il ,iu
    real(dp) ,allocatable,dimension(:,:) :: cls ,S0 ,auxmat ,auxmat2 ,auxmat3
    real(dp) ,allocatable,dimension(:,:) :: block1 ,block2
    real(dp) ,allocatable                :: FF(:,:,:,:) ,FB(:,:,:,:) &
         ,auxcb(:,:,:)
    type(allocatable_matrix) ,allocatable:: wevec(:) ,cevec(:)

    if(lsup .gt. lswitch) stop 'lsup must be <= lswitch'

    if(debugging) write(*,*) 'starting get_qml_estimate with lsup = ',lsup

    nb = bins%nb 
    
    allocate(cls(2:lsup,6))
    
    if(decouple_tp) then
       allocate(S0,source = S)
       S(1:ntemp,ntemp+1:ntot) = 0._dp
       S(ntemp+1:ntot,1:ntemp) = 0._dp
       call  dpotrf('L',ntemp,S(1:ntemp,1:ntemp),ntemp,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for TT fails'
          stop
       end if

       call  dpotrf('L',nq+nu,S(ntemp+1:ntot,ntemp+1:ntot),nq+nu,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for Pol fails'
          stop
       end if
    else
       call  dpotrf('L',ntot,S,ntot,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for TP fails'
          stop
       end if
    end if

    !2E = C^-1 dC C^-1 = C^-1 Vt dB V C^-1 = Wt dB W

    !whiten all the evecs C^-1 Vt
    allocate(wevec(2:lsup),cevec(2:lsup))
    do l1=2,lsup
       nl1 = 3*(2*l1+1)
       call wevec(l1)%alloc(nrow=ntot,ncol=nl1)
       call cevec(l1)%alloc(nrow=ntot,ncol=nl1)
       wevec(l1)%m = fevec(l1)%m
       call dpotrs( 'L', ntot, nl1, S, ntot, wevec(l1)%m, ntot, INFO )
       if(decouple_tp) then
          call dsymm('L','L', ntot, nl1, 1.d0, S0, ntot, wevec(l1)%m, ntot, &
               0.d0, cevec(l1)%m, ntot)
       else
          cevec(l1)%m = fevec(l1)%m
       end if
    end do
    if(decouple_tp)  deallocate(S0)

    do l1 = 2,lswitch
       call fevec(l1)%clean()
    end do
    deallocate(fevec)

    S = 0._dp
    iu = 0
    do i = 1,ntot
       il = iu +1
       iu = il +(ntot-i)
       S(i:ntot,i) = N0(il:iu,1)
    end do
    call move_alloc(to=N0,from=S)
    
    !get the raw estimates xEx -tr(NE)
    do l1=2,lsup
       nl1 = 3*(2*l1+1)

       !tr(NWt dB W) = tr(WNWt dB)
       !get (WNWt)
       !NWt
       allocate(auxmat,mold=wevec(l1)%m)

       call dsymm('L','L', ntot, nl1, 1.d0, N0, & 
            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)

       allocate(block1(nl1,nl1))
       !WNWt
       call dgemm('T','N', nl1, nl1, ntot, 1.d0, wevec(l1)%m, &
            ntot, auxmat, ntot, 0.d0, block1, nl1)
       deallocate(auxmat)
       
       allocate(auxmat(nl1,ndata),auxmat2(nl1,ndata))

       !Wx
       call dgemm('T','N', nl1, ndata, ntot, 1.d0, wevec(l1)%m, &
            ntot, dt, ntot, 0.d0, auxmat, nl1)

       allocate(block2(nl1,nl1))
       block2 = transpose(block1)

       do j1=1,6
          cls = 0._dp
          cls(l1,j1) = l1*(l1+1.d0)/twopi
          call cls2block(l1,cls,feval(l1)%m(:,1),cblocks(l1,:),block1)
          !tr(NE)
          do i = 1,nl1
             cbsout(l1,j1,1) = cbsout(l1,j1,1) - sum(block2(:,i)*block1(:,i))
          end do
          cbsout(l1,j1,2:ndata) = cbsout(l1,j1,1) 

          !xEx 
          call dgemm('N','N',nl1,ndata,nl1,1.d0,block1,nl1,auxmat,nl1, &
               0.d0,auxmat2,nl1)

          do i=1,ndata
             cbsout(l1,j1,i) = cbsout(l1,j1,i) +sum(auxmat(:,i)*auxmat2(:,i))
          end do
       end do
       deallocate(block1,block2,auxmat,auxmat2)
    end do
    cbsout = cbsout*0.5_dp
        
    allocate(auxcb(nb,6,ndata))
    do i = 1,ndata
       do j1 = 1,6
          auxcb(:,j1,i) = bins%Cl2band(cbsout(:,j1,i),.true.)
       end do
    end do
    
    !compute fisher
    !F = tr(dC C^-1 dC C^-1)/2 = tr( Vt dB V C^-1 Vt dB V C^-1)/2
    allocate(FF(2:lsup,2:lsup,6,6))
    FF = 0.d0
    !$OMP PARALLELDO DEFAULT(PRIVATE) SHARED(FF,lsup,cevec,wevec,cblocks,feval,ntot)
    do l1 = 2,lsup
       write(0,*) l1
       nl10 = (2*l1+1)
       nl1  = 3*nl10
       allocate(block1(nl1,nl1))

       do l2 = 2,lsup
          nl20 = (2*l2+1)
          nl2  = 3*nl20
          allocate(block2(nl2,nl2))
          allocate(auxmat(nl1,nl2) ,auxmat2(nl1,nl2) ,auxmat3(nl2,nl1))
          auxmat  =0.d0
          call dgemm('T','N', nl1, nl2, ntot, 1.d0, cevec(l1)%m, ntot, &
               wevec(l2)%m, ntot, 0.d0, auxmat, nl1)

          do j1 = 1,6
             cls = 0.d0
             cls(l1,j1) = l1*(l1+1.d0)/twopi
             call cls2block(l1,cls,feval(l1)%m(:,1),cblocks(l1,:),block1)

             select case(j1) 
             case(myTT)
                auxmat2 = 0._dp
                call dgemm('N','N', nl10, nl2, nl10, 1.d0, &
                     block1(1:nl10,1:nl10), nl10, auxmat(1:nl10,:),&
                     nl10, 0.d0, auxmat2(1:nl10,:), nl10)
             case(myEE)
                auxmat2 = 0._dp
                call dgemm('N','N', nl10, nl2, nl10, 1.d0, &
                     block1(nl10+1:2*nl10,nl10+1:2*nl10), nl10, auxmat(nl10+1:2*nl10,:),&
                     nl10, 0.d0, auxmat2(nl10+1:2*nl10,:), nl10)

             case(myBB)
                auxmat2 = 0._dp
                call dgemm('N','N', nl10, nl2, nl10, 1.d0, &
                     block1(2*nl10+1:nl1,2*nl10+1:nl1), nl10, auxmat(2*nl10+1:nl1,:),&
                     nl10, 0.d0, auxmat2(2*nl10+1:nl1,:), nl10)

             case(myEB)
                auxmat2 = 0._dp
                call dgemm('N','N', 2*nl10, nl2, 2*nl10, 1.d0, &
                     block1(nl10+1:nl1,nl10+1:nl1), 2*nl10, auxmat(nl10+1:nl1,:),&
                     2*nl10, 0.d0, auxmat2(nl10+1:nl1,:), 2*nl10)

             case(myTE,myTB)
                call dgemm('N','N', nl1, nl2, nl1, 1.d0, block1, nl1, auxmat,&
                     nl1, 0.d0, auxmat2, nl1)
             end select

             do j2 = j1,6
                cls = 0.d0
                cls(l2,j2) = l2*(l2+1.d0)/twopi
                call cls2block(l2,cls,feval(l2)%m(:,1),cblocks(l2,:),block2)
                !B_2VW
                select case(j2)
                case(myTT)
                   auxmat3 = 0._dp
                   call dgemm('N','T', nl20, nl1, nl20, 1.d0, &
                        block2(1:nl20,1:nl20), nl20, &
                        auxmat(:,1:nl20), nl1, 0.d0, auxmat3(1:nl20,:), nl20)
                case(myEE)
                   auxmat3 = 0._dp
                   call dgemm('N','T', nl20, nl1, nl20, 1.d0, &
                        block2(nl20+1:2*nl20,nl20+1:2*nl20), nl20, &
                        auxmat(:,nl20+1:2*nl20), nl1, 0.d0, auxmat3(nl20+1:2*nl20,:), nl20)
                case(myBB)
                   auxmat3 = 0._dp
                   call dgemm('N','T', nl20, nl1, nl20, 1.d0, &
                        block2(2*nl20+1:nl2,2*nl20+1:nl2), nl20, &
                        auxmat(:,2*nl20+1:nl2), nl1, 0.d0, auxmat3(2*nl20+1:nl2,:), nl20)
                case(myEB)
                   auxmat3 = 0._dp
                   call dgemm('N','T', 2*nl20, nl1, 2*nl20, 1.d0, &
                        block2(nl20+1:nl2,nl20+1:nl2), 2*nl20, &
                        auxmat(:,nl20+1:nl2), nl1, 0.d0, &
                        auxmat3(nl20+1:nl2,:), 2*nl20)
                case(myTE,myTB)                
                   call dgemm('N','T', nl2, nl1, nl2, 1.d0, block2, nl2, &
                        auxmat, nl1, 0.d0, auxmat3, nl2)
                end select
                do m1 = 1,nl1
                   FF(l2,l1,j2,j1) = sum(auxmat2(m1,:)*auxmat3(:,m1)) &
                        + FF(l2,l1,j2,j1) 
                end do

             end do
          end do
          deallocate(auxmat,auxmat2,auxmat3)
          deallocate(block2)
       end do
       deallocate(block1)
    end do
    FF = FF*0.5_dp
    
    !now fill in the missing parts
    do j1 = 1,6
       do j2 = 1,j1-1
          do l1 = 2,lsup
             do l2 = 2,lsup
                FF(l2,l1,j2,j1) = FF(l1,l2,j1,j2)
             end do
          end do
       end do
    end do

    !now bin fisher
    allocate(FB(nb,nb,6,6))
    allocate(auxmat(2:lsup,nb))
    do j1 = 1,6
       do j2 = 1,6
          do l1 = 2,lsup
             auxmat(l1,:) = bins%Cl2band(FF(l1,:,j1,j2),.true.)
          end do
          do l1 = 1,nb
             FB(:,l1,j1,j2) = bins%Cl2band(auxmat(:,l1),.true.)
          end do
       end do
    end do
    deallocate(auxmat)
    call move_alloc(from=FB,to=FF)

    nl1 = nb
    allocate(auxmat(6*nl1,6*nl1))
    do j1=1,6
       do j2=1,6
          auxmat((j2-1)*nl1 +1:j2*nl1,(j1-1)*nl1 +1:j1*nl1) = &
               FF(:,:,j2,j1)
       end do
    end do
    
    allocate(auxmat2(6*nl1,ndata))
    do i = 1,ndata
       do j1 = 1,6
          auxmat2((j1-1)*nl1 +1:j1*nl1,i) = auxcb(:,j1,i)
       end do
    end do

    call dposv('L',6*nl1,ndata,auxmat,6*nl1,auxmat2,6*nl1,info)

    if(info .ne. 0) then
       write(*,*) 'TP Fisher matrix is not invertible info = ',info
       stop
    end if

    do i = 1,ndata
       do j1 = 1,6
          cbsout(2:lsup,j1,i) = bins%band2Cl(auxmat2((j1-1)*nl1 +1:j1*nl1,i))
       end do
    end do
    
    call dpotri('L',6*nl1,auxmat,6*nl1,info)

    allocate(auxmat3(nl1,1))
    do j1 = 1,6
       do l1=1,nb
          auxmat3(l1,1) = sqrt(auxmat((j1-1)*nl1 +l1,(j1-1)*nl1 +l1))
       end do
       cbserr(:,j1) = bins%band2Cl(auxmat3(:,1))
    end do

    deallocate(auxmat,auxmat2,FF)

    write(*,*) 'done get_qmlb_estimate'

  end subroutine get_QMLB_estimate_tp
  
! ---------------------------------------------------------------------------

  subroutine  get_QML_estimate(lsup,clsout,clserr)
!    use lpl_healpix_utils_mod

    implicit none
    integer(i4b) ,intent(in) :: lsup
    real(dp)    ,intent(out) :: clsout(2:,:,:) ,clserr(2:,:)

    integer(i4b)             :: xndata ,i ,j
    real(dp)    ,allocatable :: xout(:,:,:) ,xerr(:,:)
    
    clsout = 0._dp
    clserr = 0._dp

    if(bins%nb .ge. 1) then
       if(has_x) then
          call get_QMLB_estimate_tp(lsup,clsout,clserr)
          !clserr(:,:) = bins%band2Cl(xerr(:,:))
          !do j = 1,xndata
          !   clsout(:,:,j) = bins%band2Cl(xout(:,:,j))
          !end do
          !deallocate(xout ,xerr)
       else
          stop "Bandpower estimation only supported for I,Q,U maps"
       end if
    else
       if(has_x) then
          call get_QML_estimate_tp(lsup,clsout,clserr)
       elseif(has_t) then
          call get_QML_estimate_tt(lsup,clsout(2:,1,:),clserr(2:,1))
       else
          xndata =  size(clsout(2,1,:))
          allocate(xout(2:lsup,3,xndata) ,source = 0._dp)
          allocate(xerr(2:lsup,3)        ,source = 0._dp)
          call get_QML_estimate_pp(lsup,xout,xerr)
          clsout(:,myEE,:) = xout(:,1,:)
          clsout(:,myBB,:) = xout(:,2,:)
          clsout(:,myEB,:) = xout(:,3,:)
          clserr(:,myEE)   = xerr(:,1)
          clserr(:,myBB)   = xerr(:,2)
          clserr(:,myEB)   = xerr(:,3)
          deallocate(xout ,xerr)
       end if
    end if
  end subroutine get_QML_estimate
  
! ---------------------------------------------------------------------------

  subroutine  get_QML_estimate_tp(lsup,clsout,clserr)

    implicit none
    integer(i4b) ,intent(in) :: lsup
    real(dp)    ,intent(out) :: clsout(2:,:,:) ,clserr(2:,:)

    integer(i4b)                         :: info ,i ,n  
    integer(i4b)                         :: j1 ,j2 ,l1 ,l2 ,m1 ,nl1 ,nl2
    integer(i8b)                         :: il ,iu
    real(dp) ,allocatable,dimension(:,:) :: cls ,S0 ,auxmat ,auxmat2 ,auxmat3
    real(dp) ,allocatable,dimension(:,:) :: block1 ,block2
    real(dp) ,allocatable                :: FF(:,:,:,:) 
    type(allocatable_matrix) ,allocatable:: wevec(:) ,cevec(:)
    
    if(lsup .gt. lswitch) stop 'lsup must be <= lswitch'

    if(debugging) write(*,*) 'starting get_qml_estimate with lsup = ',lsup

    allocate(cls(2:lsup,6))
    
    if(decouple_tp) then
       allocate(S0,source = S)
       S(1:ntemp,ntemp+1:ntot) = 0._dp
       S(ntemp+1:ntot,1:ntemp) = 0._dp
       call  dpotrf('L',ntemp,S(1:ntemp,1:ntemp),ntemp,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for TT fails'
          stop
       end if

       call  dpotrf('L',nq+nu,S(ntemp+1:ntot,ntemp+1:ntot),nq+nu,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for Pol fails'
          stop
       end if
    else
       call  dpotrf('L',ntot,S,ntot,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'QML: Cholesky for TP fails'
          write(*,*) 'decouple_tp: ',decouple_tp
          stop
       end if
    end if

    !2E = C^-1 dC C^-1 = C^-1 Vt dB V C^-1 = Wt dB W

    !whiten all the evecs C^-1 Vt
    allocate(wevec(2:lsup),cevec(2:lsup))
    do l1=2,lsup
       nl1 = 3*(2*l1+1)

       call wevec(l1)%alloc(nrow=ntot,ncol=nl1)
       call cevec(l1)%alloc(nrow=ntot,ncol=nl1)
       wevec(l1)%m = fevec(l1)%m
       call dpotrs( 'L', ntot, nl1, S, ntot, wevec(l1)%m, ntot, INFO )
       if(decouple_tp) then
          call dsymm('L','L', ntot, nl1, 1.d0, S0, ntot, wevec(l1)%m, ntot, &
               0.d0, cevec(l1)%m, ntot)
       else
          cevec(l1)%m = fevec(l1)%m
       end if
    end do
    if(decouple_tp)  deallocate(S0)


    S = 0._dp
    iu = 0
    do i = 1,ntot
       il = iu +1
       iu = il +(ntot-i)
       S(i:ntot,i) = N0(il:iu,1)
    end do
    call move_alloc(to=N0,from=S)
           
    !get the raw estimates xEx -tr(NE)
    do l1=2,lsup
       nl1 = 3*(2*l1+1)
       !tr(NWt dB W) = tr(WNWt dB)
       !get (WNWt)
       !NWt
       allocate(auxmat,mold=wevec(l1)%m)

       call dsymm('L','L', ntot, nl1, 1.d0, N0, & 
            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)
!       call dgemm('N','N', ntot, nl1, ntot, 1.d0, N0, &
!            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)

       allocate(block1(nl1,nl1))
       !WNWt
       call dgemm('T','N', nl1, nl1, ntot, 1.d0, wevec(l1)%m, &
            ntot, auxmat, ntot, 0.d0, block1, nl1)
       deallocate(auxmat)
       
       allocate(auxmat(nl1,ndata),auxmat2(nl1,ndata))

       !Wx
       call dgemm('T','N', nl1, ndata, ntot, 1.d0, wevec(l1)%m, &
            ntot, dt, ntot, 0.d0, auxmat, nl1)

       allocate(block2(nl1,nl1))
       block2 = transpose(block1)

       do j1=1,6
          cls = 0._dp
          cls(l1,j1) = l1*(l1+1.d0)/twopi
          call cls2block(l1,cls,feval(l1)%m(:,1),cblocks(l1,:),block1)
          !tr(NE)
          if (debias) then
             do n = 1,nl1
                clsout(l1,j1,1) = clsout(l1,j1,1) - sum(block2(:,n)*block1(:,n))
             end do
          else
             clsout(l1,j1,1) = 0._dp
          end if
          clsout(l1,j1,2:ndata) = clsout(l1,j1,1) 

          !xEx 
          call dgemm('N','N',nl1,ndata,nl1,1.d0,block1,nl1,auxmat,nl1, &
               0.d0,auxmat2,nl1)

          do i=1,ndata
             clsout(l1,j1,i) = clsout(l1,j1,i) +sum(auxmat(:,i)*auxmat2(:,i))
          end do
       end do
       deallocate(block1,block2,auxmat,auxmat2)
    end do
    clsout = clsout*0.5_dp 

    !compute fisher
    !F = tr(dC C^-1 dC C^-1)/2 = tr( Vt dB V C^-1 Vt dB V C^-1)/2
    allocate(FF(2:lsup,2:lsup,6,6))
    FF = 0.d0
    do l1 = 2,lsup

       nl1 = 3*(2*l1+1)
       allocate(block1(nl1,nl1))

       do l2 = 2,lsup
          nl2 = 3*(2*l2+1)
          allocate(block2(nl2,nl2))
          allocate(auxmat(nl1,nl2) ,auxmat2(nl1,nl2) ,auxmat3(nl2,nl1))
          auxmat  =0.d0
          call dgemm('T','N', nl1, nl2, ntot, 1.d0, cevec(l1)%m, ntot, &
               wevec(l2)%m, ntot, 0.d0, auxmat, nl1)

          do j1 = 1,6
             cls = 0.d0
             cls(l1,j1) = l1*(l1+1.d0)/twopi
             call cls2block(l1,cls,feval(l1)%m(:,1),cblocks(l1,:),block1)
             auxmat2 = 0.d0
             call dgemm('N','N', nl1, nl2, nl1, 1.d0, block1, nl1, auxmat,&
                  nl1, 0.d0, auxmat2, nl1)

             do j2 = j1,6
                cls = 0.d0
                cls(l2,j2) = l2*(l2+1)/twopi
                call cls2block(l2,cls,feval(l2)%m(:,1),cblocks(l2,:),block2)
                !B_2VW
                auxmat3 = 0.d0

                call dgemm('N','T', nl2, nl1, nl2, 1.d0, block2, nl2, &
                     auxmat, nl1, 0.d0, auxmat3, nl2)
                !Trace
                do m1 = 1,nl1
                   FF(l2,l1,j2,j1) = sum(auxmat2(m1,:)*auxmat3(:,m1)) &
                        + FF(l2,l1,j2,j1) 
                end do
                !FF(l2,l1,j2,j1) = FF(l2,l1,j2,j1)*.5_dp

             end do
          end do
          deallocate(auxmat,auxmat2,auxmat3)
          deallocate(block2)
       end do
       deallocate(block1)
    end do
    FF = FF*0.5_dp

    !now fill in the missing parts
    do j1 = 1,6
       do j2 = 1,j1-1
          do l1 = 2,lsup
             do l2 = 2,lsup
                FF(l2,l1,j2,j1) = FF(l1,l2,j1,j2)
             end do
          end do
       end do
    end do

    nl1 = lsup-1
    allocate(auxmat(6*nl1,6*nl1))
    do j1=1,6
       do j2=1,6
          auxmat((j2-1)*nl1 +1:j2*nl1,(j1-1)*nl1 +1:j1*nl1) = &
               FF(2:lsup,2:lsup,j2,j1)
       end do
    end do

    inquire(iolength=nl2) auxmat
    open(newunit=l2,file='fisher_dump.unf',status='unknown',action='write',form='unformatted',access='direct',recl=nl2)
    write(l2,rec=1) auxmat
    close(l2)
    
    allocate(auxmat2(6*nl1,ndata))
    do n = 1,ndata
       do j1 = 1,6
          auxmat2((j1-1)*nl1 +1:j1*nl1,n) = clsout(2:lsup,j1,n)
       end do
    end do

    call dposv('L',6*nl1,ndata,auxmat,6*nl1,auxmat2,6*nl1,info)

    if(info .ne. 0) then
       write(*,*) 'TP Fisher matrix is not invertible info = ',info
       stop
    end if

!    write(0,*) "NNNNOOOOOOOOOOOOOOOOOOOO!!!!!!!!!!!!!"
    do n = 1,ndata
       do j1 = 1,6
          clsout(2:lsup,j1,n) = auxmat2((j1-1)*nl1 +1:j1*nl1,n) 
       end do
    end do

    call dpotri('L',6*nl1,auxmat,6*nl1,info)
    do j1 = 1,6
       do l1=2,lsup
          clserr(l1,j1) = sqrt(auxmat((j1-1)*nl1 +(l1-1),(j1-1)*nl1 +(l1-1)))
       end do
    end do

    deallocate(auxmat,auxmat2,FF)

    write(*,*) 'done get_qml_estimate'

  end subroutine get_QML_estimate_tp

! ---------------------------------------------------------------------------

  subroutine  get_QML_estimate_pp(lsup,clsout,clserr)

    implicit none
    integer(i4b) ,intent(in) :: lsup
    real(dp)    ,intent(out) :: clsout(2:,:,:) ,clserr(2:,:)

    integer(i4b)                         :: info ,i ,n  
    integer(i4b)                         :: j1 ,j2 ,l1 ,l2 ,m1 ,nl1 ,nl2
    integer(i8b)                         :: il ,iu
    real(dp) ,allocatable,dimension(:,:) :: cls ,auxmat ,auxmat2 ,auxmat3
    real(dp) ,allocatable,dimension(:,:) :: block1 ,block2
    real(dp) ,allocatable                :: FF(:,:,:,:) 
    type(allocatable_matrix) ,allocatable:: wevec(:) ,cevec(:)

    if(lsup .gt. lswitch) stop 'lsup must be <= lswitch'

    if(debugging) write(*,*) 'starting get_qml_estimate with lsup = ',lsup

    allocate(cls(2:lsup,3))

    call  dpotrf('L',ntot,S,ntot,info)
    if(info.ne.0) then
       write(*,*) 'INFO =',info
       write(*,*) 'Cholesky for PP fails'
       stop
    end if

    !2E = C^-1 dC C^-1 = C^-1 Vt dB V C^-1 = Wt dB W

    !whiten all the evecs C^-1 Vt
    allocate(wevec(2:lsup),cevec(2:lsup))
    do l1=2,lsup
       nl1 = 2*(2*l1+1)
       call wevec(l1)%alloc(nrow=ntot,ncol=nl1)
       call cevec(l1)%alloc(nrow=ntot,ncol=nl1)
       wevec(l1)%m = fevec(l1)%m
       call dpotrs( 'L', ntot, nl1, S, ntot, wevec(l1)%m, ntot, INFO )
       cevec(l1)%m = fevec(l1)%m
    end do

    S = 0._dp
    iu = 0
    do i = 1,ntot
       il = iu +1
       iu = il +(ntot-i)
       S(i:ntot,i) = N0(il:iu,1)
    end do
    call move_alloc(to=N0,from=S)

    !get the raw estimates xEx -tr(NE)
    do l1=2,lsup
       nl1 = 2*(2*l1+1)
       !tr(NWt dB W) = tr(WNWt dB)
       !get (WNWt)
       !NWt
       allocate(auxmat,mold=wevec(l1)%m)
       call dsymm('L','L', ntot, nl1, 1.d0, N0, & 
            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)
!       call dgemm('N','N', ntot, nl1, ntot, 1.d0, N0, &
!            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)

       allocate(block1(nl1,nl1) ,source = 0._dp)
       !WNWt
       call dgemm('T','N', nl1, nl1, ntot, 1.d0, wevec(l1)%m, &
            ntot, auxmat, ntot, 0.d0, block1, nl1)
       deallocate(auxmat)
       
       allocate(auxmat(nl1,ndata),auxmat2(nl1,ndata))

       !Wx
       call dgemm('T','N', nl1, ndata, ntot, 1.d0, wevec(l1)%m, &
            ntot, dt, ntot, 0.d0, auxmat, nl1)

       allocate(block2(nl1,nl1))
       block2 = transpose(block1)

       do j1=1,3
          cls = 0._dp
          cls(l1,j1) = l1*(l1+1.d0)/twopi
          call cls2block_p(l1,cls,feval(l1)%m(:,1),cblocks(l1,:),block1)
          !tr(NE)
          do n = 1,nl1
             clsout(l1,j1,1) = clsout(l1,j1,1) - sum(block2(:,n)*block1(:,n))
          end do
          clsout(l1,j1,2:ndata) = clsout(l1,j1,1) 

          !xEx 
          call dgemm('N','N',nl1,ndata,nl1,1.d0,block1,nl1,auxmat,nl1, &
               0.d0,auxmat2,nl1)

          do i=1,ndata
             clsout(l1,j1,i) = clsout(l1,j1,i) +sum(auxmat(:,i)*auxmat2(:,i))
          end do
       end do
       deallocate(block1,block2,auxmat,auxmat2)
    end do
    clsout = clsout*0.5_dp 

    !compute fisher
    !F = tr(dC C^-1 dC C^-1)/2 = tr( Vt dB V C^-1 Vt dB V C^-1)/2
    allocate(FF(2:lsup,2:lsup,3,3))
    FF = 0.d0
    do l1 = 2,lsup
       nl1 = 2*(2*l1+1)
       allocate(block1(nl1,nl1))
       do j1 = 1,3
          cls = 0.d0
          cls(l1,j1) = l1*(l1+1.d0)/twopi
          call cls2block_p(l1,cls,feval(l1)%m(:,1),cblocks(l1,:),block1)

          do l2 = 2,lsup
             nl2 = 2*(2*l2+1)
             allocate(block2(nl2,nl2))
             allocate(auxmat(nl1,nl2) ,auxmat2(nl1,nl2) ,auxmat3(nl2,nl1))
             auxmat  =0.d0

             call dgemm('T','N', nl1, nl2, ntot, 1.d0, cevec(l1)%m, ntot, &
                  wevec(l2)%m, ntot, 0.d0, auxmat, nl1)

             call dgemm('N','N', nl1, nl2, nl1, 1.d0, block1, nl1, auxmat,&
                  nl1, 0.d0, auxmat2, nl1)

             do j2 = 1,3
                cls = 0.d0
                cls(l2,j2) = l2*(l2+1.d0)/twopi
                call cls2block_p(l2,cls,feval(l2)%m(:,1),cblocks(l2,:),block2)
                !B_2VW
                call dgemm('N','T', nl2, nl1, nl2, 1.d0, block2, nl2, &
                     auxmat, nl1, 0.d0, auxmat3, nl2)
                do m1 = 1,nl1
                   FF(l2,l1,j2,j1) = sum(auxmat2(m1,:)*auxmat3(:,m1)) &
                        + FF(l2,l1,j2,j1) 
                end do

             end do
             deallocate(auxmat,auxmat2,auxmat3)
             deallocate(block2)
          end do
       end do
       deallocate(block1)
    end do
    FF = FF*0.5_dp

    nl1 = lsup-1
    allocate(auxmat(3*nl1,3*nl1))
    do j1=1,3
       do j2=1,3
          auxmat((j2-1)*nl1 +1:j2*nl1,(j1-1)*nl1 +1:j1*nl1) = &
               FF(2:lsup,2:lsup,j2,j1)
       end do
    end do

    allocate(auxmat2(3*nl1,ndata))
    do n = 1,ndata
       do j1 = 1,3
          auxmat2((j1-1)*nl1 +1:j1*nl1,n) = clsout(2:lsup,j1,n)
       end do
    end do

    call dposv('L',3*nl1,ndata,auxmat,3*nl1,auxmat2,3*nl1,info)

    if(info .ne. 0) then
       write(*,*) 'P Fisher matrix is not invertible info = ',info
       stop
    end if

    do n = 1,ndata
       do j1 = 1,3
          clsout(2:lsup,j1,n) = auxmat2((j1-1)*nl1 +1:j1*nl1,n) 
       end do
    end do
    call dpotri('L',3*nl1,auxmat,3*nl1,info)

    do j1 = 1,3
       do l1=2,lsup
          clserr(l1,j1) = sqrt(auxmat((j1-1)*nl1 +(l1-1),(j1-1)*nl1 +(l1-1)))
       end do
    end do

    deallocate(auxmat,auxmat2,FF)

    write(*,*) 'done get_qml_estimate'

  end subroutine get_QML_estimate_pp

! ---------------------------------------------------------------------------

  subroutine  get_QML_estimate_tt(lsup,clsout,clserr)

    implicit none
    integer(i4b) ,intent(in) :: lsup
    real(dp)    ,intent(out) :: clsout(2:,:) ,clserr(2:)

    integer(i4b)                         :: info ,i ,n  
    integer(i4b)                         :: l1 ,l2 ,m1 ,nl1 ,nl2
    integer(i8b)                         :: il ,iu
    real(dp) ,allocatable,dimension(:,:) :: auxmat ,auxmat2 ,auxmat3
    real(dp) ,allocatable,dimension(:,:) :: block1 ,block2
    real(dp) ,allocatable                :: FF(:,:) ,cls(:)
    type(allocatable_matrix) ,allocatable :: wevec(:) ,cevec(:)

    if(lsup .gt. lswitch) stop 'lsup must be <= lswitch'
    if(debugging) write(*,*) 'starting get_qml_estimate with lsup = ',lsup

    allocate(cls(2:lsup) ,source = 0._dp)

    call  dpotrf('L',ntot,S,ntot,info)
    if(info.ne.0) then
       write(*,*) 'INFO =',info
       write(*,*) 'Cholesky for TT fails'
       stop
    end if

    !2E = C^-1 dC C^-1 = C^-1 Vt dB V C^-1 = Wt dB W

    !whiten all the evecs C^-1 Vt
    allocate(wevec(2:lsup),cevec(2:lsup))
    do l1=2,lsup
       nl1 = 2*l1+1
       call wevec(l1)%alloc(nrow=ntot,ncol=nl1)
       call cevec(l1)%alloc(nrow=ntot,ncol=nl1)
       wevec(l1)%m = fevec(l1)%m
       call dpotrs( 'L', ntot, nl1, S, ntot, wevec(l1)%m, ntot, INFO )
       cevec(l1)%m = fevec(l1)%m
    end do

    S = 0._dp
    iu = 0
    do i = 1,ntot
       il = iu +1
       iu = il +(ntot-i)
       S(i:ntot,i) = N0(il:iu,1)
    end do
    call move_alloc(to=N0,from=S)
    
    !get the raw estimates xEx -tr(NE)
    do l1=2,lsup
       nl1 = 2*l1+1
       !tr(NWt dB W) = tr(WNWt dB)
       !get (WNWt)
       !NWt
       allocate(auxmat,mold=wevec(l1)%m)
       call dsymm('L','L', ntot, nl1, 1.d0, N0, & 
            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)
!       call dgemm('N','N', ntot, nl1, ntot, 1.d0, N0, &
!            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)
       
       allocate(block1(nl1,nl1) ,source = 0._dp)
       !WNWt
       call dgemm('T','N', nl1, nl1, ntot, 1.d0, wevec(l1)%m, &
            ntot, auxmat, ntot, 0.d0, block1, nl1)
       deallocate(auxmat)
       
       allocate(auxmat(nl1,ndata),auxmat2(nl1,ndata) ,source = 0._dp)

       !Wx
       call dgemm('T','N', nl1, ndata, ntot, 1.d0, wevec(l1)%m, &
            ntot, dt, ntot, 0.d0, auxmat, nl1)

       allocate(block2 ,mold = transpose(block1)) !workaround for gfortran issue
       block2 = transpose(block1)

       cls     = 0._dp
       cls(l1) = l1*(l1+1.d0)/twopi

       block1  = 0._dp
       do n = 1,nl1
          block1(n,n) = feval(l1)%m(n,1)*cls(l1)
       end do

       !tr(NE)
       do n = 1,nl1
          clsout(l1,1) = clsout(l1,1) - sum(block2(:,n)*block1(:,n))
       end do
       clsout(l1,2:ndata) = clsout(l1,1) 

       !xEx 
       call dgemm('N','N',nl1,ndata,nl1,1.d0,block1,nl1,auxmat,nl1, &
            0.d0,auxmat2,nl1)

       do i=1,ndata
          clsout(l1,i) = clsout(l1,i) +sum(auxmat(:,i)*auxmat2(:,i))
       end do
       deallocate(block1,block2,auxmat,auxmat2)
    end do
    clsout = clsout*0.5_dp 

    !compute fisher
    !F = tr(dC C^-1 dC C^-1)/2 = tr( Vt dB W Vt dB W )/2 =
    !  = tr(dB W Vt dB W Vt)/2
    allocate(FF(2:lsup,2:lsup) ,source = 0._dp)
    do l1 = 2,lsup
       nl1 = 2*l1+1
       allocate(block1(nl1,nl1) ,source = 0._dp)
       cls = 0.d0
       cls(l1) = l1*(l1+1.d0)/twopi
       do n = 1,nl1
          block1(n,n) = feval(l1)%m(n,1)*cls(l1)
       end do
       
       do l2 = l1,lsup
          nl2 = 2*l2+1
          allocate(block2(nl2,nl2) ,source = 0._dp)
          allocate(auxmat(nl1,nl2) ,auxmat2(nl1,nl2) ,auxmat3(nl2,nl1) &
               ,source = 0._dp)

          ! V_l1 W_l2 -> auxmat
          call dgemm('T','N', nl1, nl2, ntot, 1.d0, cevec(l1)%m, ntot, &
               wevec(l2)%m, ntot, 0.d0, auxmat, nl1)

          ! B_l1 V_l1 W_l2 -> auxmat2
          call dgemm('N','N', nl1, nl2, nl1, 1.d0, block1, nl1, auxmat,&
               nl1, 0.d0, auxmat2, nl1)

          cls = 0.d0
          cls(l2) = l2*(l2+1.d0)/twopi
          do n = 1,nl2
             block2(n,n) = feval(l2)%m(n,1)*cls(l2)
          end do
          !B_l2 V_l2 W_l1 = B_l2 (V_l1 W_l2)t ->auxmat3
          call dgemm('N','T', nl2, nl1, nl2, 1.d0, block2, nl2, &
               auxmat, nl1, 0.d0, auxmat3, nl2)

          do m1 = 1,nl1
             FF(l2,l1) = sum(auxmat2(m1,:)*auxmat3(:,m1)) &
                        + FF(l2,l1) 
          end do

          deallocate(auxmat,auxmat2,auxmat3)
          deallocate(block2)
       end do
       deallocate(block1)
    end do
    FF = FF*0.5_dp

    nl1 = lsup-1
    allocate(auxmat(nl1,nl1))
    auxmat(1:nl1,1:nl1) = FF(2:lsup,2:lsup)

    allocate(auxmat2(nl1,ndata) ,source = 0._dp)
    do n = 1,ndata
       auxmat2(1:nl1,n) = clsout(2:lsup,n)
    end do

    call dposv('L',nl1,ndata,auxmat,nl1,auxmat2,nl1,info)

    if(info .ne. 0) then
       write(*,*) 'T Fisher matrix is not invertible info = ',info
       stop
    end if

    do n = 1,ndata
       clsout(2:lsup,n) = auxmat2(1:nl1,n) 
    end do
    call dpotri('L',nl1,auxmat,nl1,info)

    do l1=2,lsup
       clserr(l1) = sqrt(auxmat(l1-1,l1-1))
    end do

    deallocate(auxmat,auxmat2,FF)

    write(*,*) 'done get_qml_estimate'

  end subroutine get_QML_estimate_tt

! ---------------------------------------------------------------------------

  subroutine  get_QMLM_estimate(lsup,modes,moderr,cov)

    implicit none
    integer(i4b) ,intent(in)  :: lsup
    real(dp)     ,intent(out) :: modes(:,:,:) ,moderr(:,:) ,cov(:,:)

    integer(i4b)             :: xndata ,xnmode
    real(dp)    ,allocatable :: xout(:,:,:) ,xerr(:,:)
    
    modes  = 0._dp
    moderr = 0._dp
    cov    = 0._dp
    if(has_x) then
       call get_QMLM_estimate_tp(lsup,modes,moderr,cov)
    elseif(has_t) then
       xnmode = size(modes(:,1,1))
       call get_QMLM_estimate_tt(lsup,modes(:,1,:),moderr(:,1),cov(1:xnmode,1:xnmode))
    else
       xndata = size(modes(1,1,:))
       xnmode = size(modes(:,1,1))
       allocate(xout(xnmode,3,xndata) ,source = 0._dp)
       allocate(xerr(xnmode,3)        ,source = 0._dp)
       call get_QMLM_estimate_pp(lsup,xout,xerr,cov(1:xnmode,1:xnmode))
       modes(:,myEE,:) = xout(:,1,:)
       modes(:,myBB,:) = xout(:,2,:)
       modes(:,myEB,:) = xout(:,3,:)
       moderr(:,myEE)   = xerr(:,1)
       moderr(:,myBB)   = xerr(:,2)
       moderr(:,myEB)   = xerr(:,3)
       deallocate(xout ,xerr)
    end if

    write(*,*) 'done get_QMLM_estimate'
    
  end subroutine get_QMLM_estimate
  
  ! ---------------------------------------------------------------------------

  subroutine  get_QMLM_estimate_tp(lsup,modes,moderr,cov)

    implicit none
    integer(i4b) ,intent(in)  :: lsup
    real(dp)     ,intent(out) :: modes(:,:,:) ,moderr(:,:) ,cov(:,:)

    integer(i4b) :: info ,i ,n ,l1 ,l2 ,m1 ,nl0 ,nl1 ,nl2 ,imode1 ,imode2 &
         ,im1 ,im2 ,nmodes ,nmodes_tot ,imstart ,j1 ,j2 ,nl02

    integer(i8b)                         :: il ,iu
    real(dp) ,allocatable,dimension(:,:) :: cls ,S0 ,auxmat ,auxmat2 ,auxmat3
    real(dp) ,allocatable,dimension(:,:) :: block1 ,block2 ,wtmp ,vtmp ,atmp
    real(dp) ,allocatable                :: FF(:,:,:,:) 

    real(dp)  :: dum
    real(dp) ,allocatable :: xxx(:,:) ,yyy(:,:)

    type(allocatable_matrix) ,allocatable :: wevec(:) ,cevec(:)

    if(lsup .gt. lswitch) stop 'lsup must be <= lswitch'

    if(debugging) write(*,*) 'starting get_qml_estimate with lsup = ',lsup

    modes  = 0._dp
    nmodes = (lsup+1)**2 -4

    if(decouple_tp) then
       allocate(S0,source = S)
       S(1:ntemp,ntemp+1:ntot) = 0._dp
       S(ntemp+1:ntot,1:ntemp) = 0._dp
       call  dpotrf('L',ntemp,S(1:ntemp,1:ntemp),ntemp,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for TT fails'
          stop
       end if

       call  dpotrf('L',nq+nu,S(ntemp+1:ntot,ntemp+1:ntot),nq+nu,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for Pol fails'
          stop
       end if
    else
       call  dpotrf('L',ntot,S,ntot,info)
       if(info.ne.0) then
          write(*,*) 'INFO =',info
          write(*,*) 'Cholesky for TP fails'
          stop
       end if
    end if

    !2E = C^-1 dC C^-1 = C^-1 Vt dB V C^-1 = Wt dB W

    !whiten all the evecs C^-1 Vt ->wevecs
    allocate(wevec(2:lsup),cevec(2:lsup))
    do l1=2,lsup
       nl1 = 3*(2*l1+1)
       call wevec(l1)%alloc(nrow=ntot,ncol=nl1)
       call cevec(l1)%alloc(nrow=ntot,ncol=nl1)
       wevec(l1)%m = fevec(l1)%m
       call dpotrs( 'L', ntot, nl1, S, ntot, wevec(l1)%m, ntot, INFO )
       if(decouple_tp) then
          ! C tildeC^-1 V -> cevecs
          call dsymm('L','L', ntot, nl1, 1.d0, S0, ntot, wevec(l1)%m, ntot, &
               0.d0, cevec(l1)%m, ntot)
          write(0,*)'should really check this...'

       else
          ! V -> cevecs
          cevec(l1)%m = fevec(l1)%m
       end if
    end do
    if(decouple_tp)  deallocate(S0)

    S = 0._dp
    iu = 0
    do i = 1,ntot
       il = iu +1
       iu = il +(ntot-i)
       S(i:ntot,i) = N0(il:iu,1)
    end do
    call move_alloc(to=N0,from=S)
    
    allocate(vtmp(ntot,3),wtmp(ntot,3),source = 0._dp)
    allocate(block1(3,3),block2(3,3))
    allocate(auxmat3(3,ndata),atmp(3,ndata))
    allocate(yyy ,mold = transpose(auxmat3))

    !get the raw estimates xEx -tr(NE) = xWt dB Wx - tr(N Wt dB W)
    imode1 = 0
    do l1=2,lsup
       nl0 = 2*l1 +1 
       nl1 = 3*nl0

       !tr(NWt dB W) = tr(WNWt dB)
       !get (WNWt)
       !NWt ->auxmat
       allocate(auxmat,mold=wevec(l1)%m)
       call dsymm('L','L', ntot, nl1, 1.d0, N0, ntot, wevec(l1)%m, ntot, &
            0.d0, auxmat, ntot)

       !Wx -> auxmat2
       allocate(auxmat2(nl1,ndata))
       call dgemm('T','N', nl1, ndata, ntot, 1.d0, wevec(l1)%m, &
            ntot, dt, ntot, 0.d0, auxmat2, nl1)

       do im1 = 1,nl0
          imode1 = imode1 +1

          !select only the 3 columns of NWt & Wt corresponding to this (l,m)
          call collect_evec(im1,wevec(l1)%m,wtmp)
          call collect_evec(im1,auxmat,vtmp)
    
          !select only the 3 rows of Wx corresponding to this (l,m)
          !fugly, but will do for now
          xxx = transpose(auxmat2)
          call collect_evec(im1,xxx,yyy)
          auxmat3 = transpose(yyy)
            
          !WNWt -> block1
          call dgemm('T','N', 3, 3, ntot, 1.d0, wtmp, &
               ntot, vtmp, ntot, 0.d0, block1, 3)
          
          !transpose, so we can compute trace by summing over columns
          block2 = transpose(block1)

          do j1 = 1,6
             call get_block_lm(l1,im1,j1,feval(l1)%m(:,1),cblocks(l1,:)&
               ,block1)
             !tr(NE)
             do n = 1,3
                modes(imode1,j1,1) = modes(imode1,j1,1) -sum(block2(:,n)*block1(:,n))
             end do
             modes(imode1,j1,2:ndata) = modes(imode1,j1,1) 

             !dB Wx ->atmp
             call dgemm('N','N',3,ndata,3,1.d0,block1,3,auxmat3,3, &
                  0.d0,atmp,3)
             !xEx 
             do i=1,ndata
                modes(imode1,j1,i) = modes(imode1,j1,i) +sum(atmp(:,i)*auxmat3(:,i))
             end do

          end do
       end do
       deallocate(auxmat,auxmat2)
    end do
    deallocate(auxmat3,atmp)
    modes = modes*0.5_dp 

    block1 = 0._dp
    block2 = 0._dp
    vtmp   = 0._dp
    wtmp   = 0._dp
    
    !compute fisher
    !F = tr(dC C^-1 dC C^-1)/2 = tr( Vt dB W Vt dB W )/2 =
    !  = tr(dB W Vt dB W Vt)/2
    allocate(FF(nmodes,nmodes,6,6) ,source = 0._dp)
    allocate(auxmat(3,3),auxmat2(3,3),auxmat3(3,3))
    
    do j1 = 1,6
       do l1 = 2,lsup
          nl1 = 2*l1 +1

          do im1 = 1,nl1
             call lm2mode(l1,im1,imode1)
             call get_block_lm(l1,im1,j1,feval(l1)%m(:,1),cblocks(l1,:),block1)
             call collect_evec(im1,cevec(l1)%m,vtmp)
             do j2 = j1,6
                do l2 = l1,lsup
                   nl2 = 2*l2 +1

                   do im2 = 1,nl2 
                      call lm2mode(l2,im2,imode2)
                      call get_block_lm(l2 ,im2 ,j2 ,feval(l2)%m(:,1) &
                           ,cblocks(l2,:) ,block2)
                      call collect_evec(im2,wevec(l2)%m,wtmp)

                      ! VWt -> auxmat
                      call dgemm('T','N', 3, 3, ntot, 1.d0, vtmp, ntot, &
                        wtmp, ntot, 0.d0, auxmat, 3)

                      ! B_l1 auxmat ->auxmat2
                      call dgemm('N','N', 3, 3, 3, 1.d0, block1, 3, &
                           auxmat, 3, 0.d0, auxmat2, 3)

                      ! B_l2 auxmatt ->auxmat3
                      call dgemm('N','T', 3, 3, 3, 1.d0, block2, 3, &
                           auxmat, 3, 0.d0, auxmat3, 3)

                      auxmat = transpose(auxmat2)
                      do m1 = 1,3
                         FF(imode2,imode1,j2,j1) = FF(imode2,imode1,j2,j1)+ &
                              sum(auxmat(:,m1)*auxmat3(:,m1)) 
                      end do
                   end do
                end do
             end do
          end do
       end do
    end do
    deallocate(auxmat,auxmat2,auxmat3,block1,block2,vtmp,wtmp)
    do j1 = 1,6
       do j2 = j1,6
          FF(:,:,j2,j1) = FF(:,:,j2,j1)/2
          call symmetrize_matrix(FF(:,:,j2,j1))
          FF(:,:,j1,j2) = transpose(FF(:,:,j2,j1))
       end do
    end do

    nl1 = nmodes
    allocate(auxmat(6*nl1,6*nl1) ,source = 0._dp)
    do j1=1,6
       do j2=1,6
          auxmat((j2-1)*nl1 +1:j2*nl1,(j1-1)*nl1 +1:j1*nl1) = &
               FF(:,:,j2,j1)
       end do
    end do

    allocate(auxmat2(6*nl1,ndata))
    do n = 1,ndata
       do j1 = 1,6
          auxmat2((j1-1)*nl1 +1:j1*nl1,n) = modes(1:nmodes,j1,n)
       end do
    end do

    call dposv('L',6*nl1,ndata,auxmat,6*nl1,auxmat2,6*nl1,info)
    if(info .ne. 0) then
       write(*,*) 'Fisher matrix is not invertible info = ',info
       stop
    end if
    do n = 1,ndata
       do j1 = 1,6
          modes(1:nl1,j1,n) = auxmat2((j1-1)*nl1 +1:j1*nl1,n)
       end do
    end do
    
    call dpotri('L',6*nmodes,auxmat,6*nmodes,info)
    cov = auxmat
    do j1 = 1,6
       do im1 = 1,nl1
          im2 = im1 +(j1-1)*nl1
          moderr(im1,j1) = sqrt(auxmat(im2,im2))
       !symmetrize covmat
!       cov(im1,im1+1:3*nmodes) = cov(im1+1:3*nmodes,im1)

       end do
    end do

    deallocate(auxmat,auxmat2,FF)

  contains

    subroutine collect_evec(indx,vfull,v3)
      implicit none
      integer(i4b) ,intent(in)  :: indx
      real(dp)     ,intent(in)  :: vfull(:,:)
      real(dp)     ,intent(out) :: v3(:,:)

      integer(i4b)              :: nl ,i
      nl = size(vfull(1,:))/3

      do i = 1,3
         v3(:,i) = vfull(:,indx+(i-1)*nl)
      end do
      
    end subroutine collect_evec
        
    subroutine  get_block_lm(l,imode,spectra,eval,crossterm,mat)
      implicit none
      integer(i4b) ,intent(in) :: l ,imode ,spectra 
      real(dp)     ,intent(in) :: eval(:) 
      type(allocatable_matrix) ,intent(in) :: crossterm(2:)
      real(dp) ,intent(out)    :: mat(:,:)
      
      integer                  :: nl ,nl0 ,indx
      real(dp)                 :: norm
      
      norm = l*(l+1)/twopi
      
      nl0 = 3*(2*l+1)
      nl  = size(eval(:))
      if(nl .ne. nl0) then
         write(*,*) 'eigenvalues vector has size ',nl,' requested number of modes ',nl0
         stop
      end if
      
      nl  = 2*l+1
      mat = 0._dp
      
      select case (spectra)
      case (1,2,3) ! TT, EE, BB diagonal
         indx = (spectra-1)*nl +imode
         mat(spectra,spectra) = eval(indx)*norm
      case (myTE)  
         mat(2,1) = norm*crossterm(myTE)%m(nl+imode,imode)
         mat(1,2) = norm*crossterm(myTE)%m(nl+imode,imode)
      case (myTB)  
         mat(3,1) = norm*crossterm(myTB)%m(2*nl+imode,imode)
         mat(1,3) = norm*crossterm(myTB)%m(2*nl+imode,imode)
      case (myEB)  
         mat(3,2) = norm*crossterm(myEB)%m(2*nl+imode,nl+imode)
         mat(2,3) = norm*crossterm(myEB)%m(2*nl+imode,nl+imode)
      end select
       
    end subroutine get_block_lm

  end subroutine get_QMLM_estimate_tp

! ---------------------------------------------------------------------------  

  subroutine  get_QMLM_estimate_pp(lsup,modes,moderr,cov)

    implicit none
    integer(i4b) ,intent(in)  :: lsup
    real(dp)     ,intent(out) :: modes(:,:,:) ,moderr(:,:) ,cov(:,:)

    integer(i4b) :: info ,i ,n ,l1 ,l2 ,m1 ,nl0 ,nl1 ,nl2 ,imode1 ,imode2 &
         ,im1 ,im2 ,nmodes ,nmodes_tot ,imstart ,j1 ,j2 ,nl02

    integer(i8b)                         :: il ,iu
    real(dp) ,allocatable,dimension(:,:) :: cls ,S0 ,auxmat ,auxmat2 ,auxmat3
    real(dp) ,allocatable,dimension(:,:) :: block1 ,block2 ,wtmp ,vtmp ,atmp
    real(dp) ,allocatable                :: FF(:,:,:,:) 

    real(dp) ,allocatable                :: xxx(:,:) ,yyy(:,:)
    type(allocatable_matrix),allocatable :: wevec(:) ,cevec(:)

    if(lsup .gt. lswitch) stop 'lsup must be <= lswitch'

    if(debugging) write(*,*) 'starting get_qml_estimate with lsup = ',lsup

    modes  = 0._dp
    nmodes = (lsup+1)**2 -4

    call  dpotrf('L',ntot,S,ntot,info)
    if(info.ne.0) then
       write(*,*) 'INFO =',info
       write(*,*) 'Cholesky for P-only fails'
       stop
    end if

    !2E = C^-1 dC C^-1 = C^-1 Vt dB V C^-1 = Wt dB W

    !whiten all the evecs C^-1 Vt
    allocate(wevec(2:lsup),cevec(2:lsup))
    do l1=2,lsup
       nl1 = 2*(2*l1+1)
       call wevec(l1)%alloc(nrow=ntot,ncol=nl1)
       call cevec(l1)%alloc(nrow=ntot,ncol=nl1)
       wevec(l1)%m = fevec(l1)%m
       call dpotrs( 'L', ntot, nl1, S, ntot, wevec(l1)%m, ntot, INFO )
       cevec(l1)%m = fevec(l1)%m
    end do

    S = 0._dp
    iu = 0
    do i = 1,ntot
       il = iu +1
       iu = il +(ntot-i)
       S(i:ntot,i) = N0(il:iu,1)
    end do
    call move_alloc(to=N0,from=S)
    
    allocate(vtmp(ntot,2),wtmp(ntot,2),source = 0._dp)
    allocate(block1(2,2),block2(2,2))

    allocate(auxmat3(2,ndata),atmp(2,ndata))
    allocate(yyy ,mold = transpose(auxmat3))

    !get the raw estimates xEx -tr(NE)
    imode1 = 0
    do l1=2,lsup
       nl0 = 2*l1 +1 
       nl1 = 2*nl0

       !tr(NWt dB W) = tr(WNWt dB)
       !get (WNWt)
       !NWt -> auxmat
       allocate(auxmat,mold=wevec(l1)%m)
       call dsymm('L','L', ntot, nl1, 1.d0, N0, & 
            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)

       block1 = 0._dp
       allocate(auxmat2(nl1,ndata))
       !Wx -> auxmat2
       call dgemm('T','N', nl1, ndata, ntot, 1.d0, wevec(l1)%m, &
            ntot, dt, ntot, 0.d0, auxmat2, nl1)

       do im1 = 1,nl0
          imode1 = imode1 +1

          !select only the 2 columns of NWt & Wt for this (l,m)
          call collect_evec_p(im1,wevec(l1)%m,wtmp)
          call collect_evec_p(im1,auxmat,vtmp)

          !select only the 2 rows of Wx for this (l,m)
          !fugly, will do for now
          xxx = transpose(auxmat2)
          call collect_evec_p(im1,xxx,yyy)
          auxmat3 = transpose(yyy)

          !WNWt -> block1
          call dgemm('T','N', 2, 2, ntot, 1.d0, wtmp, &
               ntot, vtmp, ntot, 0.d0, block1, 2)
          
          !transpose, so we can compute trace by summing over columns
          block2 = transpose(block1)

          do j1 = 1,3
             call get_block_lm_p(l1,im1,j1,feval(l1)%m(:,1),cblocks(l1,:)&
               ,block1)

             !tr(NE)
             do n = 1,2
                modes(imode1,j1,1) = modes(imode1,j1,1) - sum(block2(:,n)*block1(:,n))
             end do
             modes(imode1,j1,2:ndata) = modes(imode1,j1,1) 

             !xEx 
             call dgemm('N','N',2,ndata,2,1.d0,block1,2,auxmat3,2, &
                  0.d0,atmp,2)
             
             do i=1,ndata
                modes(imode1,j1,i) = modes(imode1,j1,i) +sum(atmp(:,i)*auxmat3(:,i))
             end do
          end do
       end do
       deallocate(auxmat,auxmat2)
    end do
    deallocate(auxmat3,atmp)
    modes = modes*0.5_dp 

    block1 = 0._dp
    block2 = 0._dp
    vtmp   = 0._dp
    wtmp   = 0._dp

    !compute fisher
    !F = tr(dC C^-1 dC C^-1)/2 = tr( Vt dB W Vt dB W )/2 =
    !  = tr(dB W Vt dB W Vt)/2
    allocate(FF(nmodes,nmodes,3,3) ,source = 0._dp)
    allocate(auxmat(2,2),auxmat2(2,2),auxmat3(2,2))
    
    do j1 = 1,3
       do l1 = 2,lsup
          nl1 = 2*l1 +1

          do im1 = 1,nl1
             call lm2mode(l1,im1,imode1)
             call get_block_lm_p(l1,im1,j1,feval(l1)%m(:,1),cblocks(l1,:),block1)
             call collect_evec_p(im1,cevec(l1)%m,vtmp)
             do j2 = j1,3
                do l2 = l1,lsup
                   nl2 = 2*l2 +1

                   do im2 = 1,nl2 
                      call lm2mode(l2,im2,imode2)
                      call get_block_lm_p(l2 ,im2 ,j2 ,feval(l2)%m(:,1) &
                           ,cblocks(l2,:) ,block2)
                      call collect_evec_p(im2,wevec(l2)%m,wtmp)

                      ! VWt -> auxmat
                      call dgemm('T','N', 2, 2, ntot, 1.d0, vtmp, ntot, &
                        wtmp, ntot, 0.d0, auxmat, 2)

                      ! B_l1 auxmat ->auxmat2
                      call dgemm('N','N', 2, 2, 2, 1.d0, block1, 2, &
                           auxmat, 2, 0.d0, auxmat2, 2)

                      ! B_l2 auxmatt ->auxmat3
                      call dgemm('N','T', 2, 2, 2, 1.d0, block2, 2, &
                           auxmat, 2, 0.d0, auxmat3, 2)

                      auxmat = transpose(auxmat2)
                      do m1 = 1,nl1
                         FF(imode2,imode1,j2,j1) = FF(imode2,imode1,j2,j1)+ &
                              sum(auxmat(:,m1)*auxmat3(:,m1)) 
                      end do
                   end do
                end do
             end do
          end do
       end do
    end do
    deallocate(auxmat,auxmat2,auxmat3,block1,block2,vtmp,wtmp)
    do j1 = 1,3
       do j2 = j1,3
          FF(:,:,j2,j1) = FF(:,:,j2,j1)/2
          call symmetrize_matrix(FF(:,:,j2,j1))
          FF(:,:,j1,j2) = transpose(FF(:,:,j2,j1))
       end do
    end do

    nl1 = nmodes
    allocate(auxmat(3*nl1,3*nl1) ,source = 0._dp)
    do j1=1,3
       do j2=1,3
          auxmat((j2-1)*nl1 +1:j2*nl1,(j1-1)*nl1 +1:j1*nl1) = &
               FF(:,:,j2,j1)
       end do
    end do

    allocate(auxmat2(3*nl1,ndata))
    do n = 1,ndata
       do j1 = 1,3
          auxmat2((j1-1)*nl1 +1:j1*nl1,n) = modes(1:nmodes,j1,n)
       end do
    end do

    call dposv('L',3*nl1,ndata,auxmat,3*nl1,auxmat2,3*nl1,info)
    if(info .ne. 0) then
       write(*,*) 'Fisher matrix is not invertible info = ',info
       stop
    end if
    do n = 1,ndata
       do j1 = 1,3
          modes(1:nl1,j1,n) = auxmat2((j1-1)*nl1 +1:j1*nl1,n)
       end do
    end do
    
    call dpotri('L',3*nmodes,auxmat,3*nmodes,info)
    cov = auxmat
    do j1 = 1,3
       do im1 = 1,nl1
          im2 = im1 +(j1-1)*nl1
          moderr(im1,j1) = sqrt(auxmat(im2,im2))
       !symmetrize covmat
!       cov(im1,im1+1:3*nmodes) = cov(im1+1:3*nmodes,im1)

       end do
    end do

    deallocate(auxmat,auxmat2,FF)

  contains

    subroutine collect_evec_p(indx,vfull,v2)
      implicit none
      integer(i4b) ,intent(in)  :: indx
      real(dp)     ,intent(in)  :: vfull(:,:)
      real(dp)     ,intent(out) :: v2(:,:)

      integer(i4b)              :: nl ,i
      nl = size(vfull(1,:))/2

      do i = 1,2
         v2(:,i) = vfull(:,indx+(i-1)*nl)
      end do
      
    end subroutine collect_evec_p
        
    subroutine  get_block_lm_p(l,imode,spectra,eval,crossterm,mat)
      implicit none
      integer(i4b) ,intent(in) :: l ,imode ,spectra 
      real(dp)     ,intent(in) :: eval(:) 
      type(allocatable_matrix) ,intent(in) :: crossterm(2:)
      real(dp) ,intent(out)    :: mat(:,:)
      
      integer                  :: nl ,nl0 ,indx
      real(dp)                 :: norm
      
      norm = l*(l+1)/twopi
      
      nl0 = 2*(2*l+1)
      nl  = size(eval(:))
      if(nl .ne. nl0) then
         write(*,*) 'eigenvalues vector has size ',nl,' requested number of modes ',nl0
         stop
      end if
      
      nl  = 2*l+1
      mat = 0._dp
      
      select case (spectra)
      case (1,2) ! EE, BB diagonal
         indx = (spectra-1)*nl +imode
         mat(spectra,spectra) = eval(indx)*norm
      case (3)  
         mat(2,1) = norm*crossterm(myEB)%m(nl+imode,imode)
         mat(1,2) = norm*crossterm(myEB)%m(nl+imode,imode)
      end select
       
    end subroutine get_block_lm_p

  end subroutine get_QMLM_estimate_pp

! ---------------------------------------------------------------------------  
 
  subroutine lm2mode(l,m,i)
    implicit none
    integer(i4b) ,intent(in)  :: l ,m
    integer(i4b) ,intent(out) :: i
    
    i = l**2 +m -4
    
  end subroutine lm2mode

! ---------------------------------------------------------------------------  
    
  subroutine mode2lm(i,l,m)
    implicit none
    integer(i4b) ,intent(in)  :: i
    integer(i4b) ,intent(out) :: l ,m
    
    l = int(sqrt(real(i+4+tiny(1._dp),dp)),i4b)
    m = i +4 -l**2
    
  end subroutine mode2lm

! ---------------------------------------------------------------------------  

  subroutine  get_QMLM_estimate_tt(lsup,modes,moderr,cov)

    implicit none
    integer(i4b) ,intent(in)  :: lsup
    real(dp)     ,intent(out) :: modes(:,:) ,moderr(:) ,cov(:,:)

    integer(i4b) :: info ,i ,n ,l1 ,l2 ,m1 ,nl0 ,nl1 ,nl2 ,imode1 ,imode2 &
         ,im1 ,im2 ,nmodes ,nmodes_tot ,imstart ,j1 ,j2 ,nl02

    integer(i8b)                         :: il ,iu
    real(dp) ,allocatable,dimension(:,:) :: cls ,S0 ,auxmat ,auxmat2 ,auxmat3
    real(dp) ,allocatable,dimension(:,:) :: block1 ,block2
    real(dp) ,allocatable                :: FF(:,:) 

    type(allocatable_matrix) ,allocatable :: wevec(:) ,cevec(:)

    double precision ddot
    external ddot 
    
    if(lsup .gt. lswitch) stop 'lsup must be <= lswitch'

    if(debugging) write(*,*) 'starting get_qml_estimate with lsup = ',lsup

    modes  = 0._dp
    nmodes = (lsup+1)**2 -4

    call  dpotrf('L',ntemp,S(1:ntemp,1:ntemp),ntemp,info)
    if(info.ne.0) then
       write(*,*) 'INFO =',info
       write(*,*) 'Cholesky for TT fails'
       stop
    end if

    !2E = C^-1 dC C^-1 = C^-1 Vt dB V C^-1 = Wt dB W

    !whiten all the evecs C^-1 Vt
    allocate(wevec(2:lsup),cevec(2:lsup))
    do l1=2,lsup
       nl1 = 2*l1+1
       call wevec(l1)%alloc(nrow=ntot,ncol=nl1)
       call cevec(l1)%alloc(nrow=ntot,ncol=nl1)
       wevec(l1)%m = fevec(l1)%m
       call dpotrs( 'L', ntot, nl1, S, ntot, wevec(l1)%m, ntot, INFO )
       cevec(l1)%m = fevec(l1)%m
    end do

    S = 0._dp
    iu = 0
    do i = 1,ntot
       il = iu +1
       iu = il +(ntot-i)
       S(i:ntot,i) = N0(il:iu,1)
    end do
    call move_alloc(to=N0,from=S)
    
    !get the raw estimates xEx -tr(NE)
    imode1 = 0
    do l1=2,lsup
       nl1 = 2*l1 +1 

       !tr(NWt dB W) = tr(WNWt dB)
       !get (WNWt)
       !NWt
       allocate(auxmat,mold=wevec(l1)%m)
       call dsymm('L','L', ntot, nl1, 1.d0, N0, & 
            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)
!       call dgemm('N','N', ntot, nl1, ntot, 1.d0, N0, &
!            ntot, wevec(l1)%m, ntot, 0.d0, auxmat, ntot)

       allocate(block1(nl1,1))
       
       !WNWt
       do im1 = 1,nl1
          block1(im1,1) = ddot(ntot,wevec(l1)%m(:,im1),1,auxmat(:,im1),1) 
       end do
!       call dgemm('T','N', nl1, nl1, ntot, 1.d0, wevec(l1)%m, &
!            ntot, auxmat, ntot, 0.d0, block1, nl1)
       deallocate(auxmat)
       
       allocate(auxmat(nl1,ndata),auxmat2(1,ndata))

       !Wx
       call dgemm('T','N', nl1, ndata, ntot, 1.d0, wevec(l1)%m, &
            ntot, dt, ntot, 0.d0, auxmat, nl1)

       allocate(block2, source= block1)

       do im1 = 1,nl1
          imode1 = imode1 +1

          block1 = 0._dp
          block1(im1,1) = feval(l1)%m(im1,1)*l1*(l1+1._dp)/twopi
!          call cls2block_modes(l1,im1,1,feval(l1)%m(:,1),cblocks(l1,:)&
!               ,block1)

          !tr(NE)
!          do n = 1,nl1
!             modes(imode1,1) = modes(imode1,1) - sum(block2(:,n)*block1(:,n))
!          end do

          modes(imode1,1) = modes(imode1,1) - block1(im1,1)*block2(im1,1)
          modes(imode1,2:ndata) = modes(imode1,1) 

          !xEx 
          call dgemm('T','N',1,ndata,nl1,1.d0,block1,nl1,auxmat,nl1, &
               0.d0,auxmat2,1)
             
          do i=1,ndata
             modes(imode1,i) = modes(imode1,i) +auxmat(im1,i)*auxmat2(1,i)
          end do
       end do
       deallocate(block1,block2,auxmat,auxmat2)
    end do
    modes = modes*0.5_dp 

    !compute fisher
    !F = tr(dC C^-1 dC C^-1)/2 = tr( Vt dB W Vt dB W )/2 =
    !  = tr(dB W Vt dB W Vt)/2
    allocate(FF(nmodes,nmodes) ,source = 0._dp)

    do l1 = 2,lsup
       nl1 = 2*l1 +1
!       allocate(block1(nl1,nl1) ,source = 0._dp)
       allocate(block1(nl1,1) ,source = 0._dp)
       block1(:,1) = feval(l1)%m(:,1)*l1*(l1+1.d0)/twopi 

       do im1 = 1,nl1
          call lm2mode(l1,im1,imode1)
          
!          call cls2block_modes(l1,im1,j1,feval(l1)%m(:,1),cblocks(l1,:),block1)
!          block1(im1,1) = feval(l1)%m(im1,1)*l1*(l1+1.d0)/twopi 

          do l2 = l1,lsup
             nl2 = 2*l2 +1
             allocate(block2(nl2,1) ,source = 0._dp)
             block2(:,1) = feval(l2)%m(:,1)*l2*(l2+1.d0)/twopi 
             allocate(auxmat(nl1,nl2) ,auxmat2(1,nl2) &
                  ,auxmat3(1,nl1) ,source = 0._dp)

             ! W Vt = V C^-1 Vt = V Wt
             ! (W_1 Vt_2)t = (V_1 Wt_2)t = W_2 Vt_1
             call dgemm('T','N', nl1, nl2, ntot, 1.d0, cevec(l1)%m, ntot, &
                  wevec(l2)%m, ntot, 0.d0, auxmat, nl1)
             
             call dgemm('T','N', 1, nl2, nl1, 1.d0, block1, nl1, auxmat,&
                  nl1, 0.d0, auxmat2, 1)
             
             call dgemm('T','T', 1, nl1, nl2, 1.d0, block2, nl2, &
                  auxmat, nl1, 0.d0, auxmat3, 1)

             do im2 = 1,nl2
                call lm2mode(l2,im2,imode2)
                FF(imode2,imode1) = FF(imode2,imode1) &
                     +auxmat2(1,im2)*auxmat3(1,m1) 
             end do
             deallocate(auxmat,auxmat2,auxmat3,block2)
          end do
       end do
       deallocate(block1)
    end do

    FF(:,:) = FF(:,:)/2
    call symmetrize_matrix(FF(:,:))

    nl1 = nmodes

    call dposv('L',nl1,ndata,FF,nl1,modes,nl1,info)
    if(info .ne. 0) then
       write(*,*) 'Fisher matrix is not invertible info = ',info
       stop
    end if
    
    call dpotri('L',nmodes,FF,nmodes,info)
    cov = FF
    do im1 = 1,nl1
       moderr(im1) = sqrt(auxmat(im1,im1))
    end do

    deallocate(auxmat,auxmat2,FF)

  end subroutine get_QMLM_estimate_tt

! ---------------------------------------------------------------------------  

  subroutine  cls2block(l,cls,eval,crossterm,mat)
    implicit none
    integer(i4b) ,intent(in) :: l
    real(dp) ,intent(in)     :: cls(2:,1:) ,eval(:) 
    type(allocatable_matrix) ,intent(in) :: crossterm(2:)
    real(dp) ,intent(out)    :: mat(:,:)

    integer                  :: nl ,nl0 ,i 

    nl0 = 3*(2*l+1)
    nl = size(eval(:))
    if(nl .ne. nl0) then
       write(*,*) 'eigenvalues vector has size ',nl,' requested number of modes',nl0
       stop
    end if

    nl = size(mat(:,1))
    if(nl .ne. nl0) then
       write(*,*) 'total matrix has size ',nl,' requested number of modes ',nl0
       stop
    end if

    nl  = 2*l+1
    mat = 0._dp

    do i=1,nl
       mat(i,i) = eval(i)*cls(l,myTT)
    end do

    do i =2,6
       mat = mat +cls(l,i)*crossterm(i)%m
    end do

  end subroutine cls2block

! ---------------------------------------------------------------------------
 
  subroutine  cls2block_p(l,cls,eval,crossterm,mat)
    implicit none
    integer(i4b) ,intent(in) :: l
    real(dp) ,intent(in)     :: cls(2:,1:) ,eval(:) 
    type(allocatable_matrix) ,intent(in) :: crossterm(2:)
    real(dp) ,intent(out)    :: mat(:,:)

    integer                  :: nl ,nl0 

    nl0 = 2*(2*l+1)
    nl = size(eval(:))
    if(nl .ne. nl0) then
       write(*,*) 'eigenvalues vector has size ',nl,' requested number of modes ',nl0
       stop
    end if

    nl = size(mat(:,1))
    if(nl .ne. nl0) then
       write(*,*) 'total matrix has size ',nl,' requested number of modes ',nl0
       stop
    end if

    nl = 2*l+1

    mat = 0._dp
    mat = mat +cls(l,1)*crossterm(myEE)%m
    mat = mat +cls(l,2)*crossterm(myBB)%m
    mat = mat +cls(l,3)*crossterm(myEB)%m

  end subroutine cls2block_p

! ---------------------------------------------------------------------------

  subroutine precompute_rotation_angle(s1,c1,s2,c2)

    implicit none
    real(dp) ,intent(out) :: s1(:,:) ,c1(:,:) ,s2(:,:) ,c2(:,:)  

    integer(i4b)          :: i ,j ,iq ,jq ,iu ,ju
    real(dp)              :: a1 ,a2
       
! QT
!$OMP PARALLEL DEFAULT (PRIVATE) SHARED(c1,c2,s1,s2,mask,ntemp,nq,nu,ntot)
!$OMP DO
    do i=1,ntemp
       do j=1,nq
          call get_rotation_angle(mask%QVec(:,j),mask%TVec(:,i),a1,a2)
          jq = j +ntemp
          c1(jq,i) = cos(a1)
          s1(jq,i) = sin(a1)
          c2(jq,i) = cos(a2)
          s2(jq,i) = sin(a2)

       enddo
!UT
       do j=1,nu
          call get_rotation_angle(mask%UVec(:,j),mask%TVec(:,i),a1,a2)
          ju = j +ntemp +nq
          c1(ju,i) = cos(a1)
          s1(ju,i) = sin(a1)
          c2(ju,i) = cos(a2)
          s2(ju,i) = sin(a2)
       end do
    end do

!$OMP DO
    do i = 1,nq
!TQ       
       iq = i+ntemp
       do j=1,ntemp
          call get_rotation_angle(mask%TVec(:,j),mask%QVec(:,i),a1,a2)
          c1(j,iq) = cos(a1)
          s1(j,iq) = sin(a1)
          c2(j,iq) = cos(a2)
          s2(j,iq) = sin(a2)
       end do
!QQ
       do j = 1,nq
          call get_rotation_angle(mask%QVec(:,j),mask%QVec(:,i),a1,a2)
          jq = j+ntemp

          c1(jq,iq) = cos(a1)
          s1(jq,iq) = sin(a1)
          c2(jq,iq) = cos(a2)
          s2(jq,iq) = sin(a2)

       end do
!UQ
       do j=1,nu
          call get_rotation_angle(mask%UVec(:,j),mask%QVec(:,i),a1,a2)

          ju = j+ntemp+nq

          c1(ju,iq) = cos(a1)
          s1(ju,iq) = sin(a1)
          c2(ju,iq) = cos(a2)
          s2(ju,iq) = sin(a2)

       end do
    end do

!$OMP DO
    do i =1,nu
!TU
       iu = i+ntemp+nq
       do j=1,ntemp
          call get_rotation_angle(mask%TVec(:,j),mask%UVec(:,i),a1,a2)
          c1(j,iu) = cos(a1)
          s1(j,iu) = sin(a1)
          c2(j,iu) = cos(a2)
          s2(j,iu) = sin(a2)
       end do
!QU
       do j = 1,nq
          jq = j+ntemp
          call get_rotation_angle(mask%QVec(:,j),mask%UVec(:,i),a1,a2)

          c1(jq,iu) = cos(a1)
          s1(jq,iu) = sin(a1)
          c2(jq,iu) = cos(a2)
          s2(jq,iu) = sin(a2)

       end do
!UU
       do j=i,nu

          call get_rotation_angle(mask%UVec(:,j),mask%UVec(:,i),a1,a2)

          ju = j+ntemp+nq

          c1(ju,iu) = cos(a1)
          s1(ju,iu) = sin(a1)
          c2(ju,iu) = cos(a2)
          s2(ju,iu) = sin(a2)

       end do
    end do
!$OMP END PARALLEL
    
  end subroutine precompute_rotation_angle

end module bflike_smw_mod



