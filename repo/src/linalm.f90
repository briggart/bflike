program linalm

  use healpix_types   ,only : dp ,sp ,i4b ,i8b 
  use healpix_modules ,only : add_card, write_asctab, write_minimal_header &
       ,write_bintabh ,output_map
  use paramfile_io
  use lpl_utils_mod   ,only : maxlen
  use bflike_smw_mod
  implicit none

  integer(i4b),parameter :: nlheader = 256
  real(dp)  ,allocatable :: alm(:,:,:) ,cl(:,:,:) ,map(:,:,:) ,ncm(:,:)
  integer(i4b)           :: ncmd ,j ,ndata ,lent ,nside ,lmax ,lswitch ,i ,nmodes
  character(maxlen)      :: oroot ,parfile
  character(5)           :: tag
  type(paramfile_handle) :: handle
  logical                :: get_cls ,get_alms ,get_maps

  character(len=80)      :: almhdr(nlheader) ,clhdr(nlheader) &
       ,maphdr(nlheader)
  
  ncmd = command_argument_count()
  if(ncmd .ne. 1) then
     write(*,*) 'CRITICAL ERROR : usage linalm [params.ini]'
     stop
  else
     call get_command_argument(1,parfile)
  end if

  handle   = parse_init(parfile)
  oroot    = parse_string(handle,'output_root')
  nside    = parse_int(handle,'nside')
  get_cls  = parse_lgt(handle,'get_cls' ,.false.)
  get_alms = parse_lgt(handle,'get_alms',.false.)
  get_maps = parse_lgt(handle,'get_maps',.false.)

  call parse_finish(handle)

  if (count([get_cls ,get_alms ,get_maps]) .eq. 0) &
       stop 'no output requested, abort.'

  lent = len_trim(oroot)
  if(lent .gt. 0) then
     if (.not. oroot(lent:lent) .eq. '_') then
        oroot(lent+1:lent+1) = '_'
        lent = lent+1
     end if
     if(.not. oroot(1:1) .eq. '!') then
        do i=lent+1,2,-1
           oroot(i:i) = oroot(i-1:i-1)
        end do
        oroot(1:1) = '!'
     end if
  end if
  
  call init_pix_like(parfile,lmax,lswitch,ndata)
  call get_linalm(alm,cl=cl,map=map,ncm=ncm)

  call write_minimal_header(clhdr,'CL',bcross=.true.,polar=.true.)
  call write_minimal_header(maphdr,'MAP',nside=nside,polar=.true.,ordering='NESTED')
  call add_card(maphdr,'TTYPE4','E_POLARISATION')
  call add_card(maphdr,'TTYPE5','B_POLARISATION')
  call write_minimal_header(almhdr,'ALM')
  call add_card(almhdr,'TTYPE2','E       ')
  call add_card(almhdr,'TTYPE3','B       ')
  
  do j = 1,ndata
     write(tag,'(i5.5)') j
     if(get_maps) call output_map(map(:,:,j),maphdr,trim(oroot)//tag//'_map.fits')
     if(get_cls)  call write_asctab(cl(:,:,j),lswitch,6,clhdr,nlheader,trim(oroot)//tag//'_cl.fits')
     if(get_alms) call write_bintabh(alm(:,:,j),int((lswitch+1)**2 -4,i8b),3,almhdr,nlheader,trim(oroot)//tag//'_alm.fits')
  end do

  almhdr = ''
  
!  call write_minimal_header(almhdr,'ALM')
  nmodes = size(ncm(:,1))

  print*,shape(ncm)
  print*,shape(reshape(ncm,(/nmodes*nmodes,1/)))
  if(get_alms) then
!     call write_bintabh(ncm,int(nmodes,i8b),nmodes,almhdr,nlheader,trim(oroot)//tag//'_alm_cov.fits')
     call write_bintabh(reshape(ncm,(/nmodes*nmodes,1/)),int(nmodes*nmodes,i8b),1,almhdr,nlheader,trim(oroot)//'alm_cov.fits')
  end if

  
end program linalm
