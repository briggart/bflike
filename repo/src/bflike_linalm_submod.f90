submodule(bflike_smw_mod) bflike_linalm_submod

contains

  module subroutine modes2matrix(eval, evec, mat)
    implicit none
    type(allocatable_matrix) ,intent(in)  :: evec(2:) ,eval(2:)
    real(dp)                 ,intent(out) :: mat(:,:)
    
    integer  :: ntot ,nmodes ,l ,nl ,nstokes ,istart ,istop ,i ,off
    
    ntot   = size(mat(:,1))
    nmodes = size(mat(1,:))
    
    lmax   = size(evec(:)) +1
    
    istop = 0

    !first all T modes
    if(mask%ntemp .gt. 0) then
       off = 1
       do l = 2,lmax
          nl = 2*l+1
          istart = istop +1
          istop  = istop +nl
          mat(:,istart:istop) = evec(l)%m(:,1:nl)
          do i = 1,nl
             mat(:,i +istart -1) = mat(:,i +istart -1)*sqrt(eval(l)%m(i,1))
          end do
       end do
    else
       off = 0
    end if

    !then all P modes    
    if(mask%nqu .gt. 0) then
       !E 
       do l = 2,lmax
          nl = 2*l+1
          istart = istop +1
          istop  = istop +nl
          mat(:,istart:istop) = evec(l)%m(:,1+nl*off:(off+1)*nl)
          do i = 1,nl
             mat(:,i +istart -1) = mat(:,i +istart -1)*sqrt(eval(l)%m(off*nl +i,1))
          end do
       end do
       !B
       do l = 2,lmax
          nl = 2*l+1
          istart = istop +1
          istop  = istop +nl
          mat(:,istart:istop) = evec(l)%m(:,1+nl*(off+1):(off+2)*nl)
          do i = 1,nl
             mat(:,i +istart -1) = mat(:,i +istart -1)*sqrt(eval(l)%m((off+1)*nl +i,1))
          end do
       end do
    end if

  end subroutine
      
  module subroutine get_linalm(linalm,cl,map,ncm)
    implicit none
    real(dp)           ,allocatable ,intent(out) :: linalm(:,:,:)
    real(dp) ,optional ,allocatable ,intent(out) :: cl(:,:,:) &
         ,map(:,:,:) ! T, E, B maps, not I, Q, U
    real(dp) ,optional ,allocatable ,intent(out) :: ncm(:,:)
    
    real(dp) ,allocatable           :: yt(:,:) 
    logical                         :: get_maps = .false. ,get_cls = .false.
    integer(i4b)                    :: info ,nmodes ,i ,j

    if (present(cl))  get_cls  = .true.
    if (present(map)) get_maps = .true.

    nmodes = size(Ylm(1,:))

    call dpotrf('L',ntot,ncvm,ntot,info)

    if (info .ne. 0) stop 'input NCVM is not invertible'
       
    ! N^-1 d
    call dpotrs('L',ntot,ndata,ncvm,ntot,dt,ntot,info)

    ! Y N^-1 m
    yt = transpose(Ylm)
    allocate(auxdt(nmodes,ndata) ,source = 0.d0)
    call dgemm('N','N',nmodes,ndata,ntot,1.d0,Yt,nmodes,dt,ntot,0.d0,&
         auxdt ,nmodes)
    deallocate(dt)

    ! N^-1 Y
    call dpotrs('L',ntot,nmodes,ncvm,ntot,Ylm,ntot,info)
    deallocate(ncvm) ;allocate(ncvm(nmodes,nmodes) ,source = 0.d0)

    ! Y N^-1 Y
    call dgemm('N','N',nmodes,nmodes,ntot,1.d0,yt,nmodes,Ylm,ntot,0.d0,&
         ncvm,nmodes)
    deallocate(Ylm)
    
    call dpotrf('L',nmodes,ncvm,nmodes,info)
    if (info .ne. 0) stop 'modes NCVM is not invertible'
    
    ! (Y N^-1 Y)^-1 Y N^-1 m
    call dpotrs('L',nmodes,ndata,ncvm,nmodes,auxdt,nmodes,info)

    ! (Y N^-1 Y)^-1
    call dpotri('L',nmodes,ncvm,nmodes,info)
    do i = 1,nmodes
       do j = i+1,nmodes
          ncvm(i,j) = ncvm(j,i)
       end do
    end do
    
    call modes2output()
    deallocate(yt ,auxdt)

    if (present(ncm)) then
       call move_alloc(from=ncvm,to=ncm)
    end if
    
  contains

    subroutine modes2output()

      implicit none
      real(dp) ,allocatable :: tmp(:) ,tmp2(:,:)
      integer(i4b)          :: nl ,j ,istart ,istop ,nsingle ,i ,l
      
      nsingle = nmodes/nsp
      
      if (get_cls)  allocate(cl(0:lmax,6,ndata)  ,source = 0._dp)

      allocate(linalm(nsingle,3,ndata) ,source = 0._dp)

      if (has_t) then
         linalm(:,1,:) = auxdt(1:nsingle,:)
      end if

      if (has_p) then
         linalm(:,2,:)     = &
              auxdt(off*nsingle+1:(off+1)*nsingle,:)
         linalm(:,3,:) = &
              auxdt((off+1)*nsingle+1:(off+2)*nsingle,:)
      end if

      do j = 1,ndata
         istop = 0

         if (get_cls) then
            do l = 2,lmax
               nl  = 2*l +1
               istart = istop +1
               istop  = istop +nl
               cl(l,myTT,j) = sum(linalm(istart:istop,1,j)**2)/nl
               cl(l,myEE,j) = sum(linalm(istart:istop,2,j)**2)/nl
               cl(l,myBB,j) = sum(linalm(istart:istop,3,j)**2)/nl
               cl(l,myTE,j) = sum(linalm(istart:istop,1,j)*linalm(istart:istop,2,j))/nl
               cl(l,myTB,j) = sum(linalm(istart:istop,1,j)*linalm(istart:istop,3,j))/nl
               cl(l,myEB,j) = sum(linalm(istart:istop,2,j)*linalm(istart:istop,3,j))/nl
            end do
         end if
      end do
      
      if (get_maps) then

         call modes2map(auxdt,ylm_full,map)
!         call modes2maps(auxdt,transpose(Yt),map) 
         
!         allocate(map(0:mask%s%npix-1,5,ndata)) ! I,Q,U,E,B 
!         allocate(tmp2(ntot,ndata)) 
!         call dgemm('T','N',ntot,ndata,nmodes,1.d0,Yt,nmodes, auxdt &
!              ,nmodes,0.d0,tmp2 ,ntot)

!         do j = 1,ndata
!            tmp = tmp2(:,j)
!            call unmask_vec(mask%lmask,tmp)
!            map(:,1,j) = tmp(1:mask%s%npix)
!            if(has_p) then
!               map(:,2,j) = tmp(mask%s%npix+1:2*mask%s%npix)
!               map(:,3,j) = tmp(2*mask%s%npix+1:3*mask%s%npix)
!            end if
!         end do

!         if (has_p) then
!            !E
!            call dgemm('T','N',ntot,ndata,nsingle,1.d0,Yt(1:nsingle,1:ntot),&
!                 nsingle, auxdt(nsingle+1:2*nsingle,1:ndata),nsingle,0.d0,&
!                 tmp2,ntot)
!            do j = 1,ndata
!               tmp = tmp2(:,j)
!               call unmask_vec(mask%lmask,tmp)
!               map(:,4,j) = tmp(1:mask%s%npix)
!            end do

!            !B
!            call dgemm('T','N',ntot,ndata,nsingle,1.d0,Yt(1:nsingle,1:ntot),&
!                 nsingle,auxdt(2*nsingle+1:3*nsingle,1:ndata),nsingle,0.d0,&
!                 tmp2 ,ntot)
!            do j = 1,ndata
!               tmp = tmp2(:,j)
!               call unmask_vec(mask%lmask,tmp)
!               map(:,5,j) = tmp(1:mask%s%npix)
!            end do 
!         end if
      end if

    end subroutine modes2output    
      
  end subroutine

  subroutine modes2map(modes,y,maps)
    !only I,Q,U maps for now.
    real(dp)               ,intent(in) :: modes(:,:) ,y(:,:)
    real(dp) ,allocatable ,intent(out) :: maps(:,:,:)
    
    real(dp) ,allocatable :: tmp(:,:)
    integer(i4b)  :: nmodes ,ndata ,ntot ,j ,nsingle

    nmodes = size(modes(:,1))
    ndata  = size(modes(1,:))
    ntot   = size(y(:,1))

    nsingle = nmodes/nsp
    
    allocate(maps(0:mask%s%npix-1,5,ndata) ,source = hpx_dbadval) ! I,Q,U,E,B 
    allocate(tmp(ntot,ndata)) 
    call dgemm('N','N',ntot,ndata,nmodes,1.d0,Y,ntot, auxdt &
              ,nmodes,0.d0,tmp ,ntot)
    
    do j = 1,ndata
       maps(:,1,j) = tmp(1:mask%s%npix,j)
       if(has_p) then
          maps(:,2,j) = tmp(mask%s%npix+1:2*mask%s%npix,j)
          maps(:,3,j) = tmp(2*mask%s%npix+1:3*mask%s%npix,j)
       end if
    end do

    if (has_p) then
       !project E & B modes on scalar Y_lm to get E & B maps 
       
       !E
       call dgemm('N','N',ntot,ndata,nsingle,1.d0,Y(:,1:nsingle),&
            ntot, modes(nsingle+1:2*nsingle,1:ndata),nsingle,0.d0,&
            tmp,ntot)
       do j = 1,ndata
          maps(:,4,j) = tmp(1:mask%s%npix,j)
       end do

       !B
       call dgemm('N','N',ntot,ndata,nsingle,1.d0,Y(:,1:nsingle),&
            ntot,auxdt(2*nsingle+1:3*nsingle,1:ndata),nsingle,0.d0,&
            tmp ,ntot)
       do j = 1,ndata
          maps(:,5,j) = tmp(1:mask%s%npix,j)
       end do
    end if

  end subroutine modes2map

  
end submodule bflike_linalm_submod

  
