program shrink_ncvm

  use healpix_modules ,only : i8b ,i4b ,dp 
  use paramfile_io
  use lpl_utils_mod   ,only : symmetrize_matrix ,map2vec ,sky_mask ,maxlen ,read_map

  implicit none
  
  integer(i4b) ,parameter :: nested = 2
  integer(i4b)            :: nsims ,i ,j ,n ,ntot ,ncmd ,nside ,rcl ,unit ,nstokes ,nfull
  integer(i8b)           :: npix 
  real(dp) ,allocatable  :: T(:,:) ,S(:,:) ,W(:,:), X(:,:) ,map(:,:)
  real(dp)               :: lambda ,ncf ,mcf
  logical ,allocatable   :: sqmask(:,:) 
  character(maxlen)      :: ncvmfile ,parfile ,mapfile ,maskfile ,outroot ,filename
  type(paramfile_handle) :: handle
  type(sky_mask)         :: mask 

  ncmd = command_argument_count()
  if(ncmd .ne. 1) then
     write(*,*) 'CRITICAL ERROR : usage shrink_ncvm params.ini'
     stop
  else
     call get_command_argument(1,parfile)
  end if

  handle   = parse_init(parfile)
  nside    = parse_int(handle,'nside')
  nstokes  = parse_int(handle,'nstokes')
  nsims    = parse_int(handle,'nsims')

  ncf      = parse_double(handle,'ncvm_cf')
  mcf      = parse_double(handle,'map_cf')

  mapfile = parse_string(handle,'mapfile')
  maskfile = parse_string(handle,'maskfile')
  ncvmfile = parse_string(handle,'ncvmfile')
  outroot  = parse_string(handle,'output_root')

  call parse_finish(handle)

  call mask%set(maskfile)
  
  if(nside   .ne. mask%s%nside)   stop "mask has wrong nside"
  if(nstokes .gt. mask%s%nstokes) stop "not enough columns in mask"
     
  npix     = mask%s%npix
  nstokes  = mask%s%nstokes
  nfull    = npix*nstokes

  allocate(T(nfull,nfull))
  inquire(iolength=rcl) T(:,1)
  open(newunit=unit,file=ncvmfile,status='old',action='read',form='unformatted',access='direct',recl=rcl)
  do i = 1,nfull
     read(unit,rec=i) T(:,i)
  end do
  close(unit)
  T = T*ncf

  allocate(X(nfull,nsims) ,source = 0._dp)
  open(newunit=unit,file=mapfile,status='old',action='read')
  do i = 1,nsims
     read(unit,'(a)') filename
     call read_map(trim(filename),map,mask%s)
     X(:,i) = map2vec(map)
  end do
  X = X*mcf

  do i = 1,ntot
     X(i,:) = X(i,:) -sum(X(i,:))/nsims
  end do

  allocate(S(nfull,nfull),source = 0._dp)
  allocate(W(nfull,nfull),source = 0._dp)
  
  call dsyrk('L','N',nfull,nsims,1.d0,X,nfull,0.d0,S,nfull)
  X = X**2
  call dsyrk('L','N',nfull,nsims,1.d0,X,nfull,0.d0,W,nfull)
  deallocate(X)

  call symmetrize_matrix(S ,upper = .false.)
  call symmetrize_matrix(W ,upper = .false.)

  W = (W/nsims - (S/nsims)**2)*nsims**2/(nsims-1._dp)**3
  S = S/(nsims -1._dp)
  
  allocate(sqmask(nfull,nfull))

  do i = 1,nfull
     sqmask(:,i) = mask%lmask(:) .and. mask%lmask(i)
  end do

  lambda = sum(W,mask=sqmask)/sum((T-S)**2,mask=sqmask)
  lambda = max(0._dp,min(1._dp,lambda))
  where(sqmask) T = lambda*T +(1._dp-lambda)*S

  T = T/ncf
  !T = S

  open(newunit=unit,file=trim(outroot)//'_shrink_ncvm.dat',status='unknown',action='write',form='unformatted',access='direct',recl=rcl)
  do i = 1,nfull
     write(unit,rec=i) T(:,i)
  end do
  close(unit)

  open(newunit=unit,file=trim(outroot)//'_shrink_lambda.txt',status='unknown',action='write')
  write(unit,*) lambda
  close(unit)

end program shrink_ncvm
