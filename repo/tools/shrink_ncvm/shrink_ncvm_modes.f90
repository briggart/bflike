program shrink_ncvm

  use healpix_modules ,only : i8b ,i4b ,dp 
  use paramfile_io
  use lpl_utils_mod   ,only : symmetrize_matrix ,map2vec ,sky_mask ,maxlen ,read_map ,mask_matrix ,mask_vec
  use bflike_types_mod,only : allocatable_matrix
  use bflike_smw_mod  ,only : read_plms_external
  
  implicit none
  
  integer(i4b) ,parameter :: nested = 2
  integer(i4b)            :: nsims ,i ,j ,n ,ntot ,ncmd ,nside ,rcl ,unit ,nstokes ,nfull ,ntemp ,nq ,nu ,nl ,nmodes ,lmax ,l ,istart ,istop ,off
  integer(i8b)           :: npix 
  real(dp) ,allocatable  :: T(:,:) ,dX(:,:) ,dT(:,:) ,map(:,:) ,v(:) &
       ,Tref(:,:) ,Xref(:,:) ,P(:,:) ,Q(:,:) ,R(:,:) ,norm(:,:)
       
  real(dp)               :: lambda_T ,lambda_E ,lambda_B ,ncf ,mcf
  logical ,allocatable   :: sqmask(:,:) 
  character(maxlen)      :: ncvmfile ,parfile ,mapfile ,maskfile ,outroot ,filename ,plmsfile 
  type(allocatable_matrix) ,allocatable :: evals(:), evecs(:) ,blocks(:,:)
  type(paramfile_handle) :: handle
  type(sky_mask)         :: mask
  logical                :: has_t ,has_p ,has_x
!  logical ,allocatable   :: mask(:)
  
  ncmd = command_argument_count()
  if(ncmd .ne. 1) then
     write(*,*) 'CRITICAL ERROR : usage shrink_ncvm params.ini'
     stop
  else
     call get_command_argument(1,parfile)
  end if

  handle   = parse_init(parfile)
  nside    = parse_int(handle,'nside')
  nstokes  = parse_int(handle,'nstokes')
  nsims    = parse_int(handle,'nsims')

  ncf      = parse_double(handle,'ncvm_cf')
  mcf      = parse_double(handle,'map_cf')

  mapfile  = parse_string(handle,'mapfile')
  maskfile = parse_string(handle,'maskfile')
  plmsfile = parse_string(handle,'basisfile')
  ncvmfile = parse_string(handle,'ncvmfile')
  outroot  = parse_string(handle,'output_root')

  call parse_finish(handle)

  call mask%s%set(nside=nside,ordering=nested,npix=npix,nstokes=nstokes)

  if(.not.read_plms_external(plmsfile,lmax,evals,evecs,blocks,norm,mask%lmask)) &
       stop 'error reading plms file'
  
  
  do l = 2,lmax
     nl = size(evecs(l)%m(1,:))
     do n = 1,nl
        evecs(l)%m(1,:) = evecs(l)%m(1,:)/sum(evecs(l)%m(1,:)**2)
     end do
  end do

  nmodes = (lmax+1)**2 - 4
  
  npix  = 12*nside**2
  
  ntemp = count(mask%lmask(1:npix))
  nq    = count(mask%lmask(npix+1:2*npix))
  nu    = count(mask%lmask(2*npix+1:3*npix))

  ntot = ntemp+nq+nu

  has_t = ntemp .gt. 0
  has_p = nq + nu .gt. 0
  has_x = has_t .and. has_p
  

  if(has_t) then
     off = 1
  else
     off = 0
  end if
  nfull = npix*nstokes

  allocate(Tref(nfull,nfull))
  inquire(iolength=rcl) Tref(:,1)
  open(newunit=unit,file=ncvmfile,status='old',action='read',form='unformatted',access='direct',recl=rcl)
  do i = 1,nfull
     read(unit,rec=i) Tref(:,i)
  end do
  close(unit)
  call mask_matrix(mask%lmask,Tref)
  Tref = Tref*ncf
  T    = Tref

  allocate(Xref(ntot,nsims) ,source = 0._dp)
  open(newunit=unit,file=mapfile,status='old',action='read')
  do i = 1,nsims
     read(unit,'(a)') filename
     call read_map(trim(filename),map,mask%s)
     v = map2vec(map)
     call mask_vec(mask%lmask,v)
     Xref(:,i) = v
  end do
  Xref = Xref*mcf
  
  if(has_t) then
     !we really don't care for T noise

  end if
  
  if(has_p) then
     allocate(P(ntot,nmodes),source = 0._dp)

     !EE
     istop = 0
     do l = 2,lmax
        nl = 2*l+1
        istart = istop +1
        istop  = istart +nl -1
        P(:,istart:istop) = evecs(l)%m(:,off*nl+1:(1+off)*nl)
     end do

     dT = Tref
     call project_matrix(dT,P)
!     T  = T - dT
     dX = Xref
     write(0,*) size(Tref(:,1)),size(Xref(:,1))
     write(0,*) size(dT(:,1)),size(dX(:,1))
     call project_matrix(dX,P)
     call shrinker(dX,dT,lambda_E)
     T  = dT
     
     !BB
     istop = 0
     do l = 2,lmax
        nl = 2*l+1
        istart = istop +1
        istop  = istart +nl -1
        P(:,istart:istop) = evecs(l)%m(:,(1+off)*nl+1:(2+off)*nl)
     end do

     dT = Tref
     call project_matrix(dT,P)
!     T  = T - dT
     dX = Xref
     call project_matrix(dX,P)
     call shrinker(dX,dT,lambda_B)
!     T  = T +dT

  end if

!  T = Xref
  T = T/ncf

  call unmask_ncvm(mask%lmask,T)


  open(newunit=unit,file=trim(outroot)//'_shrink_ncvm.dat',status='unknown',action='write',form='unformatted',access='direct',recl=rcl)
  do i = 1,nfull
     write(unit,rec=i) T(:,i)
  end do
  close(unit)

  open(newunit=unit,file=trim(outroot)//'_shrink_lambda.txt',status='unknown',action='write')
  write(unit,*) lambda_E
  write(unit,*) lambda_B
  close(unit)

contains

  subroutine shrinker(X,T,lambda,mask)

    implicit none
    
    real(dp)           ,intent(inout) :: X(:,:)  
    real(dp)           ,intent(inout) :: T(:,:)
    real(dp)           ,intent(out)   :: lambda
    logical  ,optional ,intent(in)    :: mask(:)

    real(dp) ,allocatable :: S(:,:) ,W(:,:)
    logical  ,allocatable :: sqmask(:,:)
    integer(i4b)          :: nsims ,nfull

    nfull = size(X(:,1))
    nsims = size(X(1,:))
    
    allocate(S ,W ,mold = T)
    write(0,*) nfull
    call dsyrk('L','N',nfull,nsims,1.d0,X,nfull,0.d0,S,nfull)
    X = X**2
    call dsyrk('L','N',nfull,nsims,1.d0,X,nfull,0.d0,W,nfull)

    call symmetrize_matrix(S ,upper = .false.)
    call symmetrize_matrix(W ,upper = .false.)

    W = (W/nsims - (S/nsims)**2)*nsims**2/(nsims-1._dp)**3
    S = S/(nsims -1._dp)
    
    allocate(sqmask(nfull,nfull))
    if (present(mask)) then
       do i = 1,nfull
          sqmask(:,i) = mask(:) .and. mask(i)
       end do
    else
       sqmask(:,:) = .true.
    end if
       
    lambda = sum(W,mask=sqmask)/sum((T-S)**2,mask=sqmask)
    lambda = max(0._dp,min(1._dp,lambda))
    where(sqmask) T = lambda*T +(1._dp-lambda)*S
    T = S
   
  end subroutine shrinker
  
  subroutine project_matrix(M,P)
    implicit none
    real(dp) ,intent(inout) :: M(:,:)
    real(dp) ,intent(in)    :: P(:,:)
    
    real(dp) ,allocatable   :: Q(:,:)
    integer                 :: nrow ,ncol ,ndim
    
    nrow = size(M(:,1))
    ncol = size(M(1,:))
    ndim = size(P(1,:))
    
    if (nrow .ne. size(P(1,:))) then
       M = -huge(dp)
       return
    end if
    
    allocate(Q(ndim,ncol))
!    call dgemm('T','N',ndim,ncol,nrow,1.d0,P,nrow,M,nrow,0.d0,Q,ndim)
!    call dgemm('N','N',nrow,ncol,ndim,1.d0,P,nrow,Q,ndim,0.d0,M,nrow)
    
  end subroutine project_matrix

  subroutine unmask_ncvm(mask,ncvm)
    implicit none
    logical               ,intent(in)    :: mask(:)
    real(dp) ,allocatable ,intent(inout) :: ncvm(:,:)

    logical  ,allocatable :: sqmask(:,:)
    real(dp) ,allocatable :: tmp(:,:)
    integer(i4b)          :: i ,n

    n = size(mask)
    allocate(sqmask(n,n) ,tmp(n,n))
    do i = 1,n
       sqmask(:,i) = mask(:) .and. mask(i)
    end do

    tmp = unpack(reshape(ncvm,[n*n]),mask=sqmask,field= 0._dp)
    call move_alloc(from=tmp,to=ncvm)

  end subroutine unmask_ncvm
  
end program shrink_ncvm
