module filter_matrix

  use healpix_modules
  use lpl_utils_mod
  implicit none

contains

  subroutine output_heigen(alm,file,debug)

    implicit none
    complex(dpc)      ,intent(in) :: alm(:,0:,0:,:)
    character(len=*)  ,intent(in) :: file
    logical ,optional ,intent(in) :: debug

    integer      :: nstokes ,lmax ,mmax ,nmaps ,unit ,im ,in 
    integer(i8b) :: cnt
    logical      :: dbg

    nstokes = size(alm(:,0,0,1))
    lmax    = size(alm(1,:,0,1)) -1
    mmax    = size(alm(1,0,:,1)) -1
    nmaps   = size(alm(1,0,0,:))
    write(0,*) nstokes
    write(0,*) lmax
    write(0,*) mmax
    write(0,*) nmaps

    open(newunit=unit,file=file,status='unknown',action='write',form='unformatted')
    write(unit) nstokes
    write(unit) lmax
    write(unit) mmax
    write(unit) nmaps
    
    if (present(debug)) then
       dbg = debug
    else
       dbg = .false.
    end if

    if (dbg) then 
       write(*,*) 'writing heigen on '//file
       write(*,*) 'nstokes = ',nstokes
       write(*,*) 'lmax    = ',lmax
       write(*,*) 'mmax    = ',mmax
       write(*,*) 'nmaps   = ',nmaps
       cnt = nstokes*(lmax+1)*(mmax+1)*nmaps
       write(*,*) 'values to write = ',cnt
    end if
    cnt = 0_i8b
    do in = 1,nmaps
       do im = 0,mmax
          write(unit) alm(1:nstokes,im:lmax,im,in)
       end do
    end do
    close(unit)

    if(dbg) write(*,*) 'values written = ',cnt

  end subroutine output_heigen

  subroutine input_heigen(file,alm,nstokes,lmax,mmax,nmaps,old_format)

    implicit none
    character(len=*)          ,intent(in)  :: file
    complex(dpc) ,allocatable ,intent(out) :: alm(:,:,:,:)
    integer(i4b) ,optional    ,intent(out) :: nstokes ,lmax ,mmax ,nmaps
    logical      ,optional    ,intent(in)  :: old_format   

    integer :: ns ,nl ,nm ,nn ,unit ,il ,im ,in ,istat
    logical :: of

    if(present(old_format)) then
       of = old_format
    else
       of = .false.
    end if

    if(allocated(alm)) deallocate(alm)

    open(newunit=unit,file=file,status='old',action='read',form='unformatted',iostat=istat)
    if(istat .eq. 0) then
       
       read(unit) ns
       read(unit) nl
       read(unit) nm
       read(unit) nn
       allocate(alm(ns,0:nl,0:nm,nn))
       alm = 0._dp
       if (of) then
          do in = 1,nn
             do im = 0,nm
                do il = 0,nl
                   read(unit) alm(1:ns,il,im,in)
                end do
             end do
          end do
        else
          do in = 1,nn
             do im = 0,nm
                read(unit) alm(1:ns,im:lmax,im,in)
             end do
          end do
       end if
       close(unit)
       if(present(nstokes)) nstokes = ns
       if(present(lmax))    lmax    = nl
       if(present(mmax))    mmax    = nm
       if(present(nmaps))   nmaps   = nn
    else
       if(present(nstokes)) nstokes = -1
       if(present(lmax))    lmax    = -1
       if(present(mmax))    mmax    = -1
       if(present(nmaps))   nmaps   = -1
    end if
  end subroutine input_heigen

  subroutine decompose_matrix(m,evec,eval,error,n_eval,overwrite)
    real(dp) ,intent(inout) ,target     :: m(:,:)
    real(dp) ,intent(out)               :: evec(:,:) ,eval(:)
    integer(i4b) ,intent(out) ,optional :: error ,n_eval
    logical ,intent(in) ,optional       :: overwrite

    real(dp) ,allocatable     :: work(:) 
    real(dp) ,pointer         :: tmp(:,:) => null()
    integer(i4b) ,allocatable :: iwork(:)
    integer(i4b) ,allocatable :: isuppz(:)

    integer(i4b)              :: lwork ,liwork ,ntot ,info ,neigen
    logical                   :: ow = .true.

    real(kind=8)              :: dlamch
    external dlamch
    
    ntot = size(m(:,1))

    if (present(overwrite)) then
       ow = overwrite
    else
       ow = .false.
    end if
    
    if (ow) then
       tmp => m
    else
       allocate(tmp,source=m)
    end if

    allocate(work(1) ,iwork(1) ,isuppz(2*ntot))
    call dsyevr('V', 'A', 'L', ntot, tmp, ntot, 0.d0, 0.d0, 0, 0, dlamch('S'), &
         neigen, eval, evec, ntot, isuppz, work, -1, iwork, -1, info)
    
    lwork  = work(1)
    liwork = iwork(1)

    deallocate(work ,iwork)
    allocate(work(lwork) ,iwork(liwork))
    call dsyevr('V', 'A', 'L', ntot, tmp, ntot, 0.d0, 0.d0, 0, 0, dlamch('S'), &
         neigen, eval, evec, ntot, isuppz, work, lwork, iwork, liwork, info)

    if (present(error)) then
       error = info
    else
       if(info .ne. 0) then
          write(*,*) 'info = ',info
          stop
       end if
    end if
    deallocate(work ,iwork)

    if(ow) then
       tmp => null()
    else
       deallocate(tmp)
    end if

    if(present(n_eval)) n_eval = neigen

  end subroutine decompose_matrix

!  subroutine recompose_matrix(evec,eval,n_eval,m,overwrite)
  subroutine recompose_matrix(evec,eval,m,overwrite)
    !only the lower part of m is returned for now
    implicit none
    real(dp) ,intent(inout) ,target :: evec(:,:)
    real(dp) ,intent(in)            :: eval(:)
    real(dp) ,intent(out)           :: m(:,:)
    logical  ,intent(in)  ,optional :: overwrite

    real(dp)     ,pointer :: tmp(:,:) => null()
    integer(i4b)          :: n_eval
    integer(i4b)          :: ntot ,i
    logical               :: ow

    ntot   = size(m(:,1))
    if (size(evec(:,1)) .ne. ntot) then
       print*,'evec(:,1) and m(:,1) must have the same size'
       stop
    end if

    n_eval = size(evec(1,:))

    if (present(overwrite)) then
       ow = overwrite
    else
       ow = .false.
    end if
    
    if (ow) then
       tmp => evec
    else
       allocate(tmp,source=evec)
    end if

    do concurrent (i = 1:n_eval)
       tmp(:,i) = tmp(:,i)*sqrt(eval(i))
    end do

    call dsyrk('L','N',ntot,n_eval,1.d0,tmp,ntot,0.d0,m,ntot)
   
    if(ow) then
       tmp => null()
    else
       deallocate(tmp)
    end if

  end subroutine recompose_matrix
 
  subroutine eigen2harmonic(eigen,lmax,heigen,weights)
    real(dp)     ,intent(in)               :: eigen(:,:)
    integer(i4b) ,intent(in)               :: lmax
    complex(dpc) ,intent(out) ,allocatable :: heigen(:,:,:,:)
    real(dp)     ,intent(in) ,optional     :: weights(:,:)

    real(dp) ,allocatable  :: w8(:,:) ,map(:,:)
    integer(i4b)           :: ntot ,nside ,npix ,nstokes ,i

    ntot = size(eigen(:,1))
    call ntot2mappix(ntot,nside,npix,nstokes)

    allocate(map(0:npix-1,nstokes))
    allocate(heigen(nstokes,0:lmax,0:lmax,ntot))

    if (present(weights)) then
       allocate(w8,source=weights)
    else
       call get_ring_weights_pol(nside,w8)
    end if

    do i = 1,ntot
       call vec2map(eigen(:,i),map)
       call convert_nest2ring(nside,map(:,:))
       call map2alm_iterative(nside,lmax,lmax,0,map,heigen(1:nstokes,0:lmax,0:lmax,i),[-1._dp,1._dp],w8)
    end do

  end subroutine eigen2harmonic

  subroutine eigen2harmonic_p(eigen,lmax,heigen,weights)
    !only the polarization part of the real space is passed to the subroutine
    implicit none
    real(dp)     ,intent(in)               :: eigen(:,:)
    integer(i4b) ,intent(in)               :: lmax
    complex(dpc) ,intent(out) ,allocatable :: heigen(:,:,:,:)
    real(dp)     ,intent(in)  ,optional    :: weights(:,:)

    real(dp) ,allocatable     :: w8(:,:) ,map(:,:)
    complex(dpc) ,allocatable :: alm(:,:,:) 
    integer(i4b)              :: ntot ,nside ,npix ,nstokes ,n_eigen ,i

    ntot    = 3*size(eigen(:,1))/2
    call ntot2mappix(ntot,nside,npix,nstokes)

    n_eigen = size(eigen(1,:))

    allocate(map(0:npix-1,nstokes))
    if(allocated(heigen)) deallocate(heigen)
    allocate(heigen(nstokes-1,0:lmax,0:lmax,n_eigen))
    allocate(alm(nstokes,0:lmax,0:lmax))

    if (present(weights)) then
       allocate(w8,source=weights)
    else
       call get_ring_weights_pol(nside,w8)
    end if

    print*,'::',sum(eigen**2)
    do i = 1,2*npix
       write(700,*) eigen(i,1)
    end do
    do i = 1,n_eigen
       call Pvec2IQU(eigen(:,i),map)
       call convert_nest2ring(nside,map)
       call map2alm_iterative(nside,lmax,lmax,0,map,alm,[-1._dp,1._dp],w8)
       heigen(:,:,:,i) = alm(2:3,:,:)
    end do
    print*,'::',sum(alm(2:3,2:,2:)**2)
    print*,'::',sum(heigen(:,2:,2:,1)**2)

  end subroutine eigen2harmonic_p
 
  subroutine eigen2harmonic_spin(eigen,lmax,heigen,weights)
    !only the polarization part of the real space is passed to the subroutine
    implicit none
    real(dp)     ,intent(in)               :: eigen(:,:)
    integer(i4b) ,intent(in)               :: lmax
    complex(dpc) ,intent(out) ,allocatable :: heigen(:,:,:,:)
    real(dp)     ,intent(in) ,optional     :: weights(:,:)

    real(dp) ,allocatable     :: w8(:,:) ,map(:,:)
    integer(i4b)              :: ntot ,nside ,npix ,nstokes ,n_eigen ,i

    ntot    = 3*size(eigen(:,1))/2
    call ntot2mappix(ntot,nside,npix,nstokes)

    n_eigen = size(eigen(1,:))

    allocate(map(0:npix-1,nstokes-1))
    allocate(heigen(nstokes-1,0:lmax,0:lmax,n_eigen))

    if (present(weights)) then
       allocate(w8,source=weights)
    else
       call get_ring_weights_pol(nside,w8)
    end if

    do i = 1,n_eigen
!       call Pvec2IQU(eigen(:,i),map)
       call vec2map(eigen(:,i),map)
       call convert_nest2ring(nside,map)
       call map2alm_spin(nside,lmax,lmax,2,map,heigen(:,:,:,i),[-1._dp,1._dp],w8)
    end do

  end subroutine eigen2harmonic_spin

  subroutine harmonic2eigen(heigen,evec)
    complex(dpc) ,intent(in)  :: heigen(:,0:,0:,:)
    real(dp)     ,intent(out) :: evec(:,:)

    real(dp) ,allocatable  :: plm(:,:) ,map(:,:)
    integer(i4b)           :: ntot ,nside ,npix ,nstokes ,nplm ,lmax ,i

    lmax = size(heigen(1,:,0,1)) -1 
    ntot = size(evec(1,:))
    if (ntot .ne. size(heigen(1,0,0,:))) then
       print*,'eigen(1,:) and heigen(1,0,0,:) must have the same size'
       stop
    end if
    call ntot2mappix(ntot,nside,npix,nstokes)

    nplm = nside*(lmax+1)*(2*lmax-lmax+2) 
    allocate(map(0:npix-1,nstokes))
    allocate(plm(0:nplm-1,nstokes))
    call plm_gen(nside,lmax,lmax,plm)

    do i = 1,ntot
       call alm2map(nside,lmax,lmax,heigen(1:nstokes,0:lmax,0:lmax,i),map(:,:),plm=plm)
       call convert_ring2nest(nside,map(:,:))
!       call map2vector(map,evec(:,i))
       evec(:,i) = map2vec(map)
    end do

  end subroutine harmonic2eigen

  subroutine harmonic2eigen_p(heigen,evec)
    !outputs only the polarization part of the real space eigenvector
    implicit none
    complex(dpc) ,intent(in)     :: heigen(:,0:,0:,:)
    real(dp)     ,intent(out)    :: evec(:,:)

    real(dp) ,allocatable     :: map(:,:)
    integer(i4b)              :: ntot ,nside ,npix ,nstokes ,n_eigen ,i ,lmax ,mmax
    complex(dpc) ,allocatable :: alm(:,:,:)

    ntot    = 3*size(evec(:,1))/2
    n_eigen = size(evec(1,:))
    lmax    = size(heigen(1,:,0,1)) -1
    mmax    = lmax
    if (n_eigen .ne. size(heigen(1,0,0,:))) then
       print*,'eigen(1,:) and heigen(1,0,0,:) must have the same size'
       stop
    end if
    call ntot2mappix(ntot,nside,npix,nstokes)

    allocate(map(0:npix-1,nstokes))
    allocate(alm(nstokes,0:lmax,0:mmax))

!    print*,'>>>',sum(abs(heigen(2:3,:,:,:))),sum(plm)
    evec = 0._dp
    do i = 1,n_eigen
       alm(2:nstokes,:,:) = heigen(:,:,:,i)
       call alm2map(nside,lmax,mmax,alm,map)
       call convert_ring2nest(nside,map)
!       call map2vector_p(map,evec(:,i))
       evec(:,i) = IQU2Pvec(map)
!       write(1000,'(i5,3(2x,e25.12))') i,sum(map(:,2:3)**2),sum(evec(:,i)**2),sum(abs(alm(2:nstokes,:,:)))
    end do

  end subroutine harmonic2eigen_p

  subroutine harmonic2eigen_spin(heigen,evec)
    !outputs only the polarization part of the real space eigenvector
    implicit none
    complex(dpc) ,intent(in)  :: heigen(:,0:,0:,:)
    real(dp)     ,intent(out) :: evec(:,:)

    real(dp) ,allocatable     :: map(:,:)
    integer(i4b)              :: ntot ,nside ,npix ,nstokes ,n_eigen ,i ,lmax ,mmax

    ntot    = 3*size(evec(:,1))/2
    n_eigen = size(evec(1,:))
    lmax    = size(heigen(1,:,0,1)) -1
    mmax    = lmax
    if (n_eigen .ne. size(heigen(1,0,0,:))) then
       print*,'eigen(1,:) and heigen(1,0,0,:) must have the same size'
       stop
    end if
    call ntot2mappix(ntot,nside,npix,nstokes)

    allocate(map(0:npix-1,2))

    do i = 1,n_eigen
       call alm2map_spin(nside,lmax,mmax,2,heigen(:,:,:,i),map)
       call convert_ring2nest(nside,map)
       evec(:,i) = map2vec(map)
    end do

  end subroutine harmonic2eigen_spin

  subroutine filter_heigen(heigen,filter,nside,lmax)
    implicit none
    complex(dpc) ,intent(inout) :: heigen(:,0:,0:,:)
    real(dp)     ,intent(in)    :: filter(0:,:)
    integer(i4b) ,intent(in)    :: lmax

    integer(i4b)  :: neigen ,nside ,l1 ,i ,nstokes

    l1 = size(filter(:,1)) -1
    if (lmax .gt. l1) then
       print*, 'lmax must be <= filter_lmax'
       stop
    end if
    l1 = size(heigen(1,:,0,1)) -1
    if (lmax .gt. l1) then
       print*, 'lmax must be <= heigen_lmax'
       stop
    end if

    nstokes = size(heigen(:,0,0,1))
    neigen  = size(heigen(1,0,0,:))

    do i = 1,neigen
       call alter_alm(nside,lmax,lmax,0._dp,heigen(1:nstokes,0:lmax,0:lmax,i),window=filter)
    end do

  end subroutine filter_heigen

  subroutine filter_heigen_p(heigen,filter,nside,lmax)
    !directly harmonic filtering of eigenvectors
    implicit none
    complex(dpc) ,intent(inout) :: heigen(:,0:,0:,:)
    real(dp)     ,intent(in)    :: filter(0:,:)
    integer(i4b) ,intent(in)    :: lmax

    real(dp)     ,allocatable :: hf(:,:,:)   
    integer(i4b)              :: neigen ,nside ,l1 ,i 

    l1 = size(filter(:,1)) -1
    if (lmax .gt. l1) then
       print*, 'lmax must be <= filter_lmax'
       stop
    end if
    l1 = size(heigen(1,:,0,1)) -1
    if (lmax .gt. l1) then
       print*, 'lmax must be <= heigen_lmax'
       stop
    end if

    neigen  = size(heigen(1,0,0,:))

    allocate(hf(2,0:lmax,0:lmax))
    hf = 1._dp
    do l1 = 0,lmax
       hf(1,l1:lmax,l1) = filter(l1:lmax,2)
       hf(2,l1:lmax,l1) = filter(l1:lmax,3)
    end do

    do concurrent(i = 1:neigen)
       heigen(1:2,0:lmax,0:lmax,i) = heigen(1:2,0:lmax,0:lmax,i)*hf(1:2,0:lmax,0:lmax)
    end do

  end subroutine filter_heigen_p

  subroutine filter_heigen_hpx(heigen,filter,nside,lmax)
    !uses healpix alter_alm
    implicit none
    complex(dpc) ,intent(inout) :: heigen(:,0:,0:,:)
    real(dp)     ,intent(in)    :: filter(0:,:)
    integer(i4b) ,intent(in)    :: lmax

    complex(dpc) ,allocatable :: alm(:,:,:)
    integer(i4b)              :: neigen ,nside ,l1 ,i ,nstokes

    l1 = size(filter(:,1)) -1
    if (lmax .gt. l1) then
       print*, 'lmax must be <= filter_lmax'
       stop
    end if
    l1 = size(heigen(1,:,0,1)) -1
    if (lmax .gt. l1) then
       print*, 'lmax must be <= heigen_lmax'
       stop
    end if

    nstokes = size(heigen(:,0,0,1)) +1
    neigen  = size(heigen(1,0,0,:))

    allocate(alm(nstokes,0:lmax,0:lmax))
    do i = 1,neigen
       alm(2:nstokes,0:lmax,0:lmax) = heigen(1:2,0:lmax,0:lmax,i)
       call alter_alm(nside,lmax,lmax,0._dp,alm,window=filter)
       heigen(1:2,0:lmax,0:lmax,i)  = alm(2:nstokes,0:lmax,0:lmax)
    end do

  end subroutine filter_heigen_hpx

  subroutine filter_eigenvector(eigen,filter,lmax,weights)
    implicit none
    real(dp) ,intent(inout)        :: eigen(:,:)
    real(dp) ,intent(in)           :: filter(0:,:)
    integer(i4b) ,intent(in)       :: lmax
    real(dp) ,intent(in) ,optional :: weights(:,:)

    real(dp) ,allocatable          :: w8(:,:) ,map(:,:)
    complex(dpc) ,allocatable      :: alm(:,:,:)
    integer(i4b)  :: ntot ,nside ,npix ,nstokes ,i

    ntot = size(eigen(:,1))
    call ntot2mappix(ntot,nside,npix,nstokes)

    allocate(map(0:npix-1,nstokes))
    allocate(alm(nstokes,0:lmax,0:lmax))

    if (present(weights)) then
       allocate(w8,source=weights)
    else
       call get_ring_weights_pol(nside,w8)
    end if

    do i = 1,ntot
       call vec2map(eigen(:,i),map)
       call convert_nest2ring(nside,map(:,:))
       call map2alm_iterative(nside,lmax,lmax,0,map,alm,[-1._dp,1._dp],w8)
       call alter_alm(nside,lmax,lmax,0._dp,alm,window=filter)
       call alm2map(nside,lmax,lmax,alm,map(:,:))
       call convert_ring2nest(nside,map(:,:))
       eigen(:,i) = map2vec(map)
    end do

  end subroutine filter_eigenvector

  subroutine ntot2mappix(ntot,nside,npix,nstokes)
    integer(i4b) ,intent(in)  :: ntot
    integer(i4b) ,intent(out) :: nside ,npix ,nstokes

    !ntot = 12*nside*nside*nstokes 

    if (9*(ntot/9) .eq. ntot) then
       nstokes = 3
    else
       nstokes = 1
    end if

    npix  = ntot/nstokes
    nside = npix2nside(npix)

  end subroutine ntot2mappix

end module filter_matrix
