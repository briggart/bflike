program fit_ncvm

#ifdef INTEL_COMPILER
  use ifport
#endif

  use mpi_f08

  use healpix_modules
  use lpl_utils_mod
  use filter_matrix
  implicit none
  
  integer(i4b) ,parameter :: ring = 1 ,nested = 2

  real(dp)     ,allocatable :: filter(:,:) ,cov(:,:) ,eval(:) ,evec(:,:)      &
       ,map(:,:) ,llike(:,:,:) ,tmp(:,:) ,auxm(:,:) ,tmpcov(:,:) ,chi2(:,:,:) &
       ,rfilter(:,:) ,v(:) ,buff(:,:,:)
  complex(dpc) ,allocatable :: alm(:,:,:,:) ,almr(:,:,:,:)

  real(dp)                  :: amin ,amax ,fmin ,fmax ,cmin ,cmax ,aa ,cc &
       ,ff ,astep ,cstep ,fstep ,regnoise_P ,rp2 ,abest ,cbest ,fbest         &
       ,bestlike ,lgdet ,dchi

  integer(i4b)            :: nside_in ,nstokes ,lmax ,npix_in ,ntot_in ,nqu_in&
       ,nside_fit ,npix_fit ,nqu_fit ,ntot_fit ,nmask ,lmin ,i ,info ,rcl     &
       ,unit ,j ,ncmd ,ndata ,ne_min ,nstep ,ja ,jc ,jf ,l ,fscale ,loff ,l0   
 
  integer                 :: ierr ,myid ,nproc ,master ,ntodo ,res ,id(3) ,ja_start ,ja_stop ,root

  real                    :: t1 ,t2 ,et(2)
  character(len=1024)     :: mapfile ,evecfile ,evalfile ,outroot ,parfile    &
       ,maskfile
  type(paramfile_handle)  :: handle
  type(planck_rng)        :: rng_handle
  type(sky_mask)          :: mask
  
  logical ,allocatable    :: lmask(:)
  logical                 :: regularize ,regularize_maps ,p_only &
       ,harmonic_evecs ,old_format ,apply_pwf

  call mpi_init(ierr)
  call mpi_comm_rank(mpi_comm_world, myid, ierr)
  call mpi_comm_size(mpi_comm_world, nproc, ierr)

  root = 0

  if(myid .eq. root) then 

     ncmd = command_argument_count()

     if(ncmd .ne. 1) then
        write(*,*) 'CRITICAL ERROR : usage fit_ncvm params.ini'
        stop
     else
        call get_command_argument(1,parfile)
     end if
     
     handle  = parse_init(parfile)

     nside_in  = parse_int(handle,'nside_in')
     nside_fit = parse_int(handle,'nside_fit')
     nstokes   = parse_int(handle,'nstokes')
     p_only    = parse_lgt(handle,'p_only')
     
     npix_in = nside2npix(nside_in)
     ntot_in = npix_in*nstokes
     if (nstokes .eq. 3) nqu_in = 2*npix_in
     if (nstokes .ne. 3) stop 'only fitting of polarization supported'
     
     npix_fit = nside2npix(nside_fit)
     ntot_fit = nstokes*npix_fit
     nqu_fit  = 2*npix_fit
     
     evecfile = parse_string(handle,'evectors')
     evalfile = parse_string(handle,'evalues')
     
     ndata    = parse_int(handle,'ndata')
     mapfile  = parse_string(handle,'maps')
     outroot  = parse_string(handle,'outroot')
     
     maskfile = parse_string(handle,'maskfile')
     
     regnoise_P      = parse_double(handle,'regnoise_P')
     regularize      = parse_lgt(handle,'regularize')
     regularize_maps = parse_lgt(handle,'regularize_maps')
     harmonic_evecs  = parse_lgt(handle,'harmonic_evecs')
     old_format      = parse_lgt(handle,'old_format',.false.)
     apply_pwf       = parse_lgt(handle,'apply_pwf')
     
     nstep    = parse_int(handle,'nstep')
     
     !f(l) = F0(1 +c(l/l0)^alpha) 
     amin     = parse_double(handle,'alpha_min')
     amax     = parse_double(handle,'alpha_max')
     
     fmin     = parse_double(handle,'F0_min')
     fmax     = parse_double(handle,'F0_max')
     
     cmin     = parse_double(handle,'c_min')
     cmax     = parse_double(handle,'c_max')
     
     lmin     = parse_int(handle,'lmin')
     lmax     = parse_int(handle,'lmax')
     l0       = parse_double(handle,'l0')
     loff     = parse_double(handle,'loff')
     
     call parse_finish(handle)
     
     call mask%set(maskfile)
     allocate(lmask ,source = mask%lmask(npix_fit:ntot_fit-1))
     nmask = count(lmask)
     
     allocate(map(nmask,ndata),tmp(0:npix_fit-1,nstokes))
     if (ndata .eq. 1) then
        call validate_mapfile(mapfile,npix_fit,nstokes,nested)
        call input_map(mapfile,tmp,npix_fit,nstokes)
        v = map2vec(tmp)
        call mask_vec(mask%lmask,v)
        map(1:nmask,1) = v(1:nmask)
        
     else
        open(newunit=unit,file=mapfile,status='old',action='read')
        do i = 1,ndata
           read(unit,'(a)') mapfile
           call validate_mapfile(mapfile,npix_fit,nstokes,nested)
           call input_map(mapfile,tmp,npix_fit,nstokes)
           v = map2vec(tmp)
           call mask_vec(mask%lmask,v)
           map(:,i) = v
        end do
        close(unit)
     end if
     deallocate(tmp,v)
     call mask%free_mem()
     
     regularize = regularize .and. (regnoise_P .gt. 0._dp)
     
     if( (.not.regularize) .and. regularize_maps ) then
        print*,'you must have regnoise_P > 0 to regularize maps and NCVM'
        regularize_maps = .false.
     else
        call rand_init(rng_handle,658402,83063,8043,90555)
        do i = 1,ndata
           do j = 1,nmask
              map(j,i) = map(j,i) +regnoise_P*rand_gauss(rng_handle)
           end do
        end do
     end if
     
     rp2 = regnoise_P**2
     
     if (harmonic_evecs) then
        !eigenvectors are already in harmonic space, so we just need to read them
        call input_heigen(trim(evecfile),almr,old_format=old_format)
     else
        !read eigenvectors and convert to s-harmonics 
        allocate(evec(nqu_in,1))
        allocate(almr(2,0:lmax,0:lmax,nqu_in))
        inquire(iolength=rcl) evec
        open(newunit=unit,file=trim(evecfile),status='old',action='read',&
             form='unformatted',access='direct',recl=rcl)
        read(unit,rec=1) evec
        call eigen2harmonic_spin(evec,lmax,alm)
        almr(:,:,:,1)      = alm(:,:,:,1)
        !     print*,1,sum(evec**2),sum(almr(:,2:,2:,1))
        call eigen2harmonic_spin(evec,lmax,alm)
        almr(:,:,:,1)      = alm(:,:,:,1)
        !     print*,1,sum(evec**2),sum(almr(:,2:,2:,1))
        read(unit,rec=1) evec
        print*,sum(evec**2)
        call eigen2harmonic_spin(evec,lmax,alm)
        almr(:,:,:,1)      = alm(:,:,:,1)
        do i = 1,nqu_in
           read(unit,rec=i) evec
           call eigen2harmonic_spin(evec,lmax,alm)
           almr(:,:,:,i)      = alm(:,:,:,1)
        end do
        close(unit)
        deallocate(evec,alm)
        call output_heigen(almr,trim(evecfile)//'-alm')
     end if
     write(*,*) 'read evecs'

     !read eigenvalues
     allocate(eval(nqu_in))
     open(newunit=unit,file=trim(evalfile),status='old',action='read')
     do i = 1,nqu_in
        read(unit,*) eval(i)
     end do
     close(unit)
      
     ne_min = 1
     i = 1
     do while (eval(i) .le. 1.e-12_dp*eval(nqu_in))
        eval(i) = 0._dp
        i       = i+1
        ne_min  = i
     end do
     
     if (regularize) then
        fscale = 1 
     else
        fscale = 10
     end if
     
     astep = (amax - amin)/nstep
     cstep = (cmax - cmin)/nstep
     fstep = (fmax - fmin)/(nstep*fscale)
     
     allocate(rfilter(0:lmax,3))
     rfilter = 1._dp
     
     if(apply_pwf) call pixel_window(rfilter,nside_fit)
     
     do l = lmin+1,lmax
        rfilter(l,:) = rfilter(l,:)*(1._dp +cos(real(l-lmin,dp)*pi/(lmax-lmin)))*0.5_dp
     end do
     rfilter(0:1,2:3) = 0._dp

  end if
  
  call mpi_bcast(regularize,  1, mpi_logical, root, mpi_comm_world, ierr)
  call mpi_bcast(rp2,  1, mpi_double_precision, root, mpi_comm_world, ierr)

  call mpi_bcast(amin, 1, mpi_double_precision, root, mpi_comm_world, ierr)
  call mpi_bcast(cmin, 1, mpi_double_precision, root, mpi_comm_world, ierr)
  call mpi_bcast(fmin, 1, mpi_double_precision, root, mpi_comm_world, ierr)

  call mpi_bcast(astep, 1, mpi_double_precision, root, mpi_comm_world, ierr)
  call mpi_bcast(cstep, 1, mpi_double_precision, root, mpi_comm_world, ierr)
  call mpi_bcast(fstep, 1, mpi_double_precision, root, mpi_comm_world, ierr)

  call mpi_bcast(fscale, 1, mpi_double_precision, root, mpi_comm_world, ierr)

  call mpi_bcast(lmax, 1, mpi_int, root, mpi_comm_world, ierr)
  call mpi_bcast(loff, 1, mpi_int, root, mpi_comm_world, ierr)
  call mpi_bcast(l0,   1, mpi_int, root, mpi_comm_world, ierr)

  call mpi_bcast(nstep, 1, mpi_int, root, mpi_comm_world, ierr)
  call mpi_bcast(nmask, 1, mpi_int, root, mpi_comm_world, ierr)
  call mpi_bcast(ndata, 1, mpi_int, root, mpi_comm_world, ierr)

  call mpi_bcast(nqu_in,  1, mpi_int, root, mpi_comm_world, ierr)
  call mpi_bcast(ne_min,  1, mpi_int, root, mpi_comm_world, ierr)
  call mpi_bcast(nqu_fit, 1, mpi_int, root, mpi_comm_world, ierr)

  if(myid .ne. root) then
     allocate(rfilter(0:lmax,3))
     allocate(eval(nqu_in))
     allocate(almr(2,0:lmax,0:lmax,nqu_in))
     allocate(map(nmask,ndata))
     allocate(lmask(nqu_fit))
  end if

  call mpi_bcast(almr, 2*(lmax+1)*(lmax+1)*nqu_in, mpi_double_complex, root, &
       mpi_comm_world, ierr)
  call mpi_bcast(rfilter, 3*(lmax+1), mpi_double_precision, root, &
       mpi_comm_world, ierr)
  call mpi_bcast(map, nmask*ndata, mpi_double_precision, root, &
       mpi_comm_world, ierr)
  call mpi_bcast(eval, nqu_in, mpi_double_precision, root, mpi_comm_world, ierr)
  call mpi_bcast(lmask, nqu_fit, mpi_logical, root, mpi_comm_world, ierr)

  allocate(auxm,source = map)
  allocate(evec(nqu_fit,nqu_in))
  allocate(llike(0:nstep*fscale,0:nstep,0:nstep),source=0._dp)
  allocate(alm,source=almr)
  allocate(cov(nqu_fit,nqu_fit) ,source = 0._dp)
  allocate(tmp,mold=cov)
  allocate(chi2,source=llike)
  allocate(filter,source=rfilter)
  allocate(tmpcov(nmask,nmask))

  ntodo = (nstep+1)/nproc
  res = nstep+1 -ntodo*nproc
  
  ja_start = 0 + min(myid,res)*(ntodo+1) +max(myid-res,0)*ntodo
  if ( myid .lt. res) then
     ja_stop  = ja_start +ntodo
  else
     ja_stop  = ja_start +ntodo -1
  end if

 
  do ja = ja_start,ja_stop
!     cycle
     aa = amin +ja*astep
     do jc = 0,nstep
        t1 = etime(et)
        cc = cmin +jc*cstep
        do concurrent (l = 1:lmax)
           filter(l,:) = (1._dp +cc*(real(loff+l,dp)/(loff+l0))**aa)*rfilter(l,:)
        end do
        
        alm = almr
        call filter_heigen_p(alm(:,:,:,ne_min:nqu_in),filter,nside_fit,lmax)
        call harmonic2eigen_spin(alm(:,:,:,ne_min:nqu_in),evec(:,ne_min:nqu_in))
        call recompose_matrix(evec(:,ne_min:nqu_in),eval(ne_min:nqu_in),cov,overwrite=.true.)
        tmp = cov
        call mask_matrix(lmask,tmp)
        
        if(regularize) then
           do jf = 0,nstep*fscale
              ff = fmin + fstep*jf
              tmpcov = ff*tmp
              do concurrent (i = 1:nmask)
                 tmpcov(i,i) = tmpcov(i,i) +rp2
              end do
              
              call dpotrf( 'L', nmask, tmpcov, nmask, info)
              if (info .ne. 0) then
                 llike(jf,jc,ja) = hpx_dbadval
                 chi2(jf,jc,ja)  = hpx_dbadval
              else
                 auxm = map
                 call dpotrs( 'L', nmask, ndata, tmpcov, nmask, auxm, nmask, info)
                 lgdet = 0._dp
                 do i = 1,nmask
                    lgdet = lgdet +log(tmpcov(i,i))
                 end do
                 lgdet = 2._dp*lgdet
                 
                 chi2(jf,jc,ja) = 0._dp
                 do i = 1,ndata
                    dchi = sum(map(:,i)*auxm(:,i))
                    chi2(jf,jc,ja)  =  chi2(jf,jc,ja) + dchi
                 end do
                 chi2(jf,jc,ja)  = chi2(jf,jc,ja)/ndata
                 llike(jf,jc,ja) = -(lgdet +chi2(jf,jc,ja))*.5_dp
              end if
              
           end do
        else
           stop 'regularization = F not implemented yet'
        end if
        t2 = etime(et)
        write(0,*)myid,jc,ja,t2-t1
     end do
  end do

  allocate(buff,mold=llike)
  call mpi_reduce(llike, buff, (nstep*fscale+1)*(nstep+1)**2, &
       mpi_double_precision, mpi_sum, root, mpi_comm_world, ierr)

  call mpi_barrier(mpi_comm_world ,ierr)
  if(myid .eq. root ) llike = buff
  call mpi_reduce(chi2,buff,(nstep*fscale+1)*(nstep+1)**2, &
mpi_double_precision,mpi_sum,root,&
       mpi_comm_world,ierr)
  if(myid .eq. root ) then 
     chi2 = buff
     deallocate(buff)
     bestlike = maxval(llike)
     id = maxloc(llike)
     fbest = fmin +(id(1)-1)*fstep !lower bounds on llike = 0
     cbest = cmin +(id(2)-1)*cstep
     abest = amin +(id(3)-1)*astep

     open(newunit=unit,file=trim(outroot)//'_like.txt',status='unknown',action='write')
     do ja = 0,nstep
        aa = amin +ja*astep
        do jc = 0,nstep
           cc = cmin +jc*cstep
           do jf = 0,nstep*fscale
              ff = fmin + fstep*jf
              write(unit,'(3(2x,i3),3(2x,e15.7),2(2x,e22.12))') jf,jc,ja,ff,cc,aa,llike(jf,jc,ja),chi2(jf,jc,ja) 
           end do
        end do
     end do
     close(unit)
     
     open(newunit=unit,file=trim(outroot)//'_best.txt',status='unknown',action='write')
     write(unit,'(4(3x,e22.12))') fbest,cbest,abest,bestlike
     close(unit)
     
     open(newunit=unit,file=trim(outroot)//'_filter.txt',status='unknown',action='write')

     write(unit,'(3x,i3,4(3x,e22.12))') 0,filter(0,:)
     do l = 1,lmax
        filter(l,:) = (1._dp +cbest*(real(loff+l,dp)/(loff+l0))**abest)*fbest*rfilter(l,:)
        write(unit,'(3x,i3,4(3x,e22.12))') l,filter(l,:)
     end do
     close(unit)

     alm = almr
     call filter_heigen_p(alm,filter,nside_fit,lmax)
     call harmonic2eigen_spin(alm,evec)
     call recompose_matrix(evec,eval,cov)
     inquire(iolength=rcl) cov
     open(newunit=unit,file=trim(outroot)//'_bestfit_cov_L.dat',status='unknown',action='write',access='direct',form='unformatted',recl=rcl)
     write(unit,rec=1) cov
     close(unit)

  end if

  call mpi_finalize(ierr)

end program fit_ncvm
