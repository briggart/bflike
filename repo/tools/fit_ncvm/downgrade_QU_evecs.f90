program downgrade_QU_evecs

#ifdef INTEL_COMPILER
  use ifport
#endif

  use healpix_modules
  use lpl_utils_mod
  use filter_matrix
  implicit none

  integer(i4b) ,parameter :: ring = 1 ,nested = 2
  integer(i4b)            :: nside_in ,npix_in ,nside_out ,npix_out ,nqu_in ,nqu_out 

  complex(dpc) ,allocatable :: almr(:,:,:,:) ,alm(:,:,:,:)
  real(dp)     ,allocatable :: evec(:,:) ,cov(:,:) ,filter(:,:) ,eval(:) ,pwf(:,:)
  integer(i4b)              :: i ,rcl ,l ,unit ,lmax ,ncmd ,lmin 
  real(dp)                  :: fwhm
  real                      :: t1 ,t2 ,et(2)

  type(paramfile_handle)    :: handle
  character(len=512)        :: parfile ,evecfile ,evalfile ,outroot ,beamfile
  character(len=80)         :: hdr(80)
  logical                   :: apply_pwf

  ncmd = command_argument_count()

  if(ncmd .ne. 1) then
     write(*,*) 'CRITICAL ERROR : usage downgrade_evecs params.ini'
     stop
  else
     call get_command_argument(1,parfile)
  end if

  handle  = parse_init(parfile)

  nside_in  = parse_int(handle,'nside_in')
  npix_in   = 12*nside_in**2
  nqu_in    = 2*npix_in

  nside_out = parse_int(handle,'nside_out')
  npix_out  = 12*nside_out**2
  nqu_out   = 2*npix_out

  lmin      = parse_int(handle,'lmin')
  lmax      = parse_int(handle,'lmax')
  fwhm      = parse_double(handle,'fwhm')
  beamfile  = parse_string(handle,'beamfile')
  apply_pwf = parse_lgt(handle,'apply_pwf')

  evecfile  = parse_string(handle,'evecs_in')
  evalfile  = parse_string(handle,'evals_in')
  outroot   = parse_string(handle,'output_root')

  call parse_finish(handle)

  allocate(evec(nqu_in,1))
  allocate(almr(2,0:lmax,0:lmax,nqu_in))
  inquire(iolength=rcl) evec

  !read eigenvectors 1 at the time. we assume only Q,U part of EV is stored.
  open(newunit=unit,file=evecfile,status='old',action='read',form='unformatted',access='direct',recl=rcl)
  i = 1
  read(unit,rec=i) evec
  call eigen2harmonic_spin(evec,lmax,alm)
  almr(:,:,:,i) = alm(:,:,:,1)
  t1 = etime(et)
  do i = 1,nqu_in
     read(unit,rec=i) evec
     call eigen2harmonic_spin(evec,lmax,alm)
     almr(:,:,:,i) = alm(:,:,:,1)
  end do
  close(unit)

  deallocate(evec,alm)
  t2 = etime(et)
  write(*,*) 'e2h: ',t2-t1
  t1 = t2
  allocate(filter(0:lmax,3))

  if (beamfile .ne. '') then
     call fits2cl(trim(beamfile),filter,lmax,3,hdr)
  elseif (fwhm .ge. 0) then
     call gaussbeam(fwhm,lmax,filter)
  else
     filter = 1._dp
     do l = lmin,lmax
        filter(l,:) = (1._dp +cos((l-lmin)*pi/(lmax-lmin)))*.5_dp
     end do
  end if

  if (apply_pwf) then
     allocate(pwf,mold=filter)
     call pixel_window(pwf,nside_out)
     filter = filter*pwf
     deallocate(pwf)
  end if

  call filter_heigen_p(almr,filter,nside_in,lmax)
  deallocate(filter)
  t2 = etime(et)
  write(*,*) 'filtering: ',t2 -t1
  t1 = t2

  allocate(evec(nqu_out,nqu_in))
  call harmonic2eigen_spin(almr,evec)
  deallocate(almr)
  t2 = etime(et)
  write(*,*) 'h2e: ',t2 -t1
  t1 = t2

  inquire(iolength=rcl) evec(:,1)
  open(newunit=unit,file=trim(evecfile)//'_downgraded',status='replace',action='write',form='unformatted',access='direct',recl=rcl)
  do i =1,nqu_in
     write(unit,rec=i) evec(:,i)
  end do
  close(unit)
  t2 = etime(et)
  write(*,*) 'write_dg: ',t2 -t1
  t1 = t2

  allocate(cov(nqu_out,nqu_out))
  allocate(eval(nqu_in))

  open(newunit=unit,file=evalfile,status='old',action='read')
  do i = 1 ,nqu_in
     read(unit,*) eval(i)
  end do
  close(unit)
  call recompose_matrix(evec,eval,cov,overwrite=.true.)
  deallocate(evec,eval)
  !only lower triangular part is computed, so symmetrize matrix
  do i = 1,nqu_out
     cov(i,i+1:nqu_out) = cov(i+1:nqu_out,i)
  end do
  t2 = etime(et)
  write(*,*) 'recompose: ',t2 -t1
  t1 = t2

  inquire(iolength=rcl) cov(:,1)
  open(newunit=unit,file=outroot,status='unknown',action='write',form='unformatted',access='direct',recl=rcl)
  do i =1,nqu_out
     write(unit,rec=i) cov(:,i)
  end do
  close(unit)
  t2 = etime(et)
  write(*,*) 'write: ',t2 -t1
  t1 = t2

  allocate(evec ,mold=cov)
  allocate(eval(nqu_out))
  call decompose_matrix(cov,evec,eval,overwrite=.true.)
  deallocate(cov)
  t2 = etime(et)
  write(*,*) 'decompose: ',t2 -t1
  t1 = t2

  open(newunit=unit,file=trim(outroot)//'-evecs',status='unknown',action='write',form='unformatted',access='direct',recl=rcl)
  do i =1,nqu_out
     write(unit,rec=i) evec(:,i)
  end do
  close(unit)
  open(newunit=unit,file=trim(outroot)//'.eigenvals',status='unknown',action='write')
  do i = 1,nqu_out
     write(unit,*) eval(i)
  end do
  close(unit)
  t2 = etime(et)
  write(*,*) 'write: ',t2 -t1
  t1 = t2

  call eigen2harmonic_spin(evec,lmax,almr)
  t2 = etime(et)
  write(*,*) 'e2h: ',t2 -t1
  t1 = t2

  call output_heigen(almr,trim(outroot)//'-evecs_alm',debug=.true.)
  t2 = etime(et)
  write(*,*) 'output: ',t2 -t1
  t1 = t2

end program downgrade_QU_evecs
